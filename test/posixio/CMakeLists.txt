enable_testing()

if (EZTRACE_ENABLE_POSIXIO)

  set(CMAKE_C_FLAGS "-pthread")
  set(LINK_OPTIONS  "-pthread")

  add_executable(posixio posixio.c)
  add_test(build_posixio "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target posixio)
  add_executable(my_cat my_cat.c)
  add_test(build_my_cat "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target my_cat)

  add_test (posixio_tests bash "${CMAKE_CURRENT_SOURCE_DIR}/run.sh" DEPENDS "build_posixio;build_my_cat")
  set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/posixio")


  # Get the list of tests, and set environment variables
  get_property(test_list DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY TESTS)
  set_property(TEST ${test_list}
    PROPERTY ENVIRONMENT
    "EZTRACE_LIBRARY_PATH=${EZTRACE_LIBRARY_PATH}"
    ${TEST_ENVIRONMENT}
  )
endif()
