/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#include "eztrace.h"
#include "eztrace_sampling.h"


#include "main_ev_codes.h"


int (*libbin_instrumented_function) (double *t, int size, int n);
int (*liblib_instrumented_function) (double *t, int size, int n);



int lib_instrumented_function (double *t, int size, int n) {
    FUNCTION_ENTRY;
    EZTRACE_EVENT_PACKED_1 (EZTRACE_lib_function_1, n);
    int ret = liblib_instrumented_function (t, size, n);
    EZTRACE_EVENT_PACKED_1 (EZTRACE_lib_function_2, n);
    return ret;
}

int bin_instrumented_function (double *t, int size, int n) {
  FUNCTION_ENTRY;
  EZTRACE_EVENT_PACKED_1 (EZTRACE_bin_function_1, n);
  int ret = libbin_instrumented_function (t, size, n);
  EZTRACE_EVENT_PACKED_1 (EZTRACE_bin_function_2, n);
  return ret;
}



START_INTERCEPT
  INTERCEPT2("lib_instrumented_function", liblib_instrumented_function)
INTERCEPT2("bin_instrumented_function", libbin_instrumented_function)
END_INTERCEPT

static void __main_init (void) __attribute__ ((constructor));
/* Initialize the current library */
static void
__main_init (void)
{
  DYNAMIC_INTERCEPT_ALL();

  /* start event recording */
#ifdef EZTRACE_AUTOSTART
  eztrace_start ();
#endif

}

static void __main_conclude (void) __attribute__ ((destructor));
static void
__main_conclude (void)
{
  /* stop event recording */
  eztrace_stop ();
}
