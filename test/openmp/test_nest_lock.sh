#!/bin/bash
CUR_PATH=$(dirname $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="nest_lock"

run_and_check_command "$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "openmp" "./test_$name" || ((nb_fail++))

trace_filename="test_${name}_trace/eztrace_log.otf2"
trace_check_integrity "$trace_filename" || return 1
trace_check_enter_leave_parity  "$trace_filename"

nb_locks=100
nb_calls=$(echo "$nb_locks * 2"|bc)

trace_check_event_type "$trace_filename" "THREAD_ACQUIRE_LOCK" $nb_locks
trace_check_event_type "$trace_filename" "THREAD_RELEASE_LOCK" $nb_locks

trace_check_nb_enter "$trace_filename" "OpenMP Set Nest Lock" $nb_calls
trace_check_nb_leave "$trace_filename" "OpenMP Set Nest Lock" $nb_calls
trace_check_nb_enter "$trace_filename" "OpenMP Unset Nest Lock" $nb_calls
trace_check_nb_leave "$trace_filename" "OpenMP Unset Nest Lock" $nb_calls

trace_check_event_type "$trace_filename" "THREAD_TEAM_BEGIN" 4
trace_check_event_type "$trace_filename" "THREAD_TEAM_END" 4

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed
