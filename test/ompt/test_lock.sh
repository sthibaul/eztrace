#!/bin/bash
CUR_PATH=$(dirname $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="lock"

run_and_check_command "$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "ompt" "./test_$name"

trace_filename="test_${name}_trace/eztrace_log.otf2"
trace_check_integrity "$trace_filename"
#trace_check_enter_leave_parity  "$trace_filename"

trace_check_event_type "$trace_filename" "THREAD_ACQUIRE_LOCK" 100
trace_check_event_type "$trace_filename" "THREAD_RELEASE_LOCK" 100

trace_check_nb_enter "$trace_filename" "OpenMP acquire mutex" 100
trace_check_nb_leave "$trace_filename" "OpenMP acquire mutex" 100

trace_check_event_type "$trace_filename" "THREAD_BEGIN" 50
trace_check_event_type "$trace_filename" "THREAD_END" 50

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed
