enable_testing()

if (EZTRACE_ENABLE_MPI)
  set(CMAKE_C_COMPILER ${MPICC})
  set(CMAKE_Fortran_COMPILER ${MPIF90})

  add_executable(mpi_ping ../mpi/mpi_ping.c)
  add_test (build_mpi_ping "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target mpi_ping)

  add_executable(mpi_pthread ../mpi/mpi_pthread.c)
  add_test (build_mpi_pthread "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target mpi_pthread)

  add_test (mpi_tests bash "${CMAKE_CURRENT_SOURCE_DIR}/../mpi/run.sh" DEPENDS "build_mpi_ping build_mpi_pthread")
  set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/mpi")

  # Get the list of tests, and set environment variables
  get_property(test_list DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY TESTS)
  set_property(TEST ${test_list}
    PROPERTY ENVIRONMENT
    "EZTRACE_LIBRARY_PATH=${EZTRACE_LIBRARY_PATH}"
    ${TEST_ENVIRONMENT}
  )
endif()
