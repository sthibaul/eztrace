Version 2.1
-----------
* New modules: cuda, netcdf, pnetcdf, starpu
* Add support for compiler instrumentation
* Some modules are still (temporarily) disabled: papi
* fix the loading of the ompt module (see https://bugs.launchpad.net/ubuntu/+source/eztrace/+bug/2016471)

Version 2.0
-----------
* EZTrace now generates OTF2 traces
* Some modules are (temporarily) disabled: cuda, papi, starpu
* New module: iotracer. This module uses IOTracer to trace the IO events that happen in the Linux kernel.
* New module: ompt. This module traces OpenMP events using the OMPT interface. Unlike with the openmp module, you do not need to instrument the application with eztrace_cc.


Version 1.1
-----------
* Add a StarPU module

Version 1.0
-----------
* Add support for CUDA applications
* Add a script that generates an EZTrace plugin directly from an executable program
* EZTrace now relies on LiTL (instead of FxT) for recording events
* EZTrace can now track the CPU on which a thread run
* Add a sampling interface that allows to call a function every x ms
* EZTrace is now under the CeCILL-B license

Version 0.9
-----------
* Add support for MPI Communicators in collective communications
* Add support for non-blocking collective communications
* Add options in eztrace and eztrace.old to manipulate various environment variable (EZTRACE_TRACE, EZTRACE_OUTPUT_DIR, etc.)

Version 0.8
-----------
* EZTrace now relies on Opari2. Thus, OpenMP 3.0 programs can be analyzed
* EZTrace provides two trace flush policies: the trace is written to disk only at the end of the program (thus, some events may be lost, but there is no overhead) or when the event buffer is full (thus, no event lost, but there's an overhead when the trace is being written to disk)
* EZTrace now implements a trace synchronisation mechanism
* EZTrace can now instrument functions located inside the application or in a statically-linked library(it does not require to be dynamically linked anymore)

Version 0.7
-----------
* EZTrace can now use PAPI (Performance Application Programming Interface) for extracting hardware counters
* Update the OpenMP module. It can now use Opari for instrumenting OpenMP program and extract precise informations
* Fix the generation of OTF traces
* Improve statistics on MPI messages

Version 0.6
-----------
* Add a module for standard IO primitives (read, write, select, ...)
* Add a module for tracking memory consumption (malloc, free, ...)
* Add a source-to-source compiler (eztrace_create) for converting simple scripts
  into eztrace plugins
* FxT and GTG (with OTF support) are now included in eztrace. Thus, you
  don't need to download/install them separately.

Version 0.5
-----------
* EZTrace now relies on modules (plugins).
* EZTrace now supports user-defined plugins.
* EZTrace now works on Mac OS X. However, since mac os pthread interface
  is limited compared to the linux one, the pthread module is not
  available on macos.
* The Coreblas module has been removed. It is now part of the Plasma
  project.
* eztrace_stats is now available. Instead of merging traces, it computes
  statistics on the traces (number of events, average size of MPI
  messages, etc.)
* Fix various bugs in PThread and MPI modules.

Version 0.4
-----------
* EZTrace now uses GTG for converting traces. You can now generate OTF
  traces


Version 0.3
-----------
* Add full support for MPI communication operations (for both C and
	Fortran bindings)
* Add support for OpenMP scheduling strategies (static, dynamic, guided
  and runtime)
* Fix various bugs in PThread, OpenMP and MPI modules

Version 0.2
-----------
* Add support for Fortran MPI programs
* Add support for MPI collective communications
* Add support for non-blocking MPI communications

Version 0.1.1
-----------
* Fix a bug regarding pthread_cond_signal and pthread_cond_broadcast

Version 0.1
-----------
* This first release supports:
	* Threads state (blocked/working)
	* OpenMP parallel sections for C or Fortran programs (merely tested)
	* sem_post/sem_wait are shown as Events and Links in the Paje trace
        * Basic MPI support
