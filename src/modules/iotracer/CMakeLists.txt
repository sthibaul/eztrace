add_library(eztrace-iotracer SHARED
  iotracer.c
  iotracer_to_otf2.c
)

target_link_libraries(eztrace-iotracer
  PRIVATE
    atomic
    dl
    eztrace-core
    eztrace-lib
    eztrace-instrumentation
)

target_include_directories(eztrace-iotracer
  PRIVATE
  ${CMAKE_SOURCE_DIR}/src/core/include/eztrace-core/
)

#---------------------------------------------

install(TARGETS eztrace-iotracer
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
