#ifndef EZT_DEMANGLE_H
#define EZT_DEMANGLE_H

/* demangle a string into a newly allocated buffer.
 */
const char* ezt_demangle(const char* mangled_str);

#endif /* EZT_DEMANGLE_H */
