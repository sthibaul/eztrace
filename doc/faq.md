# Frequently Asked Questions

Q: What if I don't want to trace my whole application, but only a small part of
   it?

A: Then, you can call `eztrace_start()` and `eztrace_stop()` in your code to specify
   the part to be traced. You will need to `#include <eztrace.h>` and to link with
   libeztrace. Then, just run your application as usual
   (i.e. `$ ./my_program my_arg1`)

Q: I need to trace my program while using `gdb`, how can I do that?

A: Just add the `-d` option to `eztrace` or `eztrace.preload` to enable `gdb`:
   ```
$ eztrace -d ./my_program my_arg1 my_arg2
   ```

Q: I want my trace to be saved in a specific directory, how can I do that?

A: By default, EZTrace saves the trace in the current directory. You can change
   this by setting `EZTRACE_TRACE_DIR` or by providing the `-o` option to eztrace.


Q: eztrace does not generate OTF2 traces. Instead, I get file named USER_eztrace_log_rank_X.

A: You're using eztrace version 1 with the old trace format. With eztrace 2.0, we move to the standard OTF2 trace format. You can either upgrade to eztrace 2, or use [the documentation for eztrace 1.x](https://eztrace.gitlab.io/eztrace/)