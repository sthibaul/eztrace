/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include "mpi_eztrace.h"

#include <dlfcn.h>
#include <eztrace-lib/eztrace.h>
#include <mpi.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <unistd.h>
#include <eztrace-core/eztrace_attributes.h>

static void MPI_Waitany_prolog(int count MAYBE_UNUSED,
			       MPI_Fint* reqs MAYBE_UNUSED,
                               int* index  MAYBE_UNUSED,
                               MPI_Status* status  MAYBE_UNUSED,
                               size_t size MAYBE_UNUSED,
			       int* isvalid MAYBE_UNUSED) {
}

static int MPI_Waitany_core(int count,
			    MPI_Request* reqs,
			    int* index,
                            MPI_Status* status) {
  return libMPI_Waitany(count, reqs, index, status);
}


static void MPI_Waitany_epilog(int count MAYBE_UNUSED,
			       MPI_Fint* reqs MAYBE_UNUSED,
                               int* index  MAYBE_UNUSED,
                               MPI_Status* status MAYBE_UNUSED,
                               size_t size MAYBE_UNUSED,
			       int* isvalid) {
  if(isvalid[*index])
    mpi_complete_request(GET_ARRAY_ITEM(reqs, size, *index), &status[*index]);
}

int MPI_Waitany(int count,
		MPI_Request* reqs,
		int* index,
		MPI_Status* status) {
  FUNCTION_ENTRY;
  ALLOCATE_ITEMS(int, count, isvalid_static, isvalid);

  MPI_Status ezt_mpi_status[count];
  if(status == MPI_STATUSES_IGNORE)
    status = ezt_mpi_status;

  for(int i=0; i<count; i++){
    /* Make sure that requests passed to mpi_complete_request are valid.
     * This prevents from completing the same request several times
     */
    isvalid[i] = reqs[i] != MPI_REQUEST_NULL;
  }

  MPI_Waitany_prolog(count, (MPI_Fint*)reqs, index, status, sizeof(MPI_Request), isvalid);
  int ret = MPI_Waitany_core(count, reqs, index, status);
  MPI_Waitany_epilog(count, (MPI_Fint*)reqs, index, status, sizeof(MPI_Request), isvalid);
  FUNCTION_EXIT;
  return ret;
}

void mpif_waitany_(int* c,
		   MPI_Fint* r,
		   MPI_Status* s,
		   int* index,
		   int* error) {
  FUNCTION_ENTRY_("mpi_waitany_");
  ALLOCATE_ITEMS(int, *c, isvalid_static, isvalid);

  int i;
  MPI_Waitany_prolog(*c, r, index, s, sizeof(MPI_Fint), isvalid);

  ALLOCATE_ITEMS(MPI_Request, *c, c_req, p_req);

  for (i = 0; i < *c; i++) {
    p_req[i] = MPI_Request_f2c(r[i]);
    isvalid[i] = p_req[i] != MPI_REQUEST_NULL;
  }

  *error = MPI_Waitany_core(*c, p_req, index, s);

  for (i = 0; i < *c; i++)
    r[i] = MPI_Request_c2f(p_req[i]);

  MPI_Waitany_epilog(*c, r, index, s, sizeof(MPI_Fint), isvalid);
  FUNCTION_EXIT_("mpi_waitany_");
}
