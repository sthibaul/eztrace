#!/bin/bash
CUR_PATH=$(dirname $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="mpi_ping"
np="2"

[ -n "$MPI_MODULE_NAME" ] || MPI_MODULE_NAME=mpi
run_and_check_command "$MPIRUN_PATH" $MPIRUN_CLI_OPTION -np $np "$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "$MPI_MODULE_NAME ./$name"

trace_filename="${name}_trace/eztrace_log.otf2"
if ! "$OTF2_PRINT_PATH" "$trace_filename" 2>&1 > /dev/null ; then
    print_error "Cannot parse trace '$trace_filename'"
    exit 1
fi

trace_check_enter_leave_parity  "$trace_filename"
# 2 processes: 100 warmup + 10000 iterations = 2*(100+10000) = 20200 sends and recvs

trace_check_event_type "$trace_filename" "MPI_SEND" 20200
trace_check_event_type "$trace_filename" "MPI_RECV" 20200 

trace_check_nb_enter "$trace_filename" "MPI_Send" 20200 
trace_check_nb_leave "$trace_filename" "MPI_Send" 20200 
trace_check_nb_enter "$trace_filename" "MPI_Recv" 20200 
trace_check_nb_leave "$trace_filename" "MPI_Recv" 20200 

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed
