enable_testing()

if (EZTRACE_ENABLE_OMPT)
  set(CMAKE_C_FLAGS "-fopenmp")
  set(LINK_OPTIONS "-fopenmp")

  add_executable(ompt_test_lock test_lock.c)
  add_executable(ompt_test_parallel_for test_parallel_for.c)
  add_executable(ompt_test_task test_task.c)

  set_target_properties(ompt_test_lock PROPERTIES OUTPUT_NAME "test_lock")
  set_target_properties(ompt_test_parallel_for PROPERTIES OUTPUT_NAME "test_parallel_for")
  set_target_properties(ompt_test_task PROPERTIES OUTPUT_NAME "test_task")

  add_test(ompt_build_test_lock "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target ompt_test_lock)
  add_test(ompt_build_test_parallel_for "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target ompt_test_parallel_for)
  add_test(ompt_build_test_task "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target ompt_test_task)

  add_test (ompt_tests bash "${CMAKE_CURRENT_SOURCE_DIR}/run.sh" DEPENDS "ompt_build_test_lock;ompt_build_test_parallel_for;ompt_build_test_task")
  set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/ompt")


  # Get the list of tests, and set environment variables
  get_property(test_list DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY TESTS)
  set_property(TEST ${test_list}
    PROPERTY ENVIRONMENT
    "EZTRACE_LIBRARY_PATH=${EZTRACE_LIBRARY_PATH}"
    ${TEST_ENVIRONMENT}
  )
endif()
