# set(CMAKE_REQUIRED_INCLUDES "${STARPU_INCLUDE_DIRS}")
# foreach(libdir ${STARPU_LIBRARY_DIRS})
#   list(APPEND CMAKE_REQUIRED_FLAGS "-L${libdir}")
# endforeach()
# string(REPLACE ";" " " CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS}")
# set(CMAKE_REQUIRED_LIBRARIES "${STARPU_LIBRARIES}")

check_function_exists(starpu_mpi_data_register_comm STARPU_MPI_DATA_REGISTER_COMM_FOUND)
if ( STARPU_MPI_DATA_REGISTER_COMM_FOUND )
  add_definitions(-DHAVE_STARPU_MPI_DATA_REGISTER_COMM)
endif()


# Include directories
#include_directories(
#  ${MPI_INCLUDE_PATH}
#  )

# Libraries diretory
  # ${STARPU_LIBRARY_DIRS}

# Create : libeztrace-starpu.so
add_library(eztrace-starpu SHARED
  starpu.c
)

target_include_directories(eztrace-starpu
  PRIVATE
    ${STARPU_INCLUDE_DIRS}
)

target_link_libraries(eztrace-starpu
  PRIVATE
    dl
    eztrace-core
    eztrace-lib
    ${STARPU_LIBRARIES}
)

install(TARGETS eztrace-starpu
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
