/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <eztrace-core/eztrace_config.h>
#include <eztrace-lib/eztrace.h>
#include <eztrace-lib/eztrace_module.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mpi.h>
#include <otf2/OTF2_AttributeList.h>

#include <pnetcdf.h>

/* set to 1 when all the hooks are set.
 * This is usefull in order to avoid recursive calls to functions for example
 */
static volatile int _pnetcdf_initialized = 0;

#define CURRENT_MODULE pnetcdf
DECLARE_CURRENT_MODULE;


/* pointers to actual functions */
int (*libncmpi_create)(MPI_Comm comm, const char *path, int cmode, MPI_Info info,
		       int *ncidp);

int (*libncmpi_open)(MPI_Comm comm, const char *path, int omode, MPI_Info info,
		     int *ncidp);

int (*libncmpi_inq_file_info)(int ncid, MPI_Info *info_used);

int (*libncmpi_get_file_info)(int ncid, MPI_Info *info_used); /* deprecated */

int (*libncmpi_delete)(const char *filename, MPI_Info info);

int (*libncmpi_enddef)(int ncid);

int (*libncmpi__enddef)(int ncid, MPI_Offset h_minfree, MPI_Offset v_align,
			MPI_Offset v_minfree, MPI_Offset r_align);

int (*libncmpi_redef)(int ncid);

int (*libncmpi_set_default_format)(int format, int *old_formatp);

int (*libncmpi_inq_default_format)(int *formatp);

int (*libncmpi_sync)(int ncid);

int (*libncmpi_flush)(int ncid);

int (*libncmpi_sync_numrecs)(int ncid);

int (*libncmpi_abort)(int ncid);

int (*libncmpi_begin_indep_data)(int ncid);

int (*libncmpi_end_indep_data)(int ncid);

int (*libncmpi_close)(int ncid);

int (*libncmpi_set_fill)(int ncid, int fillmode, int *old_modep);

int (*libncmpi_def_var_fill)(int ncid, int varid, int no_fill, const void *fill_value);

int (*libncmpi_fill_var_rec)(int ncid, int varid, MPI_Offset recno);

/* End File Functions */

/* Begin Define Mode Functions */

int (*libncmpi_def_dim)(int ncid, const char *name, MPI_Offset len, int *idp);

int (*libncmpi_def_var)(int ncid, const char *name, nc_type xtype, int ndims,
			const int *dimidsp, int *varidp);

int (*libncmpi_rename_dim)(int ncid, int dimid, const char *name);

int (*libncmpi_rename_var)(int ncid, int varid, const char *name);

int (*libncmpi_inq)(int ncid, int *ndimsp, int *nvarsp, int *ngattsp, int *unlimdimidp);

int (*libncmpi_inq_format)(int ncid, int *formatp);

int (*libncmpi_inq_file_format)(const char *filename, int *formatp);

int (*libncmpi_inq_version)(int ncid, int *NC_mode);

int (*libncmpi_inq_striping)(int ncid, int *striping_size, int *striping_count);

int (*libncmpi_inq_ndims)(int ncid, int *ndimsp);

int (*libncmpi_inq_nvars)(int ncid, int *nvarsp);

int (*libncmpi_inq_num_rec_vars)(int ncid, int *nvarsp);

int (*libncmpi_inq_num_fix_vars)(int ncid, int *nvarsp);

int (*libncmpi_inq_natts)(int ncid, int *ngattsp);

int (*libncmpi_inq_unlimdim)(int ncid, int *unlimdimidp);

int (*libncmpi_inq_dimid)(int ncid, const char *name, int *idp);

int (*libncmpi_inq_dim)(int ncid, int dimid, char *name, MPI_Offset *lenp);

int (*libncmpi_inq_dimname)(int ncid, int dimid, char *name);

int (*libncmpi_inq_dimlen)(int ncid, int dimid, MPI_Offset *lenp);

int (*libncmpi_inq_var)(int ncid, int varid, char *name, nc_type *xtypep, int *ndimsp,
			int *dimidsp, int *nattsp);

int (*libncmpi_inq_varid)(int ncid, const char *name, int *varidp);

int (*libncmpi_inq_varname)(int ncid, int varid, char *name);

int (*libncmpi_inq_vartype)(int ncid, int varid, nc_type *xtypep);

int (*libncmpi_inq_varndims)(int ncid, int varid, int *ndimsp);

int (*libncmpi_inq_vardimid)(int ncid, int varid, int *dimidsp);

int (*libncmpi_inq_varnatts)(int ncid, int varid, int *nattsp);

int (*libncmpi_inq_varoffset)(int ncid, int varid, MPI_Offset *offset);

int (*libncmpi_inq_put_size)(int ncid, MPI_Offset *size);

int (*libncmpi_inq_get_size)(int ncid, MPI_Offset *size);

int (*libncmpi_inq_header_size)(int ncid, MPI_Offset *size);

int (*libncmpi_inq_header_extent)(int ncid, MPI_Offset *extent);

int (*libncmpi_inq_malloc_size)(MPI_Offset *size);

int (*libncmpi_inq_malloc_max_size)(MPI_Offset *size);

int (*libncmpi_inq_malloc_list)(void);

int (*libncmpi_inq_files_opened)(int *num, int *ncids);

int (*libncmpi_inq_recsize)(int ncid, MPI_Offset *recsize);

int (*libncmpi_inq_var_fill)(int ncid, int varid, int *no_fill, void *fill_value);

int (*libncmpi_inq_path)(int ncid, int *pathlen, char *path);

int (*libncmpi_inq_att)(int ncid, int varid, const char *name, nc_type *xtypep,
			MPI_Offset *lenp);

int (*libncmpi_inq_attid)(int ncid, int varid, const char *name, int *idp);

int (*libncmpi_inq_atttype)(int ncid, int varid, const char *name, nc_type *xtypep);

int (*libncmpi_inq_attlen)(int ncid, int varid, const char *name, MPI_Offset *lenp);

int (*libncmpi_inq_attname)(int ncid, int varid, int attnum, char *name);

int (*libncmpi_copy_att)(int ncid_in, int varid_in, const char *name, int ncid_out,
			 int varid_out);

int (*libncmpi_rename_att)(int ncid, int varid, const char *name, const char *newname);

int (*libncmpi_del_att)(int ncid, int varid, const char *name);

int (*libncmpi_put_att)(int ncid, int varid, const char *name, nc_type xtype,
			MPI_Offset nelems, const void *value);

int (*libncmpi_put_att_text)(int ncid, int varid, const char *name, MPI_Offset len,
			     const char *op);

int (*libncmpi_put_att_schar)(int ncid, int varid, const char *name, nc_type xtype,
			      MPI_Offset len, const signed char *op);

int (*libncmpi_put_att_short)(int ncid, int varid, const char *name, nc_type xtype,
			      MPI_Offset len, const short *op);

int (*libncmpi_put_att_int)(int ncid, int varid, const char *name, nc_type xtype,
			    MPI_Offset len, const int *op);

int (*libncmpi_put_att_float)(int ncid, int varid, const char *name, nc_type xtype,
			      MPI_Offset len, const float *op);

int (*libncmpi_put_att_double)(int ncid, int varid, const char *name, nc_type xtype,
			       MPI_Offset len, const double *op);

int (*libncmpi_put_att_longlong)(int ncid, int varid, const char *name, nc_type xtype,
				 MPI_Offset len, const long long *op);

int (*libncmpi_get_att)(int ncid, int varid, const char *name, void *value);

int (*libncmpi_get_att_text)(int ncid, int varid, const char *name, char *ip);

int (*libncmpi_get_att_schar)(int ncid, int varid, const char *name, signed char *ip);

int (*libncmpi_get_att_short)(int ncid, int varid, const char *name, short *ip);

int (*libncmpi_get_att_int)(int ncid, int varid, const char *name, int *ip);

int (*libncmpi_get_att_float)(int ncid, int varid, const char *name, float *ip);

int (*libncmpi_get_att_double)(int ncid, int varid, const char *name, double *ip);

int (*libncmpi_get_att_longlong)(int ncid, int varid, const char *name, long long *ip);

int (*libncmpi_put_att_uchar)(int ncid, int varid, const char *name, nc_type xtype,
			      MPI_Offset len, const unsigned char *op);

int (*libncmpi_put_att_ubyte)(int ncid, int varid, const char *name, nc_type xtype,
			      MPI_Offset len, const unsigned char *op);

int (*libncmpi_put_att_ushort)(int ncid, int varid, const char *name, nc_type xtype,
			       MPI_Offset len, const unsigned short *op);

int (*libncmpi_put_att_uint)(int ncid, int varid, const char *name, nc_type xtype,
			     MPI_Offset len, const unsigned int *op);

int (*libncmpi_put_att_long)(int ncid, int varid, const char *name, nc_type xtype,
			     MPI_Offset len, const long *op);

int (*libncmpi_put_att_ulonglong)(int ncid, int varid, const char *name, nc_type xtype,
				  MPI_Offset len, const unsigned long long *op);

int (*libncmpi_get_att_uchar)(int ncid, int varid, const char *name, unsigned char *ip);

int (*libncmpi_get_att_ubyte)(int ncid, int varid, const char *name, unsigned char *ip);

int (*libncmpi_get_att_ushort)(int ncid, int varid, const char *name, unsigned short *ip);

int (*libncmpi_get_att_uint)(int ncid, int varid, const char *name, unsigned int *ip);

int (*libncmpi_get_att_long)(int ncid, int varid, const char *name, long *ip);

int (*libncmpi_get_att_ulonglong)(int ncid, int varid, const char *name,
				  unsigned long long *ip);

int (*libncmpi_put_var1)(int ncid, int varid, const MPI_Offset *start,
			 const void *op, MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_put_var1_all)(int ncid, int varid, const MPI_Offset *start,
			     const void *op, MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_put_var1_text)(int ncid, int varid, const MPI_Offset *start,
			      const char *op);
int (*libncmpi_put_var1_text_all)(int ncid, int varid, const MPI_Offset *start,
				  const char *op);

int (*libncmpi_put_var1_schar)(int ncid, int varid, const MPI_Offset *start,
			       const signed char *op);
int (*libncmpi_put_var1_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   const signed char *op);

int (*libncmpi_put_var1_short)(int ncid, int varid, const MPI_Offset *start,
			       const short *op);
int (*libncmpi_put_var1_short_all)(int ncid, int varid, const MPI_Offset *start,
				   const short *op);

int (*libncmpi_put_var1_int)(int ncid, int varid, const MPI_Offset *start,
			     const int *op);
int (*libncmpi_put_var1_int_all)(int ncid, int varid, const MPI_Offset *start,
				 const int *op);

int (*libncmpi_put_var1_float)(int ncid, int varid, const MPI_Offset *start,
			       const float *op);
int (*libncmpi_put_var1_float_all)(int ncid, int varid, const MPI_Offset *start,
				   const float *op);

int (*libncmpi_put_var1_double)(int ncid, int varid, const MPI_Offset *start,
				const double *op);
int (*libncmpi_put_var1_double_all)(int ncid, int varid, const MPI_Offset *start,
				    const double *op);

int (*libncmpi_put_var1_longlong)(int ncid, int varid, const MPI_Offset *start,
				  const long long *op);
int (*libncmpi_put_var1_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      const long long *op);

int (*libncmpi_get_var1)(int ncid, int varid, const MPI_Offset *start,
			 void *ip, MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_get_var1_all)(int ncid, int varid, const MPI_Offset *start,
			     void *ip, MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_get_var1_text)(int ncid, int varid, const MPI_Offset *start,
			      char *ip);
int (*libncmpi_get_var1_text_all)(int ncid, int varid, const MPI_Offset *start,
				  char *ip);

int (*libncmpi_get_var1_schar)(int ncid, int varid, const MPI_Offset *start,
			       signed char *ip);
int (*libncmpi_get_var1_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   signed char *ip);

int (*libncmpi_get_var1_short)(int ncid, int varid, const MPI_Offset *start,
			       short *ip);
int (*libncmpi_get_var1_short_all)(int ncid, int varid, const MPI_Offset *start,
				   short *ip);

int (*libncmpi_get_var1_int)(int ncid, int varid, const MPI_Offset *start,
			     int *ip);
int (*libncmpi_get_var1_int_all)(int ncid, int varid, const MPI_Offset *start,
				 int *ip);

int (*libncmpi_get_var1_float)(int ncid, int varid, const MPI_Offset *start,
			       float *ip);
int (*libncmpi_get_var1_float_all)(int ncid, int varid, const MPI_Offset *start,
				   float *ip);

int (*libncmpi_get_var1_double)(int ncid, int varid, const MPI_Offset *start,
				double *ip);
int (*libncmpi_get_var1_double_all)(int ncid, int varid, const MPI_Offset *start,
				    double *ip);

int (*libncmpi_get_var1_longlong)(int ncid, int varid, const MPI_Offset *start,
				  long long *ip);
int (*libncmpi_get_var1_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      long long *ip);

int (*libncmpi_put_var1_uchar)(int ncid, int varid, const MPI_Offset *start,
			       const unsigned char *op);
int (*libncmpi_put_var1_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   const unsigned char *op);

int (*libncmpi_put_var1_ushort)(int ncid, int varid, const MPI_Offset *start,
				const unsigned short *op);
int (*libncmpi_put_var1_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    const unsigned short *op);

int (*libncmpi_put_var1_uint)(int ncid, int varid, const MPI_Offset *start,
			      const unsigned int *op);
int (*libncmpi_put_var1_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  const unsigned int *op);

int (*libncmpi_put_var1_long)(int ncid, int varid, const MPI_Offset *start,
			      const long *ip);
int (*libncmpi_put_var1_long_all)(int ncid, int varid, const MPI_Offset *start,
				  const long *ip);

int (*libncmpi_put_var1_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   const unsigned long long *ip);
int (*libncmpi_put_var1_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       const unsigned long long *ip);

int (*libncmpi_get_var1_uchar)(int ncid, int varid, const MPI_Offset *start,
			       unsigned char *ip);
int (*libncmpi_get_var1_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   unsigned char *ip);

int (*libncmpi_get_var1_ushort)(int ncid, int varid, const MPI_Offset *start,
				unsigned short *ip);
int (*libncmpi_get_var1_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    unsigned short *ip);

int (*libncmpi_get_var1_uint)(int ncid, int varid, const MPI_Offset *start,
			      unsigned int *ip);
int (*libncmpi_get_var1_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  unsigned int *ip);

int (*libncmpi_get_var1_long)(int ncid, int varid, const MPI_Offset *start,
			      long *ip);
int (*libncmpi_get_var1_long_all)(int ncid, int varid, const MPI_Offset *start,
				  long *ip);

int (*libncmpi_get_var1_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   unsigned long long *ip);
int (*libncmpi_get_var1_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       unsigned long long *ip);

int (*libncmpi_put_var)(int ncid, int varid, const void *op, MPI_Offset bufcount,
			MPI_Datatype buftype);

int (*libncmpi_put_var_all)(int ncid, int varid, const void *op, MPI_Offset bufcount,
			    MPI_Datatype buftype);

int (*libncmpi_put_var_text)(int ncid, int varid, const char *op);
int (*libncmpi_put_var_text_all)(int ncid, int varid, const char *op);

int (*libncmpi_put_var_schar)(int ncid, int varid, const signed char *op);
int (*libncmpi_put_var_schar_all)(int ncid, int varid, const signed char *op);

int (*libncmpi_put_var_short)(int ncid, int varid, const short *op);
int (*libncmpi_put_var_short_all)(int ncid, int varid, const short *op);

int (*libncmpi_put_var_int)(int ncid, int varid, const int *op);
int (*libncmpi_put_var_int_all)(int ncid, int varid, const int *op);

int (*libncmpi_put_var_float)(int ncid, int varid, const float *op);
int (*libncmpi_put_var_float_all)(int ncid, int varid, const float *op);

int (*libncmpi_put_var_double)(int ncid, int varid, const double *op);
int (*libncmpi_put_var_double_all)(int ncid, int varid, const double *op);

int (*libncmpi_put_var_longlong)(int ncid, int varid, const long long *op);
int (*libncmpi_put_var_longlong_all)(int ncid, int varid, const long long *op);

int (*libncmpi_get_var)(int ncid, int varid, void *ip, MPI_Offset bufcount,
			MPI_Datatype buftype);
int (*libncmpi_get_var_all)(int ncid, int varid, void *ip, MPI_Offset bufcount,
			    MPI_Datatype buftype);

int (*libncmpi_get_var_text)(int ncid, int varid, char *ip);
int (*libncmpi_get_var_text_all)(int ncid, int varid, char *ip);

int (*libncmpi_get_var_schar)(int ncid, int varid, signed char *ip);
int (*libncmpi_get_var_schar_all)(int ncid, int varid, signed char *ip);

int (*libncmpi_get_var_short)(int ncid, int varid, short *ip);
int (*libncmpi_get_var_short_all)(int ncid, int varid, short *ip);

int (*libncmpi_get_var_int)(int ncid, int varid, int *ip);
int (*libncmpi_get_var_int_all)(int ncid, int varid, int *ip);

int (*libncmpi_get_var_float)(int ncid, int varid, float *ip);
int (*libncmpi_get_var_float_all)(int ncid, int varid, float *ip);

int (*libncmpi_get_var_double)(int ncid, int varid, double *ip);
int (*libncmpi_get_var_double_all)(int ncid, int varid, double *ip);

int (*libncmpi_get_var_longlong)(int ncid, int varid, long long *ip);
int (*libncmpi_get_var_longlong_all)(int ncid, int varid, long long *ip);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_put_var_uchar)(int ncid, int varid, const unsigned char *op);
int (*libncmpi_put_var_uchar_all)(int ncid, int varid, const unsigned char *op);

int (*libncmpi_put_var_ushort)(int ncid, int varid, const unsigned short *op);
int (*libncmpi_put_var_ushort_all)(int ncid, int varid, const unsigned short *op);

int (*libncmpi_put_var_uint)(int ncid, int varid, const unsigned int *op);
int (*libncmpi_put_var_uint_all)(int ncid, int varid, const unsigned int *op);

int (*libncmpi_put_var_long)(int ncid, int varid, const long *op);
int (*libncmpi_put_var_long_all)(int ncid, int varid, const long *op);

int (*libncmpi_put_var_ulonglong)(int ncid, int varid, const unsigned long long *op);
int (*libncmpi_put_var_ulonglong_all)(int ncid, int varid, const unsigned long long *op);

int (*libncmpi_get_var_uchar)(int ncid, int varid, unsigned char *ip);
int (*libncmpi_get_var_uchar_all)(int ncid, int varid, unsigned char *ip);

int (*libncmpi_get_var_ushort)(int ncid, int varid, unsigned short *ip);
int (*libncmpi_get_var_ushort_all)(int ncid, int varid, unsigned short *ip);

int (*libncmpi_get_var_uint)(int ncid, int varid, unsigned int *ip);
int (*libncmpi_get_var_uint_all)(int ncid, int varid, unsigned int *ip);

int (*libncmpi_get_var_long)(int ncid, int varid, long *ip);
int (*libncmpi_get_var_long_all)(int ncid, int varid, long *ip);

int (*libncmpi_get_var_ulonglong)(int ncid, int varid, unsigned long long *ip);
int (*libncmpi_get_var_ulonglong_all)(int ncid, int varid, unsigned long long *ip);
/* End Skip Prototypes for Fortran binding */

/* End {put,get}_var */

/* Begin {put,get}_vara */

int (*libncmpi_put_vara)(int ncid, int varid, const MPI_Offset *start,
			 const MPI_Offset *count, const void *op,
			 MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_put_vara_all)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const void *op,
			     MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_put_vara_text)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const char *op);
int (*libncmpi_put_vara_text_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const char *op);

int (*libncmpi_put_vara_schar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const signed char *op);
int (*libncmpi_put_vara_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const signed char *op);

int (*libncmpi_put_vara_short)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const short *op);
int (*libncmpi_put_vara_short_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const short *op);

int (*libncmpi_put_vara_int)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const int *op);
int (*libncmpi_put_vara_int_all)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const int *op);

int (*libncmpi_put_vara_float)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const float *op);

int (*libncmpi_put_vara_float_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const float *op);

int (*libncmpi_put_vara_double)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const double *op);
int (*libncmpi_put_vara_double_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const double *op);

int (*libncmpi_put_vara_longlong)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const long long *op);
int (*libncmpi_put_vara_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      const MPI_Offset *count, const long long *op);

int (*libncmpi_get_vara)(int ncid, int varid, const MPI_Offset *start,
			 const MPI_Offset *count, void *ip, MPI_Offset bufcount,
			 MPI_Datatype buftype);
int (*libncmpi_get_vara_all)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, void *ip, MPI_Offset bufcount,
			     MPI_Datatype buftype);

int (*libncmpi_get_vara_text)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, char *ip);
int (*libncmpi_get_vara_text_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, char *ip);

int (*libncmpi_get_vara_schar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, signed char *ip);
int (*libncmpi_get_vara_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, signed char *ip);

int (*libncmpi_get_vara_short)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, short *ip);
int (*libncmpi_get_vara_short_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, short *ip);

int (*libncmpi_get_vara_int)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, int *ip);
int (*libncmpi_get_vara_int_all)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, int *ip);

int (*libncmpi_get_vara_float)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, float *ip);
int (*libncmpi_get_vara_float_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, float *ip);

int (*libncmpi_get_vara_double)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, double *ip);
int (*libncmpi_get_vara_double_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, double *ip);

int (*libncmpi_get_vara_longlong)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, long long *ip);
int (*libncmpi_get_vara_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      const MPI_Offset *count, long long *ip);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_put_vara_uchar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const unsigned char *op);
int (*libncmpi_put_vara_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const unsigned char *op);

int (*libncmpi_put_vara_ushort)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const unsigned short *op);
int (*libncmpi_put_vara_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const unsigned short *op);

int (*libncmpi_put_vara_uint)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const unsigned int *op);
int (*libncmpi_put_vara_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const unsigned int *op);

int (*libncmpi_put_vara_long)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const long *op);
int (*libncmpi_put_vara_long_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const long *op);

int (*libncmpi_put_vara_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const unsigned long long *op);
int (*libncmpi_put_vara_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       const MPI_Offset *count, const unsigned long long *op);

int (*libncmpi_get_vara_uchar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, unsigned char *ip);
int (*libncmpi_get_vara_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, unsigned char *ip);

int (*libncmpi_get_vara_ushort)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, unsigned short *ip);
int (*libncmpi_get_vara_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, unsigned short *ip);

int (*libncmpi_get_vara_uint)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, unsigned int *ip);
int (*libncmpi_get_vara_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, unsigned int *ip);

int (*libncmpi_get_vara_long)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, long *ip);
int (*libncmpi_get_vara_long_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, long *ip);

int (*libncmpi_get_vara_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, unsigned long long *ip);
int (*libncmpi_get_vara_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       const MPI_Offset *count, unsigned long long *ip);

/* End Skip Prototypes for Fortran binding */

/* End {put,get}_vara */

/* Begin {put,get}_vars */

int (*libncmpi_put_vars)(int ncid, int varid, const MPI_Offset *start,
			 const MPI_Offset *count, const MPI_Offset *stride,
			 const void *op, MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_put_vars_all)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride,
			     const void *op, MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_put_vars_text)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const char *op);
int (*libncmpi_put_vars_text_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const char *op);

int (*libncmpi_put_vars_schar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const signed char *op);
int (*libncmpi_put_vars_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const signed char *op);

int (*libncmpi_put_vars_short)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const short *op);
int (*libncmpi_put_vars_short_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const short *op);

int (*libncmpi_put_vars_int)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride,
			     const int *op);
int (*libncmpi_put_vars_int_all)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const int *op);

int (*libncmpi_put_vars_float)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const float *op);
int (*libncmpi_put_vars_float_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const float *op);

int (*libncmpi_put_vars_double)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const double *op);
int (*libncmpi_put_vars_double_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const double *op);

int (*libncmpi_put_vars_longlong)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const long long *op);
int (*libncmpi_put_vars_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      const MPI_Offset *count, const MPI_Offset *stride,
				      const long long *op);

int (*libncmpi_get_vars)(int ncid, int varid, const MPI_Offset *start,
			 const MPI_Offset *count, const MPI_Offset *stride,
			 void *ip, MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_get_vars_all)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride,
			     void *ip, MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_get_vars_schar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       signed char *ip);
int (*libncmpi_get_vars_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   signed char *ip);

int (*libncmpi_get_vars_text)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride, char *ip);
int (*libncmpi_get_vars_text_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride, char *ip);

int (*libncmpi_get_vars_short)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride, short *ip);
int (*libncmpi_get_vars_short_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride, short *ip);

int (*libncmpi_get_vars_int)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride, int *ip);
int (*libncmpi_get_vars_int_all)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride, int *ip);

int (*libncmpi_get_vars_float)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride, float *ip);
int (*libncmpi_get_vars_float_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride, float *ip);

int (*libncmpi_get_vars_double)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride, double *ip);
int (*libncmpi_get_vars_double_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride, double *ip);

int (*libncmpi_get_vars_longlong)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  long long *ip);
int (*libncmpi_get_vars_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      const MPI_Offset *count, const MPI_Offset *stride,
				      long long *ip);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_put_vars_uchar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const unsigned char *op);
int (*libncmpi_put_vars_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const unsigned char *op);

int (*libncmpi_put_vars_ushort)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const unsigned short *op);
int (*libncmpi_put_vars_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const unsigned short *op);

int (*libncmpi_put_vars_uint)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const unsigned int *op);
int (*libncmpi_put_vars_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const unsigned int *op);

int (*libncmpi_put_vars_long)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const long *op);
int (*libncmpi_put_vars_long_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const long *op);

int (*libncmpi_put_vars_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const unsigned long long *op);
int (*libncmpi_put_vars_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       const MPI_Offset *count, const MPI_Offset *stride,
				       const unsigned long long *op);

int (*libncmpi_get_vars_uchar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       unsigned char *ip);
int (*libncmpi_get_vars_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   unsigned char *ip);

int (*libncmpi_get_vars_ushort)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				unsigned short *ip);
int (*libncmpi_get_vars_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    unsigned short *ip);

int (*libncmpi_get_vars_uint)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      unsigned int *ip);
int (*libncmpi_get_vars_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  unsigned int *ip);

int (*libncmpi_get_vars_long)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      long *ip);
int (*libncmpi_get_vars_long_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  long *ip);

int (*libncmpi_get_vars_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   unsigned long long *ip);
int (*libncmpi_get_vars_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       const MPI_Offset *count, const MPI_Offset *stride,
				       unsigned long long *ip);

/* End Skip Prototypes for Fortran binding */

/* End {put,get}_vars */

/* Begin {put,get}_varm */

int (*libncmpi_put_varm)(int ncid, int varid, const MPI_Offset *start,
			 const MPI_Offset *count, const MPI_Offset *stride,
			 const MPI_Offset *imap, const void *op,
			 MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_put_varm_all)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride,
			     const MPI_Offset *imap, const void *op,
			     MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_put_varm_text)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, const char *op);
int (*libncmpi_put_varm_text_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, const char *op);

int (*libncmpi_put_varm_schar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const signed char *op);
int (*libncmpi_put_varm_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, const signed char *op);

int (*libncmpi_put_varm_short)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const short *op);
int (*libncmpi_put_varm_short_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, const short *op);

int (*libncmpi_put_varm_int)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride,
			     const MPI_Offset *imap, const int *op);
int (*libncmpi_put_varm_int_all)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, const int *op);

int (*libncmpi_put_varm_float)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const float *op);
int (*libncmpi_put_varm_float_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, const float *op);

int (*libncmpi_put_varm_double)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const double *op);
int (*libncmpi_put_varm_double_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const MPI_Offset *imap, const double *op);

int (*libncmpi_put_varm_longlong)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, const long long *op);
int (*libncmpi_put_varm_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      const MPI_Offset *count, const MPI_Offset *stride,
				      const MPI_Offset *imap, const long long *op);

int (*libncmpi_get_varm)(int ncid, int varid, const MPI_Offset *start,
			 const MPI_Offset *count, const MPI_Offset *stride,
			 const MPI_Offset *imap, void *ip, MPI_Offset bufcount,
			 MPI_Datatype buftype);
int (*libncmpi_get_varm_all)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride,
			     const MPI_Offset *imap, void *ip, MPI_Offset bufcount,
			     MPI_Datatype buftype);

int (*libncmpi_get_varm_schar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, signed char *ip);
int (*libncmpi_get_varm_schar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, signed char *ip);

int (*libncmpi_get_varm_text)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, char *ip);
int (*libncmpi_get_varm_text_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, char *ip);

int (*libncmpi_get_varm_short)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, short *ip);
int (*libncmpi_get_varm_short_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, short *ip);

int (*libncmpi_get_varm_int)(int ncid, int varid, const MPI_Offset *start,
			     const MPI_Offset *count, const MPI_Offset *stride,
			     const MPI_Offset *imap, int *ip);
int (*libncmpi_get_varm_int_all)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, int *ip);

int (*libncmpi_get_varm_float)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, float *ip);
int (*libncmpi_get_varm_float_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, float *ip);

int (*libncmpi_get_varm_double)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, double *ip);
int (*libncmpi_get_varm_double_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const MPI_Offset *imap, double *ip);

int (*libncmpi_get_varm_longlong)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, long long *ip);
int (*libncmpi_get_varm_longlong_all)(int ncid, int varid, const MPI_Offset *start,
				      const MPI_Offset *count, const MPI_Offset *stride,
				      const MPI_Offset *imap, long long *ip);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_put_varm_uchar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const unsigned char *op);
int (*libncmpi_put_varm_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, const unsigned char *op);

int (*libncmpi_put_varm_ushort)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const unsigned short *op);
int (*libncmpi_put_varm_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const MPI_Offset *imap, const unsigned short *op);

int (*libncmpi_put_varm_uint)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, const unsigned int *op);
int (*libncmpi_put_varm_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, const unsigned int *op);

int (*libncmpi_put_varm_long)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, const long *op);
int (*libncmpi_put_varm_long_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, const long *op);

int (*libncmpi_put_varm_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, const unsigned long long *op);
int (*libncmpi_put_varm_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       const MPI_Offset *count, const MPI_Offset *stride,
				       const MPI_Offset *imap, const unsigned long long *op);

int (*libncmpi_get_varm_uchar)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, unsigned char *ip);
int (*libncmpi_get_varm_uchar_all)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, unsigned char *ip);

int (*libncmpi_get_varm_ushort)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, unsigned short *ip);
int (*libncmpi_get_varm_ushort_all)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const MPI_Offset *imap, unsigned short *ip);

int (*libncmpi_get_varm_uint)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, unsigned int *ip);
int (*libncmpi_get_varm_uint_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, unsigned int *ip);

int (*libncmpi_get_varm_long)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, long *ip);
int (*libncmpi_get_varm_long_all)(int ncid, int varid, const MPI_Offset *start,
				  const MPI_Offset *count, const MPI_Offset *stride,
				  const MPI_Offset *imap, long *ip);

int (*libncmpi_get_varm_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, unsigned long long *ip);
int (*libncmpi_get_varm_ulonglong_all)(int ncid, int varid, const MPI_Offset *start,
				       const MPI_Offset *count, const MPI_Offset *stride,
				       const MPI_Offset *imap, unsigned long long *ip);

/* End Skip Prototypes for Fortran binding */

/* End {put,get}_varm */

/* Begin of {put,get}_varn{kind} */

int (*libncmpi_put_varn)(int ncid, int varid, int num, MPI_Offset* const *starts,
			 MPI_Offset* const *counts, const void *op,
			 MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_put_varn_all)(int ncid, int varid, int num,
			     MPI_Offset* const *starts, MPI_Offset* const *counts,
			     const void *op, MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_get_varn)(int ncid, int varid, int num, MPI_Offset* const *starts,
			 MPI_Offset* const *counts,  void *ip, MPI_Offset bufcount,
			 MPI_Datatype buftype);
int (*libncmpi_get_varn_all)(int ncid, int varid, int num,
			     MPI_Offset* const *starts, MPI_Offset* const *counts,
			     void *ip, MPI_Offset bufcount, MPI_Datatype buftype);

int (*libncmpi_put_varn_text)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      const char *op);
int (*libncmpi_put_varn_text_all)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  const char *op);

int (*libncmpi_put_varn_schar)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const signed char *op);
int (*libncmpi_put_varn_schar_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   const signed char *op);

int (*libncmpi_put_varn_short)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const short *op);
int (*libncmpi_put_varn_short_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   const short *op);

int (*libncmpi_put_varn_int)(int ncid, int varid, int num,
			     MPI_Offset* const *starts, MPI_Offset* const *counts,
			     const int *op);
int (*libncmpi_put_varn_int_all)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 const int *op);

int (*libncmpi_put_varn_float)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const float *op);
int (*libncmpi_put_varn_float_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   const float *op);

int (*libncmpi_put_varn_double)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const double *op);
int (*libncmpi_put_varn_double_all)(int ncid, int varid, int num,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    const double *op);

int (*libncmpi_put_varn_longlong)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  const long long *op);
int (*libncmpi_put_varn_longlong_all)(int ncid, int varid, int num,
				      MPI_Offset* const *starts, MPI_Offset* const *counts,
				      const long long *op);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_put_varn_uchar)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const unsigned char *op);
int (*libncmpi_put_varn_uchar_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   const unsigned char *op);

int (*libncmpi_put_varn_ushort)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const unsigned short *op);
int (*libncmpi_put_varn_ushort_all)(int ncid, int varid, int num,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    const unsigned short *op);

int (*libncmpi_put_varn_uint)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      const unsigned int *op);
int (*libncmpi_put_varn_uint_all)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  const unsigned int *op);

int (*libncmpi_put_varn_long)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      const long *op);
int (*libncmpi_put_varn_long_all)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  const long *op);

int (*libncmpi_put_varn_ulonglong)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   const unsigned long long *op);
int (*libncmpi_put_varn_ulonglong_all)(int ncid, int varid, int num,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       const unsigned long long *op);

/* End Skip Prototypes for Fortran binding */

int (*libncmpi_get_varn_text)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      char *ip);
int (*libncmpi_get_varn_text_all)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  char *ip);

int (*libncmpi_get_varn_schar)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       signed char *ip);
int (*libncmpi_get_varn_schar_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   signed char *ip);

int (*libncmpi_get_varn_short)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       short *ip);
int (*libncmpi_get_varn_short_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   short *ip);

int (*libncmpi_get_varn_int)(int ncid, int varid, int num,
			     MPI_Offset* const *starts, MPI_Offset* const *counts,
			     int *ip);
int (*libncmpi_get_varn_int_all)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 int *ip);

int (*libncmpi_get_varn_float)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       float *ip);
int (*libncmpi_get_varn_float_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   float *ip);

int (*libncmpi_get_varn_double)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				double *ip);
int (*libncmpi_get_varn_double_all)(int ncid, int varid, int num,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    double *ip);

int (*libncmpi_get_varn_longlong)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  long long *ip);
int (*libncmpi_get_varn_longlong_all)(int ncid, int varid, int num,
				      MPI_Offset* const *starts, MPI_Offset* const *counts,
				      long long *ip);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_get_varn_uchar)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       unsigned char *ip);
int (*libncmpi_get_varn_uchar_all)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   unsigned char *ip);

int (*libncmpi_get_varn_ushort)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				unsigned short *ip);
int (*libncmpi_get_varn_ushort_all)(int ncid, int varid, int num,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    unsigned short *ip);

int (*libncmpi_get_varn_uint)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      unsigned int *ip);
int (*libncmpi_get_varn_uint_all)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  unsigned int *ip);

int (*libncmpi_get_varn_long)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      long *ip);
int (*libncmpi_get_varn_long_all)(int ncid, int varid, int num,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  long *ip);

int (*libncmpi_get_varn_ulonglong)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   unsigned long long *ip);
int (*libncmpi_get_varn_ulonglong_all)(int ncid, int varid, int num,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       unsigned long long *ip);

/* End Skip Prototypes for Fortran binding */

/* End of {put,get}_varn{kind} */

/* Begin {put,get}_vard */
int (*libncmpi_get_vard)(int ncid, int varid, MPI_Datatype filetype, void *ip,
			 MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_get_vard_all)(int ncid, int varid, MPI_Datatype filetype, void *ip,
			     MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_put_vard)(int ncid, int varid, MPI_Datatype filetype, const void *ip,
			 MPI_Offset bufcount, MPI_Datatype buftype);
int (*libncmpi_put_vard_all)(int ncid, int varid, MPI_Datatype filetype, const void *ip,
			     MPI_Offset bufcount, MPI_Datatype buftype);
/* End of {put,get}_vard */

/* Begin {mput,mget}_var */

/* #################################################################### */
/* Begin: more prototypes to be included for Fortran binding conversion */

/* Begin non-blocking data access functions */

int (*libncmpi_wait)(int ncid, int count, int array_of_requests[],
		     int array_of_statuses[]);

int (*libncmpi_wait_all)(int ncid, int count, int array_of_requests[],
			 int array_of_statuses[]);

int (*libncmpi_cancel)(int ncid, int num, int *reqs, int *statuses);

int (*libncmpi_buffer_attach)(int ncid, MPI_Offset bufsize);
int (*libncmpi_buffer_detach)(int ncid);
int (*libncmpi_inq_buffer_usage)(int ncid, MPI_Offset *usage);
int (*libncmpi_inq_buffer_size)(int ncid, MPI_Offset *buf_size);
int (*libncmpi_inq_nreqs)(int ncid, int *nreqs);

/* Begin {iput,iget,bput}_var1 */

int (*libncmpi_iput_var1)(int ncid, int varid, const MPI_Offset *start,
			  const void *op, MPI_Offset bufcount,
			  MPI_Datatype buftype, int *req);

int (*libncmpi_iput_var1_text)(int ncid, int varid, const MPI_Offset *start,
			       const char *op, int *req);

int (*libncmpi_iput_var1_schar)(int ncid, int varid, const MPI_Offset *start,
				const signed char *op, int *req);

int (*libncmpi_iput_var1_short)(int ncid, int varid, const MPI_Offset *start,
				const short *op, int *req);

int (*libncmpi_iput_var1_int)(int ncid, int varid, const MPI_Offset *start,
			      const int *op, int *req);

int (*libncmpi_iput_var1_float)(int ncid, int varid, const MPI_Offset *start,
				const float *op, int *req);

int (*libncmpi_iput_var1_double)(int ncid, int varid, const MPI_Offset *start,
				 const double *op, int *req);

int (*libncmpi_iput_var1_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const long long *op, int *req);

int (*libncmpi_iget_var1)(int ncid, int varid, const MPI_Offset *start, void *ip,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_iget_var1_schar)(int ncid, int varid, const MPI_Offset *start,
				signed char *ip, int *req);

int (*libncmpi_iget_var1_text)(int ncid, int varid, const MPI_Offset *start,
			       char *ip, int *req);

int (*libncmpi_iget_var1_short)(int ncid, int varid, const MPI_Offset *start,
				short *ip, int *req);

int (*libncmpi_iget_var1_int)(int ncid, int varid, const MPI_Offset *start,
			      int *ip, int *req);

int (*libncmpi_iget_var1_float)(int ncid, int varid, const MPI_Offset *start,
				float *ip, int *req);

int (*libncmpi_iget_var1_double)(int ncid, int varid, const MPI_Offset *start,
				 double *ip, int *req);

int (*libncmpi_iget_var1_longlong)(int ncid, int varid, const MPI_Offset *start,
				   long long *ip, int *req);

int (*libncmpi_bput_var1)(int ncid, int varid, const MPI_Offset *start, const void *op,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_bput_var1_text)(int ncid, int varid, const MPI_Offset *start,
			       const char *op, int *req);

int (*libncmpi_bput_var1_schar)(int ncid, int varid, const MPI_Offset *start,
				const signed char *op, int *req);

int (*libncmpi_bput_var1_short)(int ncid, int varid, const MPI_Offset *start,
				const short *op, int *req);

int (*libncmpi_bput_var1_int)(int ncid, int varid, const MPI_Offset *start,
			      const int *op, int *req);

int (*libncmpi_bput_var1_float)(int ncid, int varid, const MPI_Offset *start,
				const float *op, int *req);

int (*libncmpi_bput_var1_double)(int ncid, int varid, const MPI_Offset *start,
				 const double *op, int *req);

int (*libncmpi_bput_var1_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const long long *op, int *req);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_iput_var1_uchar)(int ncid, int varid, const MPI_Offset *start,
				const unsigned char *op, int *req);

int (*libncmpi_iput_var1_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const unsigned short *op, int *req);

int (*libncmpi_iput_var1_uint)(int ncid, int varid, const MPI_Offset *start,
			       const unsigned int *op, int *req);

int (*libncmpi_iput_var1_long)(int ncid, int varid, const MPI_Offset *start,
			       const long *ip, int *req);

int (*libncmpi_iput_var1_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const unsigned long long *op, int *req);

int (*libncmpi_iget_var1_uchar)(int ncid, int varid, const MPI_Offset *start,
				unsigned char *ip, int *req);

int (*libncmpi_iget_var1_ushort)(int ncid, int varid, const MPI_Offset *start,
				 unsigned short *ip, int *req);

int (*libncmpi_iget_var1_uint)(int ncid, int varid, const MPI_Offset *start,
			       unsigned int *ip, int *req);

int (*libncmpi_iget_var1_long)(int ncid, int varid, const MPI_Offset *start,
			       long *ip, int *req);

int (*libncmpi_iget_var1_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    unsigned long long *ip, int *req);

int (*libncmpi_bput_var1_uchar)(int ncid, int varid, const MPI_Offset *start,
				const unsigned char *op, int *req);

int (*libncmpi_bput_var1_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const unsigned short *op, int *req);

int (*libncmpi_bput_var1_uint)(int ncid, int varid, const MPI_Offset *start,
			       const unsigned int *op, int *req);

int (*libncmpi_bput_var1_long)(int ncid, int varid, const MPI_Offset *start,
			       const long *ip, int *req);

int (*libncmpi_bput_var1_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const unsigned long long *op, int *req);

/* End Skip Prototypes for Fortran binding */

/* End {iput,iget,bput}_var1 */

/* Begin {iput,iget,bput}_var */

int (*libncmpi_iput_var)(int ncid, int varid, const void *op, MPI_Offset bufcount,
			 MPI_Datatype buftype, int *req);

int (*libncmpi_iput_var_schar)(int ncid, int varid, const signed char *op, int *req);

int (*libncmpi_iput_var_text)(int ncid, int varid, const char *op, int *req);

int (*libncmpi_iput_var_short)(int ncid, int varid, const short *op, int *req);

int (*libncmpi_iput_var_int)(int ncid, int varid, const int *op, int *req);

int (*libncmpi_iput_var_float)(int ncid, int varid, const float *op, int *req);

int (*libncmpi_iput_var_double)(int ncid, int varid, const double *op, int *req);

int (*libncmpi_iput_var_longlong)(int ncid, int varid, const long long *op, int *req);

int (*libncmpi_iget_var)(int ncid, int varid, void *ip, MPI_Offset bufcount,
			 MPI_Datatype buftype, int *req);

int (*libncmpi_iget_var_schar)(int ncid, int varid, signed char *ip, int *req);

int (*libncmpi_iget_var_text)(int ncid, int varid, char *ip, int *req);

int (*libncmpi_iget_var_short)(int ncid, int varid, short *ip, int *req);

int (*libncmpi_iget_var_int)(int ncid, int varid, int *ip, int *req);

int (*libncmpi_iget_var_float)(int ncid, int varid, float *ip, int *req);

int (*libncmpi_iget_var_double)(int ncid, int varid, double *ip, int *req);

int (*libncmpi_iget_var_longlong)(int ncid, int varid, long long *ip, int *req);

int (*libncmpi_bput_var)(int ncid, int varid, const void *op, MPI_Offset bufcount,
			 MPI_Datatype buftype, int *req);

int (*libncmpi_bput_var_schar)(int ncid, int varid, const signed char *op, int *req);

int (*libncmpi_bput_var_text)(int ncid, int varid, const char *op, int *req);

int (*libncmpi_bput_var_short)(int ncid, int varid, const short *op, int *req);

int (*libncmpi_bput_var_int)(int ncid, int varid, const int *op, int *req);

int (*libncmpi_bput_var_float)(int ncid, int varid, const float *op, int *req);

int (*libncmpi_bput_var_double)(int ncid, int varid, const double *op, int *req);

int (*libncmpi_bput_var_longlong)(int ncid, int varid, const long long *op, int *req);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_iput_var_uchar)(int ncid, int varid, const unsigned char *op, int *req);

int (*libncmpi_iput_var_ushort)(int ncid, int varid, const unsigned short *op, int *req);

int (*libncmpi_iput_var_uint)(int ncid, int varid, const unsigned int *op, int *req);

int (*libncmpi_iput_var_long)(int ncid, int varid, const long *op, int *req);

int (*libncmpi_iput_var_ulonglong)(int ncid, int varid, const unsigned long long *op,
				   int *req);

int (*libncmpi_iget_var_uchar)(int ncid, int varid, unsigned char *ip, int *req);

int (*libncmpi_iget_var_ushort)(int ncid, int varid, unsigned short *ip, int *req);

int (*libncmpi_iget_var_uint)(int ncid, int varid, unsigned int *ip, int *req);

int (*libncmpi_iget_var_long)(int ncid, int varid, long *ip, int *req);

int (*libncmpi_iget_var_ulonglong)(int ncid, int varid, unsigned long long *ip, int *req);

int (*libncmpi_bput_var_uchar)(int ncid, int varid, const unsigned char *op, int *req);

int (*libncmpi_bput_var_ushort)(int ncid, int varid, const unsigned short *op, int *req);

int (*libncmpi_bput_var_uint)(int ncid, int varid, const unsigned int *op, int *req);

int (*libncmpi_bput_var_long)(int ncid, int varid, const long *op, int *req);

int (*libncmpi_bput_var_ulonglong)(int ncid, int varid, const unsigned long long *op,
				   int *req);

/* End Skip Prototypes for Fortran binding */

/* End {iput,iget,bput}_var */

/* Begin {iput,iget,bput}_vara */

int (*libncmpi_iput_vara)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const void *op,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_iput_vara_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const signed char *op, int *req);

int (*libncmpi_iput_vara_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const char *op, int *req);

int (*libncmpi_iput_vara_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const short *op, int *req);

int (*libncmpi_iput_vara_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const int *op, int *req);

int (*libncmpi_iput_vara_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const float *op, int *req);

int (*libncmpi_iput_vara_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const double *op, int *req);

int (*libncmpi_iput_vara_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const long long *op, int *req);

int (*libncmpi_iget_vara)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, void *ip, MPI_Offset bufcount,
			  MPI_Datatype buftype, int *req);

int (*libncmpi_iget_vara_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, signed char *ip, int *req);

int (*libncmpi_iget_vara_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, char *ip, int *req);

int (*libncmpi_iget_vara_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, short *ip, int *req);

int (*libncmpi_iget_vara_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, int *ip, int *req);

int (*libncmpi_iget_vara_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, float *ip, int *req);

int (*libncmpi_iget_vara_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, double *ip, int *req);

int (*libncmpi_iget_vara_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, long long *ip, int *req);

int (*libncmpi_bput_vara)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const void *op,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_bput_vara_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const signed char *op, int *req);

int (*libncmpi_bput_vara_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const char *op, int *req);

int (*libncmpi_bput_vara_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const short *op, int *req);

int (*libncmpi_bput_vara_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const int *op, int *req);

int (*libncmpi_bput_vara_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const float *op, int *req);

int (*libncmpi_bput_vara_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const double *op, int *req);

int (*libncmpi_bput_vara_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const long long *op, int *req);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_iput_vara_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const unsigned char *op, int *req);

int (*libncmpi_iput_vara_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const unsigned short *op, int *req);

int (*libncmpi_iput_vara_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const unsigned int *op, int *req);

int (*libncmpi_iput_vara_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const long *op, int *req);

int (*libncmpi_iput_vara_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const unsigned long long *op,
				    int *req);

int (*libncmpi_iget_vara_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, unsigned char *ip, int *req);

int (*libncmpi_iget_vara_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, unsigned short *ip, int *req);

int (*libncmpi_iget_vara_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, unsigned int *ip, int *req);

int (*libncmpi_iget_vara_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, long *ip, int *req);

int (*libncmpi_iget_vara_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, unsigned long long *ip, int *req);

int (*libncmpi_bput_vara_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const unsigned char *op, int *req);

int (*libncmpi_bput_vara_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const unsigned short *op, int *req);

int (*libncmpi_bput_vara_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const unsigned int *op, int *req);

int (*libncmpi_bput_vara_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const long *op, int *req);

int (*libncmpi_bput_vara_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const unsigned long long *op,
				    int *req);

/* End Skip Prototypes for Fortran binding */

/* End {iput,iget,bput}_vara */

/* Begin {iput,iget,bput}_vars */

int (*libncmpi_iput_vars)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const MPI_Offset *stride,
			  const void *op, MPI_Offset bufcount,
			  MPI_Datatype buftype, int *req);

int (*libncmpi_iput_vars_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const signed char *op, int *req);

int (*libncmpi_iput_vars_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const char *op, int *req);

int (*libncmpi_iput_vars_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const short *op, int *req);

int (*libncmpi_iput_vars_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const int *op, int *req);

int (*libncmpi_iput_vars_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const float *op, int *req);

int (*libncmpi_iput_vars_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const double *op, int *req);

int (*libncmpi_iput_vars_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const long long *op, int *req);

int (*libncmpi_iget_vars)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const MPI_Offset *stride, void *ip,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_iget_vars_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				signed char *ip, int *req);

int (*libncmpi_iget_vars_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       char *ip, int *req);

int (*libncmpi_iget_vars_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				short *ip, int *req);

int (*libncmpi_iget_vars_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      int *ip, int *req);

int (*libncmpi_iget_vars_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				float *ip, int *req);

int (*libncmpi_iget_vars_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 double *ip, int *req);

int (*libncmpi_iget_vars_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   long long *ip, int *req);

int (*libncmpi_bput_vars)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const MPI_Offset *stride,
			  const void *op, MPI_Offset bufcount,
			  MPI_Datatype buftype, int *req);

int (*libncmpi_bput_vars_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const signed char *op, int *req);

int (*libncmpi_bput_vars_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const char *op, int *req);

int (*libncmpi_bput_vars_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const short *op, int *req);

int (*libncmpi_bput_vars_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const int *op, int *req);

int (*libncmpi_bput_vars_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const float *op, int *req);

int (*libncmpi_bput_vars_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const double *op, int *req);

int (*libncmpi_bput_vars_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const long long *op, int *req);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_iput_vars_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const unsigned char *op, int *req);

int (*libncmpi_iput_vars_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const unsigned short *op, int *req);

int (*libncmpi_iput_vars_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const unsigned int *op, int *req);

int (*libncmpi_iput_vars_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const long *op, int *req);

int (*libncmpi_iput_vars_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const unsigned long long *op, int *req);

int (*libncmpi_iget_vars_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				unsigned char *ip, int *req);

int (*libncmpi_iget_vars_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 unsigned short *ip, int *req);

int (*libncmpi_iget_vars_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       unsigned int *ip, int *req);

int (*libncmpi_iget_vars_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       long *ip, int *req);

int (*libncmpi_iget_vars_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    unsigned long long *ip, int *req);

int (*libncmpi_bput_vars_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const unsigned char *op, int *req);

int (*libncmpi_bput_vars_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const unsigned short *op, int *req);

int (*libncmpi_bput_vars_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const unsigned int *op, int *req);

int (*libncmpi_bput_vars_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const long *op, int *req);

int (*libncmpi_bput_vars_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const unsigned long long *op, int *req);

/* End Skip Prototypes for Fortran binding */

/* End {iput,iget,bput}_vars */

/* Begin {iput,iget,bput}_varm */

int (*libncmpi_iput_varm)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const MPI_Offset *stride,
			  const MPI_Offset *imap, const void *op,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_iput_varm_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const signed char *op,
				int *req);

int (*libncmpi_iput_varm_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const char *op, int *req);

int (*libncmpi_iput_varm_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const short *op, int *req);

int (*libncmpi_iput_varm_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, const int *op, int *req);

int (*libncmpi_iput_varm_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const float *op, int *req);

int (*libncmpi_iput_varm_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, const double *op, int *req);

int (*libncmpi_iput_varm_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, const long long *op, int *req);

int (*libncmpi_iget_varm)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const MPI_Offset *stride,
			  const MPI_Offset *imap, void *ip, MPI_Offset bufcount,
			  MPI_Datatype buftype, int *req);

int (*libncmpi_iget_varm_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, signed char *ip, int *req);

int (*libncmpi_iget_varm_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, char *ip, int *req);

int (*libncmpi_iget_varm_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, short *ip, int *req);

int (*libncmpi_iget_varm_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, int *ip, int *req);

int (*libncmpi_iget_varm_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, float *ip, int *req);

int (*libncmpi_iget_varm_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, double *ip, int *req);

int (*libncmpi_iget_varm_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, long long *ip, int *req);

int (*libncmpi_bput_varm)(int ncid, int varid, const MPI_Offset *start,
			  const MPI_Offset *count, const MPI_Offset *stride,
			  const MPI_Offset *imap, const void *op,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_bput_varm_schar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const signed char *op, int *req);

int (*libncmpi_bput_varm_text)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const char *op, int *req);

int (*libncmpi_bput_varm_short)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const short *op, int *req);

int (*libncmpi_bput_varm_int)(int ncid, int varid, const MPI_Offset *start,
			      const MPI_Offset *count, const MPI_Offset *stride,
			      const MPI_Offset *imap, const int *op, int *req);

int (*libncmpi_bput_varm_float)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const float *op, int *req);

int (*libncmpi_bput_varm_double)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, const double *op, int *req);

int (*libncmpi_bput_varm_longlong)(int ncid, int varid, const MPI_Offset *start,
				   const MPI_Offset *count, const MPI_Offset *stride,
				   const MPI_Offset *imap, const long long *op, int *req);

/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_iput_varm_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const unsigned char *op, int *req);

int (*libncmpi_iput_varm_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, const unsigned short *op, int *req);

int (*libncmpi_iput_varm_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const unsigned int *op, int *req);

int (*libncmpi_iput_varm_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const long *op, int *req);

int (*libncmpi_iput_varm_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const MPI_Offset *imap, const unsigned long long *op,
				    int *req);

int (*libncmpi_iget_varm_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, unsigned char *ip, int *req);

int (*libncmpi_iget_varm_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, unsigned short *ip, int *req);

int (*libncmpi_iget_varm_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, unsigned int *ip, int *req);

int (*libncmpi_iget_varm_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, long *ip, int *req);

int (*libncmpi_iget_varm_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const MPI_Offset *imap, unsigned long long *ip, int *req);

int (*libncmpi_bput_varm_uchar)(int ncid, int varid, const MPI_Offset *start,
				const MPI_Offset *count, const MPI_Offset *stride,
				const MPI_Offset *imap, const unsigned char *op,
				int *req);

int (*libncmpi_bput_varm_ushort)(int ncid, int varid, const MPI_Offset *start,
				 const MPI_Offset *count, const MPI_Offset *stride,
				 const MPI_Offset *imap, const unsigned short *op,
				 int *req);

int (*libncmpi_bput_varm_uint)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const unsigned int *op,
			       int *req);

int (*libncmpi_bput_varm_long)(int ncid, int varid, const MPI_Offset *start,
			       const MPI_Offset *count, const MPI_Offset *stride,
			       const MPI_Offset *imap, const long *op, int *req);

int (*libncmpi_bput_varm_ulonglong)(int ncid, int varid, const MPI_Offset *start,
				    const MPI_Offset *count, const MPI_Offset *stride,
				    const MPI_Offset *imap, const unsigned long long *op,
				    int *req);

/* End Skip Prototypes for Fortran binding */

/* End {iput,iget,bput}_varm */

/* Begin of nonblocking {iput,iget}_varn{kind} */

int (*libncmpi_iput_varn)(int ncid, int varid, int num, MPI_Offset* const *starts,
			  MPI_Offset* const *counts, const void *op,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_iget_varn)(int ncid, int varid, int num, MPI_Offset* const *starts,
			  MPI_Offset* const *counts,  void *op, MPI_Offset bufcount,
			  MPI_Datatype buftype, int *req);

int (*libncmpi_iput_varn_text)(int ncid, int varid, int num,
			       MPI_Offset* const *starts,
			       MPI_Offset* const *counts, const char *op, int *req);

int (*libncmpi_iput_varn_schar)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const signed char *op, int *req);

int (*libncmpi_iput_varn_short)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const short *op, int *req);

int (*libncmpi_iput_varn_int)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      const int *op, int *req);

int (*libncmpi_iput_varn_float)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const float *op, int *req);

int (*libncmpi_iput_varn_double)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 const double *op, int *req);

int (*libncmpi_iput_varn_longlong)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   const long long *op, int *req);


/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_iput_varn_uchar)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const unsigned char *op, int *req);

int (*libncmpi_iput_varn_ushort)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 const unsigned short *op, int *req);

int (*libncmpi_iput_varn_uint)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const unsigned int *op, int *req);

int (*libncmpi_iput_varn_long)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const long *op, int *req);

int (*libncmpi_iput_varn_ulonglong)(int ncid, int varid, int num,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    const unsigned long long *op, int *req);

/* End Skip Prototypes for Fortran binding */

int (*libncmpi_iget_varn_text)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       char *ip, int *req);

int (*libncmpi_iget_varn_schar)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				signed char *ip, int *req);

int (*libncmpi_iget_varn_short)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				short *ip, int *req);

int (*libncmpi_iget_varn_int)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      int *ip, int *req);

int (*libncmpi_iget_varn_float)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				float *ip, int *req);

int (*libncmpi_iget_varn_double)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 double *ip, int *req);

int (*libncmpi_iget_varn_longlong)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   long long *ip, int *req);


/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_iget_varn_uchar)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				unsigned char *ip, int *req);

int (*libncmpi_iget_varn_ushort)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 unsigned short *ip, int *req);

int (*libncmpi_iget_varn_uint)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       unsigned int *ip, int *req);

int (*libncmpi_iget_varn_long)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       long *ip, int *req);

int (*libncmpi_iget_varn_ulonglong)(int ncid, int varid, int num,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    unsigned long long *ip, int *req);

/* End Skip Prototypes for Fortran binding */

/* End of {iput,iget}_varn{kind} */

/* Begin of nonblocking bput_varn{kind} */

int (*libncmpi_bput_varn)(int ncid, int varid, int num, MPI_Offset* const *starts,
			  MPI_Offset* const *counts, const void *op,
			  MPI_Offset bufcount, MPI_Datatype buftype, int *req);

int (*libncmpi_bput_varn_text)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const char *op, int *req);

int (*libncmpi_bput_varn_schar)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const signed char *op, int *req);

int (*libncmpi_bput_varn_short)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const short *op, int *req);

int (*libncmpi_bput_varn_int)(int ncid, int varid, int num,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      const int *op, int *req);

int (*libncmpi_bput_varn_float)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const float *op, int *req);

int (*libncmpi_bput_varn_double)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 const double *op, int *req);

int (*libncmpi_bput_varn_longlong)(int ncid, int varid, int num,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   const long long *op, int *req);


/* Begin Skip Prototypes for Fortran binding */
/* skip types: uchar, ubyte, ushort, uint, long, ulonglong string */

int (*libncmpi_bput_varn_uchar)(int ncid, int varid, int num,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				const unsigned char *op, int *req);

int (*libncmpi_bput_varn_ushort)(int ncid, int varid, int num,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 const unsigned short *op, int *req);

int (*libncmpi_bput_varn_uint)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const unsigned int *op, int *req);

int (*libncmpi_bput_varn_long)(int ncid, int varid, int num,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       const long *op, int *req);

int (*libncmpi_bput_varn_ulonglong)(int ncid, int varid, int num,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    const unsigned long long *op, int *req);

/* End Skip Prototypes for Fortran binding */

/* End of bput_varn{kind} */

/* End non-blocking data access functions */

/* Begin Skip Prototypes for Fortran binding */
/* skip all mput/mget APIs as Fortran cannot handle array of buffers */

int (*libncmpi_mput_var)(int ncid, int num, int *varids, void* const *buf,
			 const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);

int (*libncmpi_mput_var_all)(int ncid, int num, int *varids, void* const *buf,
			     const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);

int (*libncmpi_mput_var_text)(int ncid, int num, int *varids, char* const *buf);
int (*libncmpi_mput_var_text_all)(int ncid, int num, int *varids, char* const *buf);

int (*libncmpi_mput_var_schar)(int ncid, int num, int *varids, signed char* const *buf);
int (*libncmpi_mput_var_schar_all)(int ncid, int num, int *varids, signed char* const *buf);

int (*libncmpi_mput_var_uchar)(int ncid, int num, int *varids, unsigned char* const *buf);
int (*libncmpi_mput_var_uchar_all)(int ncid, int num, int *varids, unsigned char* const *buf);

int (*libncmpi_mput_var_short)(int ncid, int num, int *varids, short* const *buf);
int (*libncmpi_mput_var_short_all)(int ncid, int num, int *varids, short* const *buf);

int (*libncmpi_mput_var_ushort)(int ncid, int num, int *varids, unsigned short* const *buf);
int (*libncmpi_mput_var_ushort_all)(int ncid, int num, int *varids,
				    unsigned short* const *buf);

int (*libncmpi_mput_var_int)(int ncid, int num, int *varids, int* const *buf);
int (*libncmpi_mput_var_int_all)(int ncid, int num, int *varids, int* const *buf);

int (*libncmpi_mput_var_uint)(int ncid, int num, int *varids, unsigned int* const *buf);
int (*libncmpi_mput_var_uint_all)(int ncid, int num, int *varids, unsigned int* const *buf);

int (*libncmpi_mput_var_long)(int ncid, int num, int *varids, long* const *buf);
int (*libncmpi_mput_var_long_all)(int ncid, int num, int *varids, long* const *buf);

int (*libncmpi_mput_var_float)(int ncid, int num, int *varids, float* const *buf);
int (*libncmpi_mput_var_float_all)(int ncid, int num, int *varids, float* const *buf);

int (*libncmpi_mput_var_double)(int ncid, int num, int *varids, double* const *buf);
int (*libncmpi_mput_var_double_all)(int ncid, int num, int *varids, double* const *buf);

int (*libncmpi_mput_var_longlong)(int ncid, int num, int *varids, long long* const *buf);
int (*libncmpi_mput_var_longlong_all)(int ncid, int num, int *varids, long long* const *buf);

int (*libncmpi_mput_var_ulonglong)(int ncid, int num, int *varids,
				   unsigned long long* const *buf);
int (*libncmpi_mput_var_ulonglong_all)(int ncid, int num, int *varids,
				       unsigned long long* const *buf);

int (*libncmpi_mput_var1)(int ncid, int num, int *varids,
			  MPI_Offset* const *starts, void* const *buf,
			  const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);
int (*libncmpi_mput_var1_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, void* const *buf,
			      const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);

int (*libncmpi_mput_var1_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, char* const *buf);
int (*libncmpi_mput_var1_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, char* const *buf);

int (*libncmpi_mput_var1_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, signed char* const *buf);
int (*libncmpi_mput_var1_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, signed char* const *buf);

int (*libncmpi_mput_var1_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, unsigned char* const *buf);
int (*libncmpi_mput_var1_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, unsigned char* const *buf);

int (*libncmpi_mput_var1_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, short* const *buf);
int (*libncmpi_mput_var1_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, short* const *buf);

int (*libncmpi_mput_var1_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, unsigned short* const *buf);
int (*libncmpi_mput_var1_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, unsigned short* const *buf);

int (*libncmpi_mput_var1_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, int* const *buf);
int (*libncmpi_mput_var1_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, int* const *buf);

int (*libncmpi_mput_var1_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, unsigned int* const *buf);
int (*libncmpi_mput_var1_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, unsigned int* const *buf);

int (*libncmpi_mput_var1_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, long* const *buf);
int (*libncmpi_mput_var1_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, long* const *buf);

int (*libncmpi_mput_var1_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, float* const *buf);
int (*libncmpi_mput_var1_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, float* const *buf);

int (*libncmpi_mput_var1_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, double* const *buf);
int (*libncmpi_mput_var1_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, double* const *buf);

int (*libncmpi_mput_var1_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, long long* const *buf);
int (*libncmpi_mput_var1_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, long long* const *buf);

int (*libncmpi_mput_var1_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, unsigned long long* const *buf);
int (*libncmpi_mput_var1_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, unsigned long long* const *buf);


int (*libncmpi_mput_vara)(int ncid, int num, int *varids, MPI_Offset* const *starts,
			  MPI_Offset* const *counts, void* const *buf,
			  const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);
int (*libncmpi_mput_vara_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      void* const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);

int (*libncmpi_mput_vara_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       char* const *buf);
int (*libncmpi_mput_vara_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   char* const *buf);

int (*libncmpi_mput_vara_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				signed char* const *buf);
int (*libncmpi_mput_vara_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    signed char* const *buf);

int (*libncmpi_mput_vara_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				unsigned char* const *buf);
int (*libncmpi_mput_vara_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    unsigned char* const *buf);

int (*libncmpi_mput_vara_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				short* const *buf);
int (*libncmpi_mput_vara_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    short* const *buf);

int (*libncmpi_mput_vara_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 unsigned short* const *buf);
int (*libncmpi_mput_vara_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     unsigned short* const *buf);

int (*libncmpi_mput_vara_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      int* const *buf);
int (*libncmpi_mput_vara_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  int* const *buf);

int (*libncmpi_mput_vara_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       unsigned int* const *buf);
int (*libncmpi_mput_vara_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   unsigned int* const *buf);

int (*libncmpi_mput_vara_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       long* const *buf);
int (*libncmpi_mput_vara_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   long* const *buf);

int (*libncmpi_mput_vara_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				float* const *buf);
int (*libncmpi_mput_vara_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    float* const *buf);

int (*libncmpi_mput_vara_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 double* const *buf);
int (*libncmpi_mput_vara_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     double* const *buf);

int (*libncmpi_mput_vara_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   long long* const *buf);
int (*libncmpi_mput_vara_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       long long* const *buf);

int (*libncmpi_mput_vara_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    unsigned long long* const *buf);
int (*libncmpi_mput_vara_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, MPI_Offset* const *counts,
					unsigned long long* const *buf);


int (*libncmpi_mput_vars)(int ncid, int num, int *varids,
			  MPI_Offset* const *starts, MPI_Offset* const *counts,
			  MPI_Offset* const *strides, void* const *buf,
			  const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);

int (*libncmpi_mput_vars_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, void* const *buf,
			      const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);

int (*libncmpi_mput_vars_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, char* const *buf);
int (*libncmpi_mput_vars_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, char* const *buf);

int (*libncmpi_mput_vars_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, signed char* const *buf);
int (*libncmpi_mput_vars_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, signed char* const *buf);

int (*libncmpi_mput_vars_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, unsigned char* const *buf);
int (*libncmpi_mput_vars_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, unsigned char* const *buf);

int (*libncmpi_mput_vars_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, short* const *buf);
int (*libncmpi_mput_vars_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, short* const *buf);

int (*libncmpi_mput_vars_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, unsigned short* const *buf);
int (*libncmpi_mput_vars_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, unsigned short* const *buf);

int (*libncmpi_mput_vars_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, int* const *buf);
int (*libncmpi_mput_vars_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  MPI_Offset* const *strides, int* const *buf);

int (*libncmpi_mput_vars_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, unsigned int* const *buf);
int (*libncmpi_mput_vars_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, unsigned int* const *buf);

int (*libncmpi_mput_vars_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, long* const *buf);
int (*libncmpi_mput_vars_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, long* const *buf);

int (*libncmpi_mput_vars_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, float* const *buf);
int (*libncmpi_mput_vars_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, float* const *buf);

int (*libncmpi_mput_vars_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, double* const *buf);
int (*libncmpi_mput_vars_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, double* const *buf);

int (*libncmpi_mput_vars_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, long long* const *buf);
int (*libncmpi_mput_vars_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       MPI_Offset* const *strides, long long* const *buf);

int (*libncmpi_mput_vars_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, unsigned long long* const *buf);
int (*libncmpi_mput_vars_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, MPI_Offset* const *counts,
					MPI_Offset* const *strides, unsigned long long* const *buf);

int (*libncmpi_mput_varm)(int ncid, int num, int *varids,
			  MPI_Offset* const *starts, MPI_Offset* const *counts,
			  MPI_Offset* const *strides, MPI_Offset* const *imaps,
			  void* const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);
int (*libncmpi_mput_varm_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, MPI_Offset* const *imaps,
			      void* const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]);

int (*libncmpi_mput_varm_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, MPI_Offset* const *imaps,
			       char* const *buf);
int (*libncmpi_mput_varm_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   char* const *buf);

int (*libncmpi_mput_varm_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				signed char* const *buf);
int (*libncmpi_mput_varm_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    signed char* const *buf);

int (*libncmpi_mput_varm_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				unsigned char* const *buf);
int (*libncmpi_mput_varm_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    unsigned char* const *buf);

int (*libncmpi_mput_varm_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				short* const *buf);
int (*libncmpi_mput_varm_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    short* const *buf);

int (*libncmpi_mput_varm_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, MPI_Offset* const *imaps,
				 unsigned short* const *buf);
int (*libncmpi_mput_varm_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, MPI_Offset* const *imaps,
				     unsigned short* const *buf);

int (*libncmpi_mput_varm_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, MPI_Offset* const *imaps,
			      int* const *buf);
int (*libncmpi_mput_varm_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  MPI_Offset* const *strides, MPI_Offset* const *imaps,
				  int* const *buf);

int (*libncmpi_mput_varm_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, MPI_Offset* const *imaps,
			       unsigned int* const *buf);
int (*libncmpi_mput_varm_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   unsigned int* const *buf);

int (*libncmpi_mput_varm_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, MPI_Offset* const *imaps,
			       long* const *buf);
int (*libncmpi_mput_varm_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   long* const *buf);

int (*libncmpi_mput_varm_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				float* const *buf);
int (*libncmpi_mput_varm_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    float* const *buf);

int (*libncmpi_mput_varm_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, MPI_Offset* const *imaps,
				 double* const *buf);
int (*libncmpi_mput_varm_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, MPI_Offset* const *imaps,
				     double* const *buf);

int (*libncmpi_mput_varm_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   long long* const *buf);
int (*libncmpi_mput_varm_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       MPI_Offset* const *strides, MPI_Offset* const *imaps,
				       long long* const *buf);

int (*libncmpi_mput_varm_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    unsigned long long* const *buf);
int (*libncmpi_mput_varm_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, MPI_Offset* const *counts,
					MPI_Offset* const *strides, MPI_Offset* const *imaps,
					unsigned long long* const *buf);

int (*libncmpi_mget_var)(int ncid, int num, int *varids, void *bufs[],
			 const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);

int (*libncmpi_mget_var_all)(int ncid, int num, int *varids, void *bufs[],
			     const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);

int (*libncmpi_mget_var_text)(int ncid, int num, int *varids, char *bufs[]);
int (*libncmpi_mget_var_text_all)(int ncid, int num, int *varids, char *bufs[]);

int (*libncmpi_mget_var_schar)(int ncid, int num, int *varids, signed char *bufs[]);
int (*libncmpi_mget_var_schar_all)(int ncid, int num, int *varids, signed char *bufs[]);

int (*libncmpi_mget_var_uchar)(int ncid, int num, int *varids, unsigned char *bufs[]);
int (*libncmpi_mget_var_uchar_all)(int ncid, int num, int *varids, unsigned char *bufs[]);

int (*libncmpi_mget_var_short)(int ncid, int num, int *varids, short *bufs[]);
int (*libncmpi_mget_var_short_all)(int ncid, int num, int *varids, short *bufs[]);

int (*libncmpi_mget_var_ushort)(int ncid, int num, int *varids, unsigned short *bufs[]);
int (*libncmpi_mget_var_ushort_all)(int ncid, int num, int *varids,
				    unsigned short *bufs[]);

int (*libncmpi_mget_var_int)(int ncid, int num, int *varids, int *bufs[]);
int (*libncmpi_mget_var_int_all)(int ncid, int num, int *varids, int *bufs[]);

int (*libncmpi_mget_var_uint)(int ncid, int num, int *varids, unsigned int *bufs[]);
int (*libncmpi_mget_var_uint_all)(int ncid, int num, int *varids, unsigned int *bufs[]);

int (*libncmpi_mget_var_long)(int ncid, int num, int *varids, long *bufs[]);
int (*libncmpi_mget_var_long_all)(int ncid, int num, int *varids, long *bufs[]);

int (*libncmpi_mget_var_float)(int ncid, int num, int *varids, float *bufs[]);
int (*libncmpi_mget_var_float_all)(int ncid, int num, int *varids, float *bufs[]);

int (*libncmpi_mget_var_double)(int ncid, int num, int *varids, double *bufs[]);
int (*libncmpi_mget_var_double_all)(int ncid, int num, int *varids, double *bufs[]);

int (*libncmpi_mget_var_longlong)(int ncid, int num, int *varids, long long *bufs[]);
int (*libncmpi_mget_var_longlong_all)(int ncid, int num, int *varids, long long *bufs[]);

int (*libncmpi_mget_var_ulonglong)(int ncid, int num, int *varids,
				   unsigned long long *bufs[]);
int (*libncmpi_mget_var_ulonglong_all)(int ncid, int num, int *varids,
				       unsigned long long *bufs[]);

int (*libncmpi_mget_var1)(int ncid, int num, int *varids,
			  MPI_Offset* const *starts, void *bufs[],
			  const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);
int (*libncmpi_mget_var1_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, void *bufs[],
			      const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);

int (*libncmpi_mget_var1_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, char *bufs[]);
int (*libncmpi_mget_var1_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, char *bufs[]);

int (*libncmpi_mget_var1_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, signed char *bufs[]);
int (*libncmpi_mget_var1_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, signed char *bufs[]);

int (*libncmpi_mget_var1_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, unsigned char *bufs[]);
int (*libncmpi_mget_var1_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, unsigned char *bufs[]);

int (*libncmpi_mget_var1_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, short *bufs[]);
int (*libncmpi_mget_var1_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, short *bufs[]);

int (*libncmpi_mget_var1_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, unsigned short *bufs[]);
int (*libncmpi_mget_var1_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, unsigned short *bufs[]);

int (*libncmpi_mget_var1_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, int *bufs[]);
int (*libncmpi_mget_var1_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, int *bufs[]);

int (*libncmpi_mget_var1_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, unsigned int *bufs[]);
int (*libncmpi_mget_var1_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, unsigned int *bufs[]);

int (*libncmpi_mget_var1_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, long *bufs[]);
int (*libncmpi_mget_var1_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, long *bufs[]);

int (*libncmpi_mget_var1_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, float *bufs[]);
int (*libncmpi_mget_var1_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, float *bufs[]);

int (*libncmpi_mget_var1_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, double *bufs[]);
int (*libncmpi_mget_var1_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, double *bufs[]);

int (*libncmpi_mget_var1_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, long long *bufs[]);
int (*libncmpi_mget_var1_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, long long *bufs[]);

int (*libncmpi_mget_var1_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, unsigned long long *bufs[]);
int (*libncmpi_mget_var1_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, unsigned long long *bufs[]);


int (*libncmpi_mget_vara)(int ncid, int num, int *varids,
			  MPI_Offset* const *starts, MPI_Offset* const *counts,
			  void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);
int (*libncmpi_mget_vara_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);

int (*libncmpi_mget_vara_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       char *bufs[]);
int (*libncmpi_mget_vara_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   char *bufs[]);

int (*libncmpi_mget_vara_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				signed char *bufs[]);
int (*libncmpi_mget_vara_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    signed char *bufs[]);

int (*libncmpi_mget_vara_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				unsigned char *bufs[]);
int (*libncmpi_mget_vara_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    unsigned char *bufs[]);

int (*libncmpi_mget_vara_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				short *bufs[]);
int (*libncmpi_mget_vara_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    short *bufs[]);

int (*libncmpi_mget_vara_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 unsigned short *bufs[]);
int (*libncmpi_mget_vara_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     unsigned short *bufs[]);

int (*libncmpi_mget_vara_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      int *bufs[]);
int (*libncmpi_mget_vara_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  int *bufs[]);

int (*libncmpi_mget_vara_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       unsigned int *bufs[]);
int (*libncmpi_mget_vara_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   unsigned int *bufs[]);

int (*libncmpi_mget_vara_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       long *bufs[]);
int (*libncmpi_mget_vara_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   long *bufs[]);

int (*libncmpi_mget_vara_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				float *bufs[]);
int (*libncmpi_mget_vara_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    float *bufs[]);

int (*libncmpi_mget_vara_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 double *bufs[]);
int (*libncmpi_mget_vara_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     double *bufs[]);

int (*libncmpi_mget_vara_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   long long *bufs[]);
int (*libncmpi_mget_vara_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       long long *bufs[]);

int (*libncmpi_mget_vara_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    unsigned long long *bufs[]);
int (*libncmpi_mget_vara_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, MPI_Offset* const *counts,
					unsigned long long *bufs[]);

int (*libncmpi_mget_vars)(int ncid, int num, int *varids,
			  MPI_Offset* const *starts, MPI_Offset* const *counts,
			  MPI_Offset* const *strides, void *bufs[],
			  const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);
int (*libncmpi_mget_vars_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, void *bufs[],
			      const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);

int (*libncmpi_mget_vars_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, char *bufs[]);
int (*libncmpi_mget_vars_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, char *bufs[]);

int (*libncmpi_mget_vars_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, signed char *bufs[]);
int (*libncmpi_mget_vars_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, signed char *bufs[]);

int (*libncmpi_mget_vars_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, unsigned char *bufs[]);
int (*libncmpi_mget_vars_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, unsigned char *bufs[]);

int (*libncmpi_mget_vars_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, short *bufs[]);
int (*libncmpi_mget_vars_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, short *bufs[]);

int (*libncmpi_mget_vars_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, unsigned short *bufs[]);
int (*libncmpi_mget_vars_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, unsigned short *bufs[]);

int (*libncmpi_mget_vars_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, int *bufs[]);
int (*libncmpi_mget_vars_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  MPI_Offset* const *strides, int *bufs[]);

int (*libncmpi_mget_vars_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, unsigned int *bufs[]);
int (*libncmpi_mget_vars_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, unsigned int *bufs[]);

int (*libncmpi_mget_vars_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, long *bufs[]);
int (*libncmpi_mget_vars_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, long *bufs[]);

int (*libncmpi_mget_vars_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, float *bufs[]);
int (*libncmpi_mget_vars_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, float *bufs[]);

int (*libncmpi_mget_vars_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, double *bufs[]);
int (*libncmpi_mget_vars_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, double *bufs[]);

int (*libncmpi_mget_vars_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, long long *bufs[]);
int (*libncmpi_mget_vars_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       MPI_Offset* const *strides, long long *bufs[]);

int (*libncmpi_mget_vars_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, unsigned long long *bufs[]);
int (*libncmpi_mget_vars_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, MPI_Offset* const *counts,
					MPI_Offset* const *strides, unsigned long long *bufs[]);

int (*libncmpi_mget_varm)(int ncid, int num, int *varids,
			  MPI_Offset* const *starts, MPI_Offset* const *counts,
			  MPI_Offset* const *strides, MPI_Offset* const *imaps,
			  void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);

int (*libncmpi_mget_varm_all)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, MPI_Offset* const *imaps,
			      void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes);

int (*libncmpi_mget_varm_text)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, MPI_Offset* const *imaps,
			       char *bufs[]);
int (*libncmpi_mget_varm_text_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   char *bufs[]);

int (*libncmpi_mget_varm_schar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				signed char *bufs[]);
int (*libncmpi_mget_varm_schar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    signed char *bufs[]);

int (*libncmpi_mget_varm_uchar)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				unsigned char *bufs[]);
int (*libncmpi_mget_varm_uchar_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    unsigned char *bufs[]);

int (*libncmpi_mget_varm_short)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				short *bufs[]);
int (*libncmpi_mget_varm_short_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    short *bufs[]);

int (*libncmpi_mget_varm_ushort)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, MPI_Offset* const *imaps,
				 unsigned short *bufs[]);
int (*libncmpi_mget_varm_ushort_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, MPI_Offset* const *imaps,
				     unsigned short *bufs[]);

int (*libncmpi_mget_varm_int)(int ncid, int num, int *varids,
			      MPI_Offset* const *starts, MPI_Offset* const *counts,
			      MPI_Offset* const *strides, MPI_Offset* const *imaps,
			      int *bufs[]);
int (*libncmpi_mget_varm_int_all)(int ncid, int num, int *varids,
				  MPI_Offset* const *starts, MPI_Offset* const *counts,
				  MPI_Offset* const *strides, MPI_Offset* const *imaps,
				  int *bufs[]);

int (*libncmpi_mget_varm_uint)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, MPI_Offset* const *imaps,
			       unsigned int *bufs[]);
int (*libncmpi_mget_varm_uint_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   unsigned int *bufs[]);

int (*libncmpi_mget_varm_long)(int ncid, int num, int *varids,
			       MPI_Offset* const *starts, MPI_Offset* const *counts,
			       MPI_Offset* const *strides, MPI_Offset* const *imaps,
			       long *bufs[]);
int (*libncmpi_mget_varm_long_all)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   long *bufs[]);

int (*libncmpi_mget_varm_float)(int ncid, int num, int *varids,
				MPI_Offset* const *starts, MPI_Offset* const *counts,
				MPI_Offset* const *strides, MPI_Offset* const *imaps,
				float *bufs[]);
int (*libncmpi_mget_varm_float_all)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    float *bufs[]);

int (*libncmpi_mget_varm_double)(int ncid, int num, int *varids,
				 MPI_Offset* const *starts, MPI_Offset* const *counts,
				 MPI_Offset* const *strides, MPI_Offset* const *imaps,
				 double *bufs[]);
int (*libncmpi_mget_varm_double_all)(int ncid, int num, int *varids,
				     MPI_Offset* const *starts, MPI_Offset* const *counts,
				     MPI_Offset* const *strides, MPI_Offset* const *imaps,
				     double *bufs[]);

int (*libncmpi_mget_varm_longlong)(int ncid, int num, int *varids,
				   MPI_Offset* const *starts, MPI_Offset* const *counts,
				   MPI_Offset* const *strides, MPI_Offset* const *imaps,
				   long long *bufs[]);
int (*libncmpi_mget_varm_longlong_all)(int ncid, int num, int *varids,
				       MPI_Offset* const *starts, MPI_Offset* const *counts,
				       MPI_Offset* const *strides, MPI_Offset* const *imaps,
				       long long *bufs[]);

int (*libncmpi_mget_varm_ulonglong)(int ncid, int num, int *varids,
				    MPI_Offset* const *starts, MPI_Offset* const *counts,
				    MPI_Offset* const *strides, MPI_Offset* const *imaps,
				    unsigned long long *bufs[]);
int (*libncmpi_mget_varm_ulonglong_all)(int ncid, int num, int *varids,
					MPI_Offset* const *starts, MPI_Offset* const *counts,
					MPI_Offset* const *strides, MPI_Offset* const *imaps,
					unsigned long long *bufs[]);




int ncmpi_create (MPI_Comm comm, const char *path, int cmode, MPI_Info info, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_create(comm, path, cmode, info, ncidp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_open (MPI_Comm comm, const char *path, int omode, MPI_Info info, int *ncidp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_open(comm, path, omode, info, ncidp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_file_info (int ncid, MPI_Info *info_used) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_file_info(ncid, info_used);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_file_info (int ncid, MPI_Info *info_used) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_file_info(ncid, info_used);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_delete (const char *filename, MPI_Info info) {
  FUNCTION_ENTRY;
  int ret = libncmpi_delete(filename, info);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_enddef (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_enddef(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi__enddef (int ncid, MPI_Offset h_minfree, MPI_Offset v_align, MPI_Offset v_minfree, MPI_Offset r_align) {
  FUNCTION_ENTRY;
  int ret = libncmpi__enddef(ncid, h_minfree, v_align, v_minfree, r_align);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_redef (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_redef(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_set_default_format (int format, int *old_formatp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_set_default_format(format, old_formatp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_default_format (int *formatp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_default_format(formatp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_sync (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_sync(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_flush (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_flush(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_sync_numrecs (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_sync_numrecs(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_abort (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_abort(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_begin_indep_data (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_begin_indep_data(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_end_indep_data (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_end_indep_data(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_close (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_close(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_set_fill (int ncid, int fillmode, int *old_modep) {
  FUNCTION_ENTRY;
  int ret = libncmpi_set_fill(ncid, fillmode, old_modep);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_def_var_fill (int ncid, int varid, int no_fill, const void *fill_value) {
  FUNCTION_ENTRY;
  int ret = libncmpi_def_var_fill(ncid, varid, no_fill, fill_value);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_fill_var_rec (int ncid, int varid, MPI_Offset recno) {
  FUNCTION_ENTRY;
  int ret = libncmpi_fill_var_rec(ncid, varid, recno);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_def_dim (int ncid, const char *name, MPI_Offset len, int *idp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_def_dim(ncid, name, len, idp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_def_var (int ncid, const char *name, nc_type xtype, int ndims, const int *dimidsp, int *varidp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_def_var(ncid, name, xtype, ndims, dimidsp, varidp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_rename_dim (int ncid, int dimid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncmpi_rename_dim(ncid, dimid, name);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_rename_var (int ncid, int varid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncmpi_rename_var(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq (int ncid, int *ndimsp, int *nvarsp, int *ngattsp, int *unlimdimidp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq(ncid, ndimsp, nvarsp, ngattsp, unlimdimidp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_format (int ncid, int *formatp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_format(ncid, formatp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_file_format (const char *filename, int *formatp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_file_format(filename, formatp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_version (int ncid, int *NC_mode) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_version(ncid, NC_mode);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_striping (int ncid, int *striping_size, int *striping_count) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_striping(ncid, striping_size, striping_count);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_ndims (int ncid, int *ndimsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_ndims(ncid, ndimsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_nvars (int ncid, int *nvarsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_nvars(ncid, nvarsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_num_rec_vars (int ncid, int *nvarsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_num_rec_vars(ncid, nvarsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_num_fix_vars (int ncid, int *nvarsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_num_fix_vars(ncid, nvarsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_natts (int ncid, int *ngattsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_natts(ncid, ngattsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_unlimdim (int ncid, int *unlimdimidp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_unlimdim(ncid, unlimdimidp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_dimid (int ncid, const char *name, int *idp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_dimid(ncid, name, idp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_dim (int ncid, int dimid, char *name, MPI_Offset *lenp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_dim(ncid, dimid, name, lenp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_dimname (int ncid, int dimid, char *name) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_dimname(ncid, dimid, name);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_dimlen (int ncid, int dimid, MPI_Offset *lenp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_dimlen(ncid, dimid, lenp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_var (int ncid, int varid, char *name, nc_type *xtypep, int *ndimsp, int *dimidsp, int *nattsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_var(ncid, varid, name, xtypep, ndimsp, dimidsp, nattsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_varid (int ncid, const char *name, int *varidp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_varid(ncid, name, varidp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_varname (int ncid, int varid, char *name) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_varname(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_vartype (int ncid, int varid, nc_type *xtypep) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_vartype(ncid, varid, xtypep);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_varndims (int ncid, int varid, int *ndimsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_varndims(ncid, varid, ndimsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_vardimid (int ncid, int varid, int *dimidsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_vardimid(ncid, varid, dimidsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_varnatts (int ncid, int varid, int *nattsp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_varnatts(ncid, varid, nattsp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_varoffset (int ncid, int varid, MPI_Offset *offset) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_varoffset(ncid, varid, offset);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_put_size (int ncid, MPI_Offset *size) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_put_size(ncid, size);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_get_size (int ncid, MPI_Offset *size) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_get_size(ncid, size);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_header_size (int ncid, MPI_Offset *size) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_header_size(ncid, size);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_header_extent (int ncid, MPI_Offset *extent) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_header_extent(ncid, extent);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_malloc_size (MPI_Offset *size) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_malloc_size(size);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_malloc_max_size (MPI_Offset *size) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_malloc_max_size(size);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_malloc_list (void) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_malloc_list();
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_files_opened (int *num, int *ncids) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_files_opened(num, ncids);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_recsize (int ncid, MPI_Offset *recsize) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_recsize(ncid, recsize);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_var_fill (int ncid, int varid, int *no_fill, void *fill_value) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_var_fill(ncid, varid, no_fill, fill_value);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_path (int ncid, int *pathlen, char *path) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_path(ncid, pathlen, path);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_att (int ncid, int varid, const char *name, nc_type *xtypep, MPI_Offset *lenp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_att(ncid, varid, name, xtypep, lenp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_attid (int ncid, int varid, const char *name, int *idp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_attid(ncid, varid, name, idp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_atttype (int ncid, int varid, const char *name, nc_type *xtypep) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_atttype(ncid, varid, name, xtypep);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_attlen (int ncid, int varid, const char *name, MPI_Offset *lenp) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_attlen(ncid, varid, name, lenp);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_attname (int ncid, int varid, int attnum, char *name) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_attname(ncid, varid, attnum, name);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_copy_att (int ncid_in, int varid_in, const char *name, int ncid_out, int varid_out) {
  FUNCTION_ENTRY;
  int ret = libncmpi_copy_att(ncid_in, varid_in, name, ncid_out, varid_out);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_rename_att (int ncid, int varid, const char *name, const char *newname) {
  FUNCTION_ENTRY;
  int ret = libncmpi_rename_att(ncid, varid, name, newname);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_del_att (int ncid, int varid, const char *name) {
  FUNCTION_ENTRY;
  int ret = libncmpi_del_att(ncid, varid, name);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset nelems, const void *value) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att(ncid, varid, name, xtype, nelems, value);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_text (int ncid, int varid, const char *name, MPI_Offset len, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_text(ncid, varid, name, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_schar (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_schar(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_short (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_short(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_int (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_int(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_float (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_float(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_double (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_double(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_longlong (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_longlong(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att (int ncid, int varid, const char *name, void *value) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att(ncid, varid, name, value);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_text (int ncid, int varid, const char *name, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_text(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_schar (int ncid, int varid, const char *name, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_schar(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_short (int ncid, int varid, const char *name, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_short(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_int (int ncid, int varid, const char *name, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_int(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_float (int ncid, int varid, const char *name, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_float(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_double (int ncid, int varid, const char *name, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_double(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_longlong (int ncid, int varid, const char *name, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_longlong(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_uchar (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_uchar(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_ubyte (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_ubyte(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_ushort (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_ushort(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_uint (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_uint(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_long (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_long(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_att_ulonglong (int ncid, int varid, const char *name, nc_type xtype, MPI_Offset len, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_att_ulonglong(ncid, varid, name, xtype, len, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_uchar (int ncid, int varid, const char *name, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_uchar(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_ubyte (int ncid, int varid, const char *name, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_ubyte(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_ushort (int ncid, int varid, const char *name, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_ushort(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_uint (int ncid, int varid, const char *name, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_uint(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_long (int ncid, int varid, const char *name, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_long(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_att_ulonglong (int ncid, int varid, const char *name, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_att_ulonglong(ncid, varid, name, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1 (int ncid, int varid, const MPI_Offset *start, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1(ncid, varid, start, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_all (int ncid, int varid, const MPI_Offset *start, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_all(ncid, varid, start, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_text (int ncid, int varid, const MPI_Offset *start, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_text(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_text_all (int ncid, int varid, const MPI_Offset *start, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_text_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_schar (int ncid, int varid, const MPI_Offset *start, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_schar(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_schar_all (int ncid, int varid, const MPI_Offset *start, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_schar_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_short (int ncid, int varid, const MPI_Offset *start, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_short(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_short_all (int ncid, int varid, const MPI_Offset *start, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_short_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_int (int ncid, int varid, const MPI_Offset *start, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_int(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_int_all (int ncid, int varid, const MPI_Offset *start, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_int_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_float (int ncid, int varid, const MPI_Offset *start, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_float(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_float_all (int ncid, int varid, const MPI_Offset *start, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_float_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_double (int ncid, int varid, const MPI_Offset *start, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_double(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_double_all (int ncid, int varid, const MPI_Offset *start, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_double_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_longlong (int ncid, int varid, const MPI_Offset *start, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_longlong(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_longlong_all (int ncid, int varid, const MPI_Offset *start, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_longlong_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1 (int ncid, int varid, const MPI_Offset *start, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1(ncid, varid, start, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_all (int ncid, int varid, const MPI_Offset *start, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_all(ncid, varid, start, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_text (int ncid, int varid, const MPI_Offset *start, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_text(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_text_all (int ncid, int varid, const MPI_Offset *start, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_text_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_schar (int ncid, int varid, const MPI_Offset *start, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_schar(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_schar_all (int ncid, int varid, const MPI_Offset *start, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_schar_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_short (int ncid, int varid, const MPI_Offset *start, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_short(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_short_all (int ncid, int varid, const MPI_Offset *start, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_short_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_int (int ncid, int varid, const MPI_Offset *start, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_int(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_int_all (int ncid, int varid, const MPI_Offset *start, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_int_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_float (int ncid, int varid, const MPI_Offset *start, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_float(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_float_all (int ncid, int varid, const MPI_Offset *start, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_float_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_double (int ncid, int varid, const MPI_Offset *start, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_double(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_double_all (int ncid, int varid, const MPI_Offset *start, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_double_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_longlong (int ncid, int varid, const MPI_Offset *start, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_longlong(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_longlong_all (int ncid, int varid, const MPI_Offset *start, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_longlong_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_uchar (int ncid, int varid, const MPI_Offset *start, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_uchar(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_uchar_all (int ncid, int varid, const MPI_Offset *start, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_uchar_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_ushort (int ncid, int varid, const MPI_Offset *start, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_ushort(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_ushort_all (int ncid, int varid, const MPI_Offset *start, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_ushort_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_uint (int ncid, int varid, const MPI_Offset *start, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_uint(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_uint_all (int ncid, int varid, const MPI_Offset *start, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_uint_all(ncid, varid, start, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_long (int ncid, int varid, const MPI_Offset *start, const long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_long(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_long_all (int ncid, int varid, const MPI_Offset *start, const long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_long_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_ulonglong (int ncid, int varid, const MPI_Offset *start, const unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_ulonglong(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var1_ulonglong_all (int ncid, int varid, const MPI_Offset *start, const unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var1_ulonglong_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_uchar (int ncid, int varid, const MPI_Offset *start, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_uchar(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_uchar_all (int ncid, int varid, const MPI_Offset *start, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_uchar_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_ushort (int ncid, int varid, const MPI_Offset *start, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_ushort(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_ushort_all (int ncid, int varid, const MPI_Offset *start, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_ushort_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_uint (int ncid, int varid, const MPI_Offset *start, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_uint(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_uint_all (int ncid, int varid, const MPI_Offset *start, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_uint_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_long (int ncid, int varid, const MPI_Offset *start, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_long(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_long_all (int ncid, int varid, const MPI_Offset *start, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_long_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_ulonglong (int ncid, int varid, const MPI_Offset *start, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_ulonglong(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var1_ulonglong_all (int ncid, int varid, const MPI_Offset *start, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var1_ulonglong_all(ncid, varid, start, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var (int ncid, int varid, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var(ncid, varid, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_all (int ncid, int varid, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_all(ncid, varid, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_text (int ncid, int varid, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_text(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_text_all (int ncid, int varid, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_text_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_schar (int ncid, int varid, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_schar(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_schar_all (int ncid, int varid, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_schar_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_short (int ncid, int varid, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_short(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_short_all (int ncid, int varid, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_short_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_int (int ncid, int varid, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_int(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_int_all (int ncid, int varid, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_int_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_float (int ncid, int varid, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_float(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_float_all (int ncid, int varid, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_float_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_double (int ncid, int varid, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_double(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_double_all (int ncid, int varid, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_double_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_longlong (int ncid, int varid, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_longlong(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_longlong_all (int ncid, int varid, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_longlong_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var (int ncid, int varid, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var(ncid, varid, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_all (int ncid, int varid, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_all(ncid, varid, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_text (int ncid, int varid, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_text(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_text_all (int ncid, int varid, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_text_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_schar (int ncid, int varid, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_schar(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_schar_all (int ncid, int varid, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_schar_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_short (int ncid, int varid, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_short(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_short_all (int ncid, int varid, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_short_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_int (int ncid, int varid, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_int(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_int_all (int ncid, int varid, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_int_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_float (int ncid, int varid, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_float(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_float_all (int ncid, int varid, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_float_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_double (int ncid, int varid, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_double(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_double_all (int ncid, int varid, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_double_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_longlong (int ncid, int varid, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_longlong(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_longlong_all (int ncid, int varid, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_longlong_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_uchar (int ncid, int varid, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_uchar(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_uchar_all (int ncid, int varid, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_uchar_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_ushort (int ncid, int varid, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_ushort(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_ushort_all (int ncid, int varid, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_ushort_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_uint (int ncid, int varid, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_uint(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_uint_all (int ncid, int varid, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_uint_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_long (int ncid, int varid, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_long(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_long_all (int ncid, int varid, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_long_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_ulonglong (int ncid, int varid, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_ulonglong(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_var_ulonglong_all (int ncid, int varid, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_var_ulonglong_all(ncid, varid, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_uchar (int ncid, int varid, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_uchar(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_uchar_all (int ncid, int varid, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_uchar_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_ushort (int ncid, int varid, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_ushort(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_ushort_all (int ncid, int varid, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_ushort_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_uint (int ncid, int varid, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_uint(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_uint_all (int ncid, int varid, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_uint_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_long (int ncid, int varid, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_long(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_long_all (int ncid, int varid, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_long_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_ulonglong (int ncid, int varid, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_ulonglong(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_var_ulonglong_all (int ncid, int varid, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_var_ulonglong_all(ncid, varid, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara(ncid, varid, start, count, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_all(ncid, varid, start, count, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_text(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_text_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_text_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_schar(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_schar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_schar_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_short(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_short_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_short_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_int(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_int_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_int_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_float(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_float_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_float_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_double(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_double_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_double_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_longlong(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_longlong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_longlong_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara(ncid, varid, start, count, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_all(ncid, varid, start, count, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_text(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_text_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_text_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_schar(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_schar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_schar_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_short(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_short_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_short_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_int(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_int_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_int_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_float(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_float_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_float_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_double(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_double_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_double_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_longlong(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_longlong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_longlong_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_uchar(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_uchar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_uchar_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_ushort(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_ushort_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_ushort_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_uint(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_uint_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_uint_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_long(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_long_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_long_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_ulonglong(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vara_ulonglong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vara_ulonglong_all(ncid, varid, start, count, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_uchar(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_uchar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_uchar_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_ushort(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_ushort_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_ushort_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_uint(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_uint_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_uint_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_long(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_long_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_long_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_ulonglong(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vara_ulonglong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vara_ulonglong_all(ncid, varid, start, count, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars(ncid, varid, start, count, stride, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_all(ncid, varid, start, count, stride, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_text(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_text_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_text_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_schar(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_schar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_schar_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_short(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_short_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_short_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_int(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_int_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_int_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_float(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_float_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_float_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_double(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_double_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_double_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_longlong(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_longlong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_longlong_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars(ncid, varid, start, count, stride, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_all(ncid, varid, start, count, stride, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_schar(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_schar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_schar_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_text(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_text_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_text_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_short(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_short_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_short_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_int(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_int_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_int_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_float(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_float_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_float_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_double(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_double_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_double_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_longlong(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_longlong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_longlong_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_uchar(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_uchar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_uchar_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_ushort(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_ushort_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_ushort_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_uint(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_uint_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_uint_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_long(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_long_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_long_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_ulonglong(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vars_ulonglong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vars_ulonglong_all(ncid, varid, start, count, stride, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_uchar(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_uchar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_uchar_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_ushort(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_ushort_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_ushort_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_uint(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_uint_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_uint_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_long(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_long_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_long_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_ulonglong(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vars_ulonglong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vars_ulonglong_all(ncid, varid, start, count, stride, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm(ncid, varid, start, count, stride, imap, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_all(ncid, varid, start, count, stride, imap, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_text(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_text_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_text_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_schar(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_schar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_schar_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_short(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_short_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_short_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_int(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_int_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_int_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_float(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_float_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_float_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_double(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_double_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_double_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_longlong(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_longlong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_longlong_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm(ncid, varid, start, count, stride, imap, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_all(ncid, varid, start, count, stride, imap, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_schar(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_schar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_schar_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_text(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_text_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_text_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_short(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_short_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_short_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_int(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_int_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_int_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_float(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_float_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_float_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_double(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_double_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_double_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_longlong(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_longlong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_longlong_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_uchar(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_uchar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_uchar_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_ushort(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_ushort_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_ushort_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_uint(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_uint_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_uint_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_long(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_long_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_long_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_ulonglong(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varm_ulonglong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varm_ulonglong_all(ncid, varid, start, count, stride, imap, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_uchar(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_uchar_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_uchar_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_ushort(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_ushort_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_ushort_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_uint(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_uint_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_uint_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_long(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_long_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_long_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_ulonglong(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varm_ulonglong_all (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varm_ulonglong_all(ncid, varid, start, count, stride, imap, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn(ncid, varid, num, starts, counts, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const void *op, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_all(ncid, varid, num, starts, counts, op, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn(ncid, varid, num, starts, counts, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_all(ncid, varid, num, starts, counts, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_text (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_text(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_text_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_text_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_schar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_schar(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_schar_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const signed char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_schar_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_short (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_short(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_short_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_short_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_int (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_int(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_int_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_int_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_float (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_float(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_float_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const float *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_float_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_double (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_double(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_double_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const double *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_double_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_longlong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_longlong(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_longlong_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_longlong_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_uchar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_uchar(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_uchar_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned char *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_uchar_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_ushort (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_ushort(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_ushort_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned short *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_ushort_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_uint (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_uint(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_uint_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned int *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_uint_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_long (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_long(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_long_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_long_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_ulonglong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_ulonglong(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_varn_ulonglong_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned long long *op) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_varn_ulonglong_all(ncid, varid, num, starts, counts, op);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_text (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_text(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_text_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_text_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_schar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_schar(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_schar_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, signed char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_schar_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_short (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_short(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_short_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_short_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_int (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_int(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_int_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_int_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_float (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_float(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_float_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, float *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_float_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_double (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_double(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_double_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, double *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_double_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_longlong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_longlong(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_longlong_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_longlong_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_uchar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_uchar(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_uchar_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned char *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_uchar_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_ushort (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_ushort(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_ushort_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned short *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_ushort_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_uint (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_uint(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_uint_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned int *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_uint_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_long (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_long(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_long_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_long_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_ulonglong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_ulonglong(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_varn_ulonglong_all (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned long long *ip) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_varn_ulonglong_all(ncid, varid, num, starts, counts, ip);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vard (int ncid, int varid, MPI_Datatype filetype, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vard(ncid, varid, filetype, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_get_vard_all (int ncid, int varid, MPI_Datatype filetype, void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_get_vard_all(ncid, varid, filetype, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vard (int ncid, int varid, MPI_Datatype filetype, const void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vard(ncid, varid, filetype, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_put_vard_all (int ncid, int varid, MPI_Datatype filetype, const void *ip, MPI_Offset bufcount, MPI_Datatype buftype) {
  FUNCTION_ENTRY;
  int ret = libncmpi_put_vard_all(ncid, varid, filetype, ip, bufcount, buftype);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_wait (int ncid, int count, int array_of_requests[], int array_of_statuses[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_wait(ncid, count, array_of_requests, array_of_statuses);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_wait_all (int ncid, int count, int array_of_requests[], int array_of_statuses[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_wait_all(ncid, count, array_of_requests, array_of_statuses);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_cancel (int ncid, int num, int *reqs, int *statuses) {
  FUNCTION_ENTRY;
  int ret = libncmpi_cancel(ncid, num, reqs, statuses);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_buffer_attach (int ncid, MPI_Offset bufsize) {
  FUNCTION_ENTRY;
  int ret = libncmpi_buffer_attach(ncid, bufsize);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_buffer_detach (int ncid) {
  FUNCTION_ENTRY;
  int ret = libncmpi_buffer_detach(ncid);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_buffer_usage (int ncid, MPI_Offset *usage) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_buffer_usage(ncid, usage);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_buffer_size (int ncid, MPI_Offset *buf_size) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_buffer_size(ncid, buf_size);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_inq_nreqs (int ncid, int *nreqs) {
  FUNCTION_ENTRY;
  int ret = libncmpi_inq_nreqs(ncid, nreqs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1 (int ncid, int varid, const MPI_Offset *start, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1(ncid, varid, start, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_text (int ncid, int varid, const MPI_Offset *start, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_text(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_schar (int ncid, int varid, const MPI_Offset *start, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_schar(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_short (int ncid, int varid, const MPI_Offset *start, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_short(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_int (int ncid, int varid, const MPI_Offset *start, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_int(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_float (int ncid, int varid, const MPI_Offset *start, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_float(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_double (int ncid, int varid, const MPI_Offset *start, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_double(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_longlong (int ncid, int varid, const MPI_Offset *start, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_longlong(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1 (int ncid, int varid, const MPI_Offset *start, void *ip, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1(ncid, varid, start, ip, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_schar (int ncid, int varid, const MPI_Offset *start, signed char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_schar(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_text (int ncid, int varid, const MPI_Offset *start, char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_text(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_short (int ncid, int varid, const MPI_Offset *start, short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_short(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_int (int ncid, int varid, const MPI_Offset *start, int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_int(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_float (int ncid, int varid, const MPI_Offset *start, float *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_float(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_double (int ncid, int varid, const MPI_Offset *start, double *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_double(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_longlong (int ncid, int varid, const MPI_Offset *start, long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_longlong(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1 (int ncid, int varid, const MPI_Offset *start, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1(ncid, varid, start, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_text (int ncid, int varid, const MPI_Offset *start, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_text(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_schar (int ncid, int varid, const MPI_Offset *start, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_schar(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_short (int ncid, int varid, const MPI_Offset *start, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_short(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_int (int ncid, int varid, const MPI_Offset *start, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_int(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_float (int ncid, int varid, const MPI_Offset *start, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_float(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_double (int ncid, int varid, const MPI_Offset *start, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_double(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_longlong (int ncid, int varid, const MPI_Offset *start, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_longlong(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_uchar (int ncid, int varid, const MPI_Offset *start, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_uchar(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_ushort (int ncid, int varid, const MPI_Offset *start, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_ushort(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_uint (int ncid, int varid, const MPI_Offset *start, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_uint(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_long (int ncid, int varid, const MPI_Offset *start, const long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_long(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var1_ulonglong (int ncid, int varid, const MPI_Offset *start, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var1_ulonglong(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_uchar (int ncid, int varid, const MPI_Offset *start, unsigned char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_uchar(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_ushort (int ncid, int varid, const MPI_Offset *start, unsigned short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_ushort(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_uint (int ncid, int varid, const MPI_Offset *start, unsigned int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_uint(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_long (int ncid, int varid, const MPI_Offset *start, long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_long(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var1_ulonglong (int ncid, int varid, const MPI_Offset *start, unsigned long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var1_ulonglong(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_uchar (int ncid, int varid, const MPI_Offset *start, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_uchar(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_ushort (int ncid, int varid, const MPI_Offset *start, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_ushort(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_uint (int ncid, int varid, const MPI_Offset *start, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_uint(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_long (int ncid, int varid, const MPI_Offset *start, const long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_long(ncid, varid, start, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var1_ulonglong (int ncid, int varid, const MPI_Offset *start, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var1_ulonglong(ncid, varid, start, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var (int ncid, int varid, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var(ncid, varid, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_schar (int ncid, int varid, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_schar(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_text (int ncid, int varid, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_text(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_short (int ncid, int varid, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_short(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_int (int ncid, int varid, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_int(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_float (int ncid, int varid, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_float(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_double (int ncid, int varid, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_double(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_longlong (int ncid, int varid, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_longlong(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var (int ncid, int varid, void *ip, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var(ncid, varid, ip, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_schar (int ncid, int varid, signed char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_schar(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_text (int ncid, int varid, char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_text(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_short (int ncid, int varid, short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_short(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_int (int ncid, int varid, int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_int(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_float (int ncid, int varid, float *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_float(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_double (int ncid, int varid, double *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_double(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_longlong (int ncid, int varid, long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_longlong(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var (int ncid, int varid, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var(ncid, varid, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_schar (int ncid, int varid, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_schar(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_text (int ncid, int varid, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_text(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_short (int ncid, int varid, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_short(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_int (int ncid, int varid, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_int(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_float (int ncid, int varid, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_float(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_double (int ncid, int varid, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_double(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_longlong (int ncid, int varid, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_longlong(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_uchar (int ncid, int varid, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_uchar(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_ushort (int ncid, int varid, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_ushort(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_uint (int ncid, int varid, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_uint(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_long (int ncid, int varid, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_long(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_var_ulonglong (int ncid, int varid, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_var_ulonglong(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_uchar (int ncid, int varid, unsigned char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_uchar(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_ushort (int ncid, int varid, unsigned short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_ushort(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_uint (int ncid, int varid, unsigned int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_uint(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_long (int ncid, int varid, long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_long(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_var_ulonglong (int ncid, int varid, unsigned long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_var_ulonglong(ncid, varid, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_uchar (int ncid, int varid, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_uchar(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_ushort (int ncid, int varid, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_ushort(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_uint (int ncid, int varid, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_uint(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_long (int ncid, int varid, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_long(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_var_ulonglong (int ncid, int varid, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_var_ulonglong(ncid, varid, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara(ncid, varid, start, count, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_schar(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_text(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_short(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_int(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_float(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_double(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_longlong(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, void *ip, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara(ncid, varid, start, count, ip, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, signed char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_schar(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_text(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_short(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_int(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, float *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_float(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, double *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_double(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_longlong(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara(ncid, varid, start, count, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_schar(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_text(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_short(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_int(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_float(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_double(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_longlong(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_uchar(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_ushort(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_uint(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_long(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vara_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vara_ulonglong(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_uchar(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_ushort(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_uint(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_long(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vara_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, unsigned long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vara_ulonglong(ncid, varid, start, count, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_uchar(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_ushort(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_uint(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_long(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vara_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vara_ulonglong(ncid, varid, start, count, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars(ncid, varid, start, count, stride, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_schar(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_text(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_short(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_int(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_float(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_double(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_longlong(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, void *ip, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars(ncid, varid, start, count, stride, ip, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, signed char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_schar(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_text(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_short(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_int(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, float *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_float(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, double *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_double(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_longlong(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars(ncid, varid, start, count, stride, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_schar(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_text(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_short(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_int(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_float(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_double(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_longlong(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_uchar(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_ushort(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_uint(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_long(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_vars_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_vars_ulonglong(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_uchar(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_ushort(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_uint(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_long(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_vars_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, unsigned long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_vars_ulonglong(ncid, varid, start, count, stride, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_uchar(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_ushort(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_uint(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_long(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_vars_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_vars_ulonglong(ncid, varid, start, count, stride, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm(ncid, varid, start, count, stride, imap, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_schar(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_text(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_short(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_int(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_float(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_double(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_longlong(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, void *ip, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm(ncid, varid, start, count, stride, imap, ip, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, signed char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_schar(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_text(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_short(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_int(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, float *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_float(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, double *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_double(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_longlong(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm(ncid, varid, start, count, stride, imap, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_schar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_schar(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_text (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_text(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_short (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_short(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_int (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_int(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_float (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_float(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_double (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_double(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_longlong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_longlong(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_uchar(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_ushort(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_uint(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_long(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varm_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varm_ulonglong(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_uchar(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_ushort(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_uint(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_long(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varm_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, unsigned long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varm_ulonglong(ncid, varid, start, count, stride, imap, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_uchar (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_uchar(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_ushort (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_ushort(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_uint (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_uint(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_long (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_long(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varm_ulonglong (int ncid, int varid, const MPI_Offset *start, const MPI_Offset *count, const MPI_Offset *stride, const MPI_Offset *imap, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varm_ulonglong(ncid, varid, start, count, stride, imap, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn(ncid, varid, num, starts, counts, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn(ncid, varid, num, starts, counts, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_text (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_text(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_schar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_schar(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_short (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_short(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_int (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_int(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_float (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_float(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_double (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_double(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_longlong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_longlong(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_uchar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_uchar(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_ushort (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_ushort(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_uint (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_uint(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_long (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_long(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iput_varn_ulonglong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iput_varn_ulonglong(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_text (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_text(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_schar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, signed char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_schar(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_short (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_short(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_int (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_int(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_float (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, float *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_float(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_double (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, double *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_double(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_longlong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_longlong(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_uchar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned char *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_uchar(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_ushort (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned short *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_ushort(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_uint (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned int *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_uint(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_long (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_long(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_iget_varn_ulonglong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned long long *ip, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_iget_varn_ulonglong(ncid, varid, num, starts, counts, ip, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const void *op, MPI_Offset bufcount, MPI_Datatype buftype, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn(ncid, varid, num, starts, counts, op, bufcount, buftype, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_text (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_text(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_schar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const signed char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_schar(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_short (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_short(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_int (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_int(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_float (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const float *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_float(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_double (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const double *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_double(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_longlong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_longlong(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_uchar (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned char *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_uchar(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_ushort (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned short *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_ushort(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_uint (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned int *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_uint(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_long (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_long(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_bput_varn_ulonglong (int ncid, int varid, int num, MPI_Offset *const *starts, MPI_Offset *const *counts, const unsigned long long *op, int *req) {
  FUNCTION_ENTRY;
  int ret = libncmpi_bput_varn_ulonglong(ncid, varid, num, starts, counts, op, req);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var (int ncid, int num, int *varids, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var(ncid, num, varids, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_all (int ncid, int num, int *varids, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_all(ncid, num, varids, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_text (int ncid, int num, int *varids, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_text(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_text_all (int ncid, int num, int *varids, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_text_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_schar (int ncid, int num, int *varids, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_schar(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_schar_all (int ncid, int num, int *varids, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_schar_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_uchar (int ncid, int num, int *varids, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_uchar(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_uchar_all (int ncid, int num, int *varids, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_uchar_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_short (int ncid, int num, int *varids, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_short(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_short_all (int ncid, int num, int *varids, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_short_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_ushort (int ncid, int num, int *varids, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_ushort(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_ushort_all (int ncid, int num, int *varids, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_ushort_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_int (int ncid, int num, int *varids, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_int(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_int_all (int ncid, int num, int *varids, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_int_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_uint (int ncid, int num, int *varids, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_uint(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_uint_all (int ncid, int num, int *varids, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_uint_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_long (int ncid, int num, int *varids, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_long(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_long_all (int ncid, int num, int *varids, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_long_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_float (int ncid, int num, int *varids, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_float(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_float_all (int ncid, int num, int *varids, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_float_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_double (int ncid, int num, int *varids, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_double(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_double_all (int ncid, int num, int *varids, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_double_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_longlong (int ncid, int num, int *varids, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_longlong(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_longlong_all (int ncid, int num, int *varids, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_longlong_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_ulonglong (int ncid, int num, int *varids, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_ulonglong(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var_ulonglong_all (int ncid, int num, int *varids, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var_ulonglong_all(ncid, num, varids, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1 (int ncid, int num, int *varids, MPI_Offset *const *starts, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1(ncid, num, varids, starts, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_all (int ncid, int num, int *varids, MPI_Offset *const *starts, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_all(ncid, num, varids, starts, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_text (int ncid, int num, int *varids, MPI_Offset *const *starts, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_text(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_text_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_schar(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_schar_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_uchar(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_uchar_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_short (int ncid, int num, int *varids, MPI_Offset *const *starts, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_short(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_short_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_ushort(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_ushort_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_int (int ncid, int num, int *varids, MPI_Offset *const *starts, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_int(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_int_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_uint(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_uint_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_long (int ncid, int num, int *varids, MPI_Offset *const *starts, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_long(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_long_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_float (int ncid, int num, int *varids, MPI_Offset *const *starts, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_float(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_float_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_double (int ncid, int num, int *varids, MPI_Offset *const *starts, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_double(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_double_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_longlong(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_longlong_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_ulonglong(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_var1_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_var1_ulonglong_all(ncid, num, varids, starts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara(ncid, num, varids, starts, counts, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_all(ncid, num, varids, starts, counts, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_text (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_text(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_text_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_schar(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_schar_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_uchar(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_uchar_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_short (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_short(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_short_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_ushort(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_ushort_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_int (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_int(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_int_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_uint(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_uint_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_long (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_long(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_long_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_float (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_float(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_float_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_double (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_double(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_double_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_longlong(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_longlong_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_ulonglong(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vara_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vara_ulonglong_all(ncid, num, varids, starts, counts, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars(ncid, num, varids, starts, counts, strides, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_all(ncid, num, varids, starts, counts, strides, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_text (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_text(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_text_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_schar(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_schar_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_uchar(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_uchar_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_short (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_short(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_short_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_ushort(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_ushort_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_int (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_int(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_int_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_uint(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_uint_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_long (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_long(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_long_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_float (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_float(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_float_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_double (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_double(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_double_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_longlong(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_longlong_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_ulonglong(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_vars_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_vars_ulonglong_all(ncid, num, varids, starts, counts, strides, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm(ncid, num, varids, starts, counts, strides, imaps, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, void *const *buf, const MPI_Offset *bufcounts, const MPI_Datatype datatypes[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_all(ncid, num, varids, starts, counts, strides, imaps, buf, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_text (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_text(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_text_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_schar(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, signed char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_schar_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_uchar(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned char *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_uchar_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_short (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_short(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_short_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_ushort(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned short *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_ushort_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_int (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_int(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_int_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_uint(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned int *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_uint_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_long (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_long(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_long_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_float (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_float(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, float *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_float_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_double (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_double(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, double *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_double_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_longlong(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_longlong_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_ulonglong(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mput_varm_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned long long *const *buf) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mput_varm_ulonglong_all(ncid, num, varids, starts, counts, strides, imaps, buf);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var (int ncid, int num, int *varids, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var(ncid, num, varids, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_all (int ncid, int num, int *varids, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_all(ncid, num, varids, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_text (int ncid, int num, int *varids, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_text(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_text_all (int ncid, int num, int *varids, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_text_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_schar (int ncid, int num, int *varids, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_schar(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_schar_all (int ncid, int num, int *varids, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_schar_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_uchar (int ncid, int num, int *varids, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_uchar(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_uchar_all (int ncid, int num, int *varids, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_uchar_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_short (int ncid, int num, int *varids, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_short(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_short_all (int ncid, int num, int *varids, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_short_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_ushort (int ncid, int num, int *varids, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_ushort(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_ushort_all (int ncid, int num, int *varids, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_ushort_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_int (int ncid, int num, int *varids, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_int(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_int_all (int ncid, int num, int *varids, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_int_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_uint (int ncid, int num, int *varids, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_uint(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_uint_all (int ncid, int num, int *varids, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_uint_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_long (int ncid, int num, int *varids, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_long(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_long_all (int ncid, int num, int *varids, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_long_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_float (int ncid, int num, int *varids, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_float(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_float_all (int ncid, int num, int *varids, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_float_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_double (int ncid, int num, int *varids, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_double(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_double_all (int ncid, int num, int *varids, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_double_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_longlong (int ncid, int num, int *varids, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_longlong(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_longlong_all (int ncid, int num, int *varids, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_longlong_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_ulonglong (int ncid, int num, int *varids, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_ulonglong(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var_ulonglong_all (int ncid, int num, int *varids, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var_ulonglong_all(ncid, num, varids, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1 (int ncid, int num, int *varids, MPI_Offset *const *starts, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1(ncid, num, varids, starts, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_all (int ncid, int num, int *varids, MPI_Offset *const *starts, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_all(ncid, num, varids, starts, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_text (int ncid, int num, int *varids, MPI_Offset *const *starts, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_text(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_text_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_schar(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_schar_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_uchar(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_uchar_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_short (int ncid, int num, int *varids, MPI_Offset *const *starts, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_short(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_short_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_ushort(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_ushort_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_int (int ncid, int num, int *varids, MPI_Offset *const *starts, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_int(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_int_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_uint(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_uint_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_long (int ncid, int num, int *varids, MPI_Offset *const *starts, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_long(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_long_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_float (int ncid, int num, int *varids, MPI_Offset *const *starts, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_float(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_float_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_double (int ncid, int num, int *varids, MPI_Offset *const *starts, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_double(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_double_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_longlong(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_longlong_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_ulonglong(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_var1_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_var1_ulonglong_all(ncid, num, varids, starts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara(ncid, num, varids, starts, counts, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_all(ncid, num, varids, starts, counts, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_text (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_text(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_text_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_schar(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_schar_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_uchar(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_uchar_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_short (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_short(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_short_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_ushort(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_ushort_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_int (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_int(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_int_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_uint(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_uint_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_long (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_long(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_long_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_float (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_float(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_float_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_double (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_double(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_double_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_longlong(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_longlong_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_ulonglong(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vara_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vara_ulonglong_all(ncid, num, varids, starts, counts, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars(ncid, num, varids, starts, counts, strides, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_all(ncid, num, varids, starts, counts, strides, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_text (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_text(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_text_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_schar(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_schar_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_uchar(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_uchar_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_short (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_short(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_short_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_ushort(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_ushort_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_int (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_int(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_int_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_uint(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_uint_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_long (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_long(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_long_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_float (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_float(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_float_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_double (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_double(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_double_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_longlong(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_longlong_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_ulonglong(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_vars_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_vars_ulonglong_all(ncid, num, varids, starts, counts, strides, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm(ncid, num, varids, starts, counts, strides, imaps, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, void *bufs[], const MPI_Offset *bufcounts, const MPI_Datatype *datatypes) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_all(ncid, num, varids, starts, counts, strides, imaps, bufs, bufcounts, datatypes);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_text (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_text(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_text_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_text_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_schar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_schar(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_schar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, signed char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_schar_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_uchar (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_uchar(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_uchar_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned char *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_uchar_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_short (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_short(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_short_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_short_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_ushort (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_ushort(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_ushort_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned short *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_ushort_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_int (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_int(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_int_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_int_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_uint (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_uint(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_uint_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned int *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_uint_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_long (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_long(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_long_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_long_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_float (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_float(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_float_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, float *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_float_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_double (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_double(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_double_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, double *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_double_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_longlong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_longlong(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_longlong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_longlong_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_ulonglong (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_ulonglong(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}

int ncmpi_mget_varm_ulonglong_all (int ncid, int num, int *varids, MPI_Offset *const *starts, MPI_Offset *const *counts, MPI_Offset *const *strides, MPI_Offset *const *imaps, unsigned long long *bufs[]) {
  FUNCTION_ENTRY;
  int ret = libncmpi_mget_varm_ulonglong_all(ncid, num, varids, starts, counts, strides, imaps, bufs);
  FUNCTION_EXIT;
  return ret;
}


PPTRACE_START_INTERCEPT_FUNCTIONS(pnetcdf)
INTERCEPT3("ncmpi_create", libncmpi_create)
INTERCEPT3("ncmpi_open", libncmpi_open)
INTERCEPT3("ncmpi_inq_file_info", libncmpi_inq_file_info)
INTERCEPT3("ncmpi_get_file_info", libncmpi_get_file_info)
INTERCEPT3("ncmpi_delete", libncmpi_delete)
INTERCEPT3("ncmpi_enddef", libncmpi_enddef)
INTERCEPT3("ncmpi__enddef", libncmpi__enddef)
INTERCEPT3("ncmpi_redef", libncmpi_redef)
INTERCEPT3("ncmpi_set_default_format", libncmpi_set_default_format)
INTERCEPT3("ncmpi_inq_default_format", libncmpi_inq_default_format)
INTERCEPT3("ncmpi_sync", libncmpi_sync)
INTERCEPT3("ncmpi_flush", libncmpi_flush)
INTERCEPT3("ncmpi_sync_numrecs", libncmpi_sync_numrecs)
INTERCEPT3("ncmpi_abort", libncmpi_abort)
INTERCEPT3("ncmpi_begin_indep_data", libncmpi_begin_indep_data)
INTERCEPT3("ncmpi_end_indep_data", libncmpi_end_indep_data)
INTERCEPT3("ncmpi_close", libncmpi_close)
INTERCEPT3("ncmpi_set_fill", libncmpi_set_fill)
INTERCEPT3("ncmpi_def_var_fill", libncmpi_def_var_fill)
INTERCEPT3("ncmpi_fill_var_rec", libncmpi_fill_var_rec)
INTERCEPT3("ncmpi_def_dim", libncmpi_def_dim)
INTERCEPT3("ncmpi_def_var", libncmpi_def_var)
INTERCEPT3("ncmpi_rename_dim", libncmpi_rename_dim)
INTERCEPT3("ncmpi_rename_var", libncmpi_rename_var)
INTERCEPT3("ncmpi_inq", libncmpi_inq)
INTERCEPT3("ncmpi_inq_format", libncmpi_inq_format)
INTERCEPT3("ncmpi_inq_file_format", libncmpi_inq_file_format)
INTERCEPT3("ncmpi_inq_version", libncmpi_inq_version)
INTERCEPT3("ncmpi_inq_striping", libncmpi_inq_striping)
INTERCEPT3("ncmpi_inq_ndims", libncmpi_inq_ndims)
INTERCEPT3("ncmpi_inq_nvars", libncmpi_inq_nvars)
INTERCEPT3("ncmpi_inq_num_rec_vars", libncmpi_inq_num_rec_vars)
INTERCEPT3("ncmpi_inq_num_fix_vars", libncmpi_inq_num_fix_vars)
INTERCEPT3("ncmpi_inq_natts", libncmpi_inq_natts)
INTERCEPT3("ncmpi_inq_unlimdim", libncmpi_inq_unlimdim)
INTERCEPT3("ncmpi_inq_dimid", libncmpi_inq_dimid)
INTERCEPT3("ncmpi_inq_dim", libncmpi_inq_dim)
INTERCEPT3("ncmpi_inq_dimname", libncmpi_inq_dimname)
INTERCEPT3("ncmpi_inq_dimlen", libncmpi_inq_dimlen)
INTERCEPT3("ncmpi_inq_var", libncmpi_inq_var)
INTERCEPT3("ncmpi_inq_varid", libncmpi_inq_varid)
INTERCEPT3("ncmpi_inq_varname", libncmpi_inq_varname)
INTERCEPT3("ncmpi_inq_vartype", libncmpi_inq_vartype)
INTERCEPT3("ncmpi_inq_varndims", libncmpi_inq_varndims)
INTERCEPT3("ncmpi_inq_vardimid", libncmpi_inq_vardimid)
INTERCEPT3("ncmpi_inq_varnatts", libncmpi_inq_varnatts)
INTERCEPT3("ncmpi_inq_varoffset", libncmpi_inq_varoffset)
INTERCEPT3("ncmpi_inq_put_size", libncmpi_inq_put_size)
INTERCEPT3("ncmpi_inq_get_size", libncmpi_inq_get_size)
INTERCEPT3("ncmpi_inq_header_size", libncmpi_inq_header_size)
INTERCEPT3("ncmpi_inq_header_extent", libncmpi_inq_header_extent)
INTERCEPT3("ncmpi_inq_malloc_size", libncmpi_inq_malloc_size)
INTERCEPT3("ncmpi_inq_malloc_max_size", libncmpi_inq_malloc_max_size)
INTERCEPT3("ncmpi_inq_malloc_list", libncmpi_inq_malloc_list)
INTERCEPT3("ncmpi_inq_files_opened", libncmpi_inq_files_opened)
INTERCEPT3("ncmpi_inq_recsize", libncmpi_inq_recsize)
INTERCEPT3("ncmpi_inq_var_fill", libncmpi_inq_var_fill)
INTERCEPT3("ncmpi_inq_path", libncmpi_inq_path)
INTERCEPT3("ncmpi_inq_att", libncmpi_inq_att)
INTERCEPT3("ncmpi_inq_attid", libncmpi_inq_attid)
INTERCEPT3("ncmpi_inq_atttype", libncmpi_inq_atttype)
INTERCEPT3("ncmpi_inq_attlen", libncmpi_inq_attlen)
INTERCEPT3("ncmpi_inq_attname", libncmpi_inq_attname)
INTERCEPT3("ncmpi_copy_att", libncmpi_copy_att)
INTERCEPT3("ncmpi_rename_att", libncmpi_rename_att)
INTERCEPT3("ncmpi_del_att", libncmpi_del_att)
INTERCEPT3("ncmpi_put_att", libncmpi_put_att)
INTERCEPT3("ncmpi_put_att_text", libncmpi_put_att_text)
INTERCEPT3("ncmpi_put_att_schar", libncmpi_put_att_schar)
INTERCEPT3("ncmpi_put_att_short", libncmpi_put_att_short)
INTERCEPT3("ncmpi_put_att_int", libncmpi_put_att_int)
INTERCEPT3("ncmpi_put_att_float", libncmpi_put_att_float)
INTERCEPT3("ncmpi_put_att_double", libncmpi_put_att_double)
INTERCEPT3("ncmpi_put_att_longlong", libncmpi_put_att_longlong)
INTERCEPT3("ncmpi_get_att", libncmpi_get_att)
INTERCEPT3("ncmpi_get_att_text", libncmpi_get_att_text)
INTERCEPT3("ncmpi_get_att_schar", libncmpi_get_att_schar)
INTERCEPT3("ncmpi_get_att_short", libncmpi_get_att_short)
INTERCEPT3("ncmpi_get_att_int", libncmpi_get_att_int)
INTERCEPT3("ncmpi_get_att_float", libncmpi_get_att_float)
INTERCEPT3("ncmpi_get_att_double", libncmpi_get_att_double)
INTERCEPT3("ncmpi_get_att_longlong", libncmpi_get_att_longlong)
INTERCEPT3("ncmpi_put_att_uchar", libncmpi_put_att_uchar)
INTERCEPT3("ncmpi_put_att_ubyte", libncmpi_put_att_ubyte)
INTERCEPT3("ncmpi_put_att_ushort", libncmpi_put_att_ushort)
INTERCEPT3("ncmpi_put_att_uint", libncmpi_put_att_uint)
INTERCEPT3("ncmpi_put_att_long", libncmpi_put_att_long)
INTERCEPT3("ncmpi_put_att_ulonglong", libncmpi_put_att_ulonglong)
INTERCEPT3("ncmpi_get_att_uchar", libncmpi_get_att_uchar)
INTERCEPT3("ncmpi_get_att_ubyte", libncmpi_get_att_ubyte)
INTERCEPT3("ncmpi_get_att_ushort", libncmpi_get_att_ushort)
INTERCEPT3("ncmpi_get_att_uint", libncmpi_get_att_uint)
INTERCEPT3("ncmpi_get_att_long", libncmpi_get_att_long)
INTERCEPT3("ncmpi_get_att_ulonglong", libncmpi_get_att_ulonglong)
INTERCEPT3("ncmpi_put_var1", libncmpi_put_var1)
INTERCEPT3("ncmpi_put_var1_all", libncmpi_put_var1_all)
INTERCEPT3("ncmpi_put_var1_text", libncmpi_put_var1_text)
INTERCEPT3("ncmpi_put_var1_text_all", libncmpi_put_var1_text_all)
INTERCEPT3("ncmpi_put_var1_schar", libncmpi_put_var1_schar)
INTERCEPT3("ncmpi_put_var1_schar_all", libncmpi_put_var1_schar_all)
INTERCEPT3("ncmpi_put_var1_short", libncmpi_put_var1_short)
INTERCEPT3("ncmpi_put_var1_short_all", libncmpi_put_var1_short_all)
INTERCEPT3("ncmpi_put_var1_int", libncmpi_put_var1_int)
INTERCEPT3("ncmpi_put_var1_int_all", libncmpi_put_var1_int_all)
INTERCEPT3("ncmpi_put_var1_float", libncmpi_put_var1_float)
INTERCEPT3("ncmpi_put_var1_float_all", libncmpi_put_var1_float_all)
INTERCEPT3("ncmpi_put_var1_double", libncmpi_put_var1_double)
INTERCEPT3("ncmpi_put_var1_double_all", libncmpi_put_var1_double_all)
INTERCEPT3("ncmpi_put_var1_longlong", libncmpi_put_var1_longlong)
INTERCEPT3("ncmpi_put_var1_longlong_all", libncmpi_put_var1_longlong_all)
INTERCEPT3("ncmpi_get_var1", libncmpi_get_var1)
INTERCEPT3("ncmpi_get_var1_all", libncmpi_get_var1_all)
INTERCEPT3("ncmpi_get_var1_text", libncmpi_get_var1_text)
INTERCEPT3("ncmpi_get_var1_text_all", libncmpi_get_var1_text_all)
INTERCEPT3("ncmpi_get_var1_schar", libncmpi_get_var1_schar)
INTERCEPT3("ncmpi_get_var1_schar_all", libncmpi_get_var1_schar_all)
INTERCEPT3("ncmpi_get_var1_short", libncmpi_get_var1_short)
INTERCEPT3("ncmpi_get_var1_short_all", libncmpi_get_var1_short_all)
INTERCEPT3("ncmpi_get_var1_int", libncmpi_get_var1_int)
INTERCEPT3("ncmpi_get_var1_int_all", libncmpi_get_var1_int_all)
INTERCEPT3("ncmpi_get_var1_float", libncmpi_get_var1_float)
INTERCEPT3("ncmpi_get_var1_float_all", libncmpi_get_var1_float_all)
INTERCEPT3("ncmpi_get_var1_double", libncmpi_get_var1_double)
INTERCEPT3("ncmpi_get_var1_double_all", libncmpi_get_var1_double_all)
INTERCEPT3("ncmpi_get_var1_longlong", libncmpi_get_var1_longlong)
INTERCEPT3("ncmpi_get_var1_longlong_all", libncmpi_get_var1_longlong_all)
INTERCEPT3("ncmpi_put_var1_uchar", libncmpi_put_var1_uchar)
INTERCEPT3("ncmpi_put_var1_uchar_all", libncmpi_put_var1_uchar_all)
INTERCEPT3("ncmpi_put_var1_ushort", libncmpi_put_var1_ushort)
INTERCEPT3("ncmpi_put_var1_ushort_all", libncmpi_put_var1_ushort_all)
INTERCEPT3("ncmpi_put_var1_uint", libncmpi_put_var1_uint)
INTERCEPT3("ncmpi_put_var1_uint_all", libncmpi_put_var1_uint_all)
INTERCEPT3("ncmpi_put_var1_long", libncmpi_put_var1_long)
INTERCEPT3("ncmpi_put_var1_long_all", libncmpi_put_var1_long_all)
INTERCEPT3("ncmpi_put_var1_ulonglong", libncmpi_put_var1_ulonglong)
INTERCEPT3("ncmpi_put_var1_ulonglong_all", libncmpi_put_var1_ulonglong_all)
INTERCEPT3("ncmpi_get_var1_uchar", libncmpi_get_var1_uchar)
INTERCEPT3("ncmpi_get_var1_uchar_all", libncmpi_get_var1_uchar_all)
INTERCEPT3("ncmpi_get_var1_ushort", libncmpi_get_var1_ushort)
INTERCEPT3("ncmpi_get_var1_ushort_all", libncmpi_get_var1_ushort_all)
INTERCEPT3("ncmpi_get_var1_uint", libncmpi_get_var1_uint)
INTERCEPT3("ncmpi_get_var1_uint_all", libncmpi_get_var1_uint_all)
INTERCEPT3("ncmpi_get_var1_long", libncmpi_get_var1_long)
INTERCEPT3("ncmpi_get_var1_long_all", libncmpi_get_var1_long_all)
INTERCEPT3("ncmpi_get_var1_ulonglong", libncmpi_get_var1_ulonglong)
INTERCEPT3("ncmpi_get_var1_ulonglong_all", libncmpi_get_var1_ulonglong_all)
INTERCEPT3("ncmpi_put_var", libncmpi_put_var)
INTERCEPT3("ncmpi_put_var_all", libncmpi_put_var_all)
INTERCEPT3("ncmpi_put_var_text", libncmpi_put_var_text)
INTERCEPT3("ncmpi_put_var_text_all", libncmpi_put_var_text_all)
INTERCEPT3("ncmpi_put_var_schar", libncmpi_put_var_schar)
INTERCEPT3("ncmpi_put_var_schar_all", libncmpi_put_var_schar_all)
INTERCEPT3("ncmpi_put_var_short", libncmpi_put_var_short)
INTERCEPT3("ncmpi_put_var_short_all", libncmpi_put_var_short_all)
INTERCEPT3("ncmpi_put_var_int", libncmpi_put_var_int)
INTERCEPT3("ncmpi_put_var_int_all", libncmpi_put_var_int_all)
INTERCEPT3("ncmpi_put_var_float", libncmpi_put_var_float)
INTERCEPT3("ncmpi_put_var_float_all", libncmpi_put_var_float_all)
INTERCEPT3("ncmpi_put_var_double", libncmpi_put_var_double)
INTERCEPT3("ncmpi_put_var_double_all", libncmpi_put_var_double_all)
INTERCEPT3("ncmpi_put_var_longlong", libncmpi_put_var_longlong)
INTERCEPT3("ncmpi_put_var_longlong_all", libncmpi_put_var_longlong_all)
INTERCEPT3("ncmpi_get_var", libncmpi_get_var)
INTERCEPT3("ncmpi_get_var_all", libncmpi_get_var_all)
INTERCEPT3("ncmpi_get_var_text", libncmpi_get_var_text)
INTERCEPT3("ncmpi_get_var_text_all", libncmpi_get_var_text_all)
INTERCEPT3("ncmpi_get_var_schar", libncmpi_get_var_schar)
INTERCEPT3("ncmpi_get_var_schar_all", libncmpi_get_var_schar_all)
INTERCEPT3("ncmpi_get_var_short", libncmpi_get_var_short)
INTERCEPT3("ncmpi_get_var_short_all", libncmpi_get_var_short_all)
INTERCEPT3("ncmpi_get_var_int", libncmpi_get_var_int)
INTERCEPT3("ncmpi_get_var_int_all", libncmpi_get_var_int_all)
INTERCEPT3("ncmpi_get_var_float", libncmpi_get_var_float)
INTERCEPT3("ncmpi_get_var_float_all", libncmpi_get_var_float_all)
INTERCEPT3("ncmpi_get_var_double", libncmpi_get_var_double)
INTERCEPT3("ncmpi_get_var_double_all", libncmpi_get_var_double_all)
INTERCEPT3("ncmpi_get_var_longlong", libncmpi_get_var_longlong)
INTERCEPT3("ncmpi_get_var_longlong_all", libncmpi_get_var_longlong_all)
INTERCEPT3("ncmpi_put_var_uchar", libncmpi_put_var_uchar)
INTERCEPT3("ncmpi_put_var_uchar_all", libncmpi_put_var_uchar_all)
INTERCEPT3("ncmpi_put_var_ushort", libncmpi_put_var_ushort)
INTERCEPT3("ncmpi_put_var_ushort_all", libncmpi_put_var_ushort_all)
INTERCEPT3("ncmpi_put_var_uint", libncmpi_put_var_uint)
INTERCEPT3("ncmpi_put_var_uint_all", libncmpi_put_var_uint_all)
INTERCEPT3("ncmpi_put_var_long", libncmpi_put_var_long)
INTERCEPT3("ncmpi_put_var_long_all", libncmpi_put_var_long_all)
INTERCEPT3("ncmpi_put_var_ulonglong", libncmpi_put_var_ulonglong)
INTERCEPT3("ncmpi_put_var_ulonglong_all", libncmpi_put_var_ulonglong_all)
INTERCEPT3("ncmpi_get_var_uchar", libncmpi_get_var_uchar)
INTERCEPT3("ncmpi_get_var_uchar_all", libncmpi_get_var_uchar_all)
INTERCEPT3("ncmpi_get_var_ushort", libncmpi_get_var_ushort)
INTERCEPT3("ncmpi_get_var_ushort_all", libncmpi_get_var_ushort_all)
INTERCEPT3("ncmpi_get_var_uint", libncmpi_get_var_uint)
INTERCEPT3("ncmpi_get_var_uint_all", libncmpi_get_var_uint_all)
INTERCEPT3("ncmpi_get_var_long", libncmpi_get_var_long)
INTERCEPT3("ncmpi_get_var_long_all", libncmpi_get_var_long_all)
INTERCEPT3("ncmpi_get_var_ulonglong", libncmpi_get_var_ulonglong)
INTERCEPT3("ncmpi_get_var_ulonglong_all", libncmpi_get_var_ulonglong_all)
INTERCEPT3("ncmpi_put_vara", libncmpi_put_vara)
INTERCEPT3("ncmpi_put_vara_all", libncmpi_put_vara_all)
INTERCEPT3("ncmpi_put_vara_text", libncmpi_put_vara_text)
INTERCEPT3("ncmpi_put_vara_text_all", libncmpi_put_vara_text_all)
INTERCEPT3("ncmpi_put_vara_schar", libncmpi_put_vara_schar)
INTERCEPT3("ncmpi_put_vara_schar_all", libncmpi_put_vara_schar_all)
INTERCEPT3("ncmpi_put_vara_short", libncmpi_put_vara_short)
INTERCEPT3("ncmpi_put_vara_short_all", libncmpi_put_vara_short_all)
INTERCEPT3("ncmpi_put_vara_int", libncmpi_put_vara_int)
INTERCEPT3("ncmpi_put_vara_int_all", libncmpi_put_vara_int_all)
INTERCEPT3("ncmpi_put_vara_float", libncmpi_put_vara_float)
INTERCEPT3("ncmpi_put_vara_float_all", libncmpi_put_vara_float_all)
INTERCEPT3("ncmpi_put_vara_double", libncmpi_put_vara_double)
INTERCEPT3("ncmpi_put_vara_double_all", libncmpi_put_vara_double_all)
INTERCEPT3("ncmpi_put_vara_longlong", libncmpi_put_vara_longlong)
INTERCEPT3("ncmpi_put_vara_longlong_all", libncmpi_put_vara_longlong_all)
INTERCEPT3("ncmpi_get_vara", libncmpi_get_vara)
INTERCEPT3("ncmpi_get_vara_all", libncmpi_get_vara_all)
INTERCEPT3("ncmpi_get_vara_text", libncmpi_get_vara_text)
INTERCEPT3("ncmpi_get_vara_text_all", libncmpi_get_vara_text_all)
INTERCEPT3("ncmpi_get_vara_schar", libncmpi_get_vara_schar)
INTERCEPT3("ncmpi_get_vara_schar_all", libncmpi_get_vara_schar_all)
INTERCEPT3("ncmpi_get_vara_short", libncmpi_get_vara_short)
INTERCEPT3("ncmpi_get_vara_short_all", libncmpi_get_vara_short_all)
INTERCEPT3("ncmpi_get_vara_int", libncmpi_get_vara_int)
INTERCEPT3("ncmpi_get_vara_int_all", libncmpi_get_vara_int_all)
INTERCEPT3("ncmpi_get_vara_float", libncmpi_get_vara_float)
INTERCEPT3("ncmpi_get_vara_float_all", libncmpi_get_vara_float_all)
INTERCEPT3("ncmpi_get_vara_double", libncmpi_get_vara_double)
INTERCEPT3("ncmpi_get_vara_double_all", libncmpi_get_vara_double_all)
INTERCEPT3("ncmpi_get_vara_longlong", libncmpi_get_vara_longlong)
INTERCEPT3("ncmpi_get_vara_longlong_all", libncmpi_get_vara_longlong_all)
INTERCEPT3("ncmpi_put_vara_uchar", libncmpi_put_vara_uchar)
INTERCEPT3("ncmpi_put_vara_uchar_all", libncmpi_put_vara_uchar_all)
INTERCEPT3("ncmpi_put_vara_ushort", libncmpi_put_vara_ushort)
INTERCEPT3("ncmpi_put_vara_ushort_all", libncmpi_put_vara_ushort_all)
INTERCEPT3("ncmpi_put_vara_uint", libncmpi_put_vara_uint)
INTERCEPT3("ncmpi_put_vara_uint_all", libncmpi_put_vara_uint_all)
INTERCEPT3("ncmpi_put_vara_long", libncmpi_put_vara_long)
INTERCEPT3("ncmpi_put_vara_long_all", libncmpi_put_vara_long_all)
INTERCEPT3("ncmpi_put_vara_ulonglong", libncmpi_put_vara_ulonglong)
INTERCEPT3("ncmpi_put_vara_ulonglong_all", libncmpi_put_vara_ulonglong_all)
INTERCEPT3("ncmpi_get_vara_uchar", libncmpi_get_vara_uchar)
INTERCEPT3("ncmpi_get_vara_uchar_all", libncmpi_get_vara_uchar_all)
INTERCEPT3("ncmpi_get_vara_ushort", libncmpi_get_vara_ushort)
INTERCEPT3("ncmpi_get_vara_ushort_all", libncmpi_get_vara_ushort_all)
INTERCEPT3("ncmpi_get_vara_uint", libncmpi_get_vara_uint)
INTERCEPT3("ncmpi_get_vara_uint_all", libncmpi_get_vara_uint_all)
INTERCEPT3("ncmpi_get_vara_long", libncmpi_get_vara_long)
INTERCEPT3("ncmpi_get_vara_long_all", libncmpi_get_vara_long_all)
INTERCEPT3("ncmpi_get_vara_ulonglong", libncmpi_get_vara_ulonglong)
INTERCEPT3("ncmpi_get_vara_ulonglong_all", libncmpi_get_vara_ulonglong_all)
INTERCEPT3("ncmpi_put_vars", libncmpi_put_vars)
INTERCEPT3("ncmpi_put_vars_all", libncmpi_put_vars_all)
INTERCEPT3("ncmpi_put_vars_text", libncmpi_put_vars_text)
INTERCEPT3("ncmpi_put_vars_text_all", libncmpi_put_vars_text_all)
INTERCEPT3("ncmpi_put_vars_schar", libncmpi_put_vars_schar)
INTERCEPT3("ncmpi_put_vars_schar_all", libncmpi_put_vars_schar_all)
INTERCEPT3("ncmpi_put_vars_short", libncmpi_put_vars_short)
INTERCEPT3("ncmpi_put_vars_short_all", libncmpi_put_vars_short_all)
INTERCEPT3("ncmpi_put_vars_int", libncmpi_put_vars_int)
INTERCEPT3("ncmpi_put_vars_int_all", libncmpi_put_vars_int_all)
INTERCEPT3("ncmpi_put_vars_float", libncmpi_put_vars_float)
INTERCEPT3("ncmpi_put_vars_float_all", libncmpi_put_vars_float_all)
INTERCEPT3("ncmpi_put_vars_double", libncmpi_put_vars_double)
INTERCEPT3("ncmpi_put_vars_double_all", libncmpi_put_vars_double_all)
INTERCEPT3("ncmpi_put_vars_longlong", libncmpi_put_vars_longlong)
INTERCEPT3("ncmpi_put_vars_longlong_all", libncmpi_put_vars_longlong_all)
INTERCEPT3("ncmpi_get_vars", libncmpi_get_vars)
INTERCEPT3("ncmpi_get_vars_all", libncmpi_get_vars_all)
INTERCEPT3("ncmpi_get_vars_schar", libncmpi_get_vars_schar)
INTERCEPT3("ncmpi_get_vars_schar_all", libncmpi_get_vars_schar_all)
INTERCEPT3("ncmpi_get_vars_text", libncmpi_get_vars_text)
INTERCEPT3("ncmpi_get_vars_text_all", libncmpi_get_vars_text_all)
INTERCEPT3("ncmpi_get_vars_short", libncmpi_get_vars_short)
INTERCEPT3("ncmpi_get_vars_short_all", libncmpi_get_vars_short_all)
INTERCEPT3("ncmpi_get_vars_int", libncmpi_get_vars_int)
INTERCEPT3("ncmpi_get_vars_int_all", libncmpi_get_vars_int_all)
INTERCEPT3("ncmpi_get_vars_float", libncmpi_get_vars_float)
INTERCEPT3("ncmpi_get_vars_float_all", libncmpi_get_vars_float_all)
INTERCEPT3("ncmpi_get_vars_double", libncmpi_get_vars_double)
INTERCEPT3("ncmpi_get_vars_double_all", libncmpi_get_vars_double_all)
INTERCEPT3("ncmpi_get_vars_longlong", libncmpi_get_vars_longlong)
INTERCEPT3("ncmpi_get_vars_longlong_all", libncmpi_get_vars_longlong_all)
INTERCEPT3("ncmpi_put_vars_uchar", libncmpi_put_vars_uchar)
INTERCEPT3("ncmpi_put_vars_uchar_all", libncmpi_put_vars_uchar_all)
INTERCEPT3("ncmpi_put_vars_ushort", libncmpi_put_vars_ushort)
INTERCEPT3("ncmpi_put_vars_ushort_all", libncmpi_put_vars_ushort_all)
INTERCEPT3("ncmpi_put_vars_uint", libncmpi_put_vars_uint)
INTERCEPT3("ncmpi_put_vars_uint_all", libncmpi_put_vars_uint_all)
INTERCEPT3("ncmpi_put_vars_long", libncmpi_put_vars_long)
INTERCEPT3("ncmpi_put_vars_long_all", libncmpi_put_vars_long_all)
INTERCEPT3("ncmpi_put_vars_ulonglong", libncmpi_put_vars_ulonglong)
INTERCEPT3("ncmpi_put_vars_ulonglong_all", libncmpi_put_vars_ulonglong_all)
INTERCEPT3("ncmpi_get_vars_uchar", libncmpi_get_vars_uchar)
INTERCEPT3("ncmpi_get_vars_uchar_all", libncmpi_get_vars_uchar_all)
INTERCEPT3("ncmpi_get_vars_ushort", libncmpi_get_vars_ushort)
INTERCEPT3("ncmpi_get_vars_ushort_all", libncmpi_get_vars_ushort_all)
INTERCEPT3("ncmpi_get_vars_uint", libncmpi_get_vars_uint)
INTERCEPT3("ncmpi_get_vars_uint_all", libncmpi_get_vars_uint_all)
INTERCEPT3("ncmpi_get_vars_long", libncmpi_get_vars_long)
INTERCEPT3("ncmpi_get_vars_long_all", libncmpi_get_vars_long_all)
INTERCEPT3("ncmpi_get_vars_ulonglong", libncmpi_get_vars_ulonglong)
INTERCEPT3("ncmpi_get_vars_ulonglong_all", libncmpi_get_vars_ulonglong_all)
INTERCEPT3("ncmpi_put_varm", libncmpi_put_varm)
INTERCEPT3("ncmpi_put_varm_all", libncmpi_put_varm_all)
INTERCEPT3("ncmpi_put_varm_text", libncmpi_put_varm_text)
INTERCEPT3("ncmpi_put_varm_text_all", libncmpi_put_varm_text_all)
INTERCEPT3("ncmpi_put_varm_schar", libncmpi_put_varm_schar)
INTERCEPT3("ncmpi_put_varm_schar_all", libncmpi_put_varm_schar_all)
INTERCEPT3("ncmpi_put_varm_short", libncmpi_put_varm_short)
INTERCEPT3("ncmpi_put_varm_short_all", libncmpi_put_varm_short_all)
INTERCEPT3("ncmpi_put_varm_int", libncmpi_put_varm_int)
INTERCEPT3("ncmpi_put_varm_int_all", libncmpi_put_varm_int_all)
INTERCEPT3("ncmpi_put_varm_float", libncmpi_put_varm_float)
INTERCEPT3("ncmpi_put_varm_float_all", libncmpi_put_varm_float_all)
INTERCEPT3("ncmpi_put_varm_double", libncmpi_put_varm_double)
INTERCEPT3("ncmpi_put_varm_double_all", libncmpi_put_varm_double_all)
INTERCEPT3("ncmpi_put_varm_longlong", libncmpi_put_varm_longlong)
INTERCEPT3("ncmpi_put_varm_longlong_all", libncmpi_put_varm_longlong_all)
INTERCEPT3("ncmpi_get_varm", libncmpi_get_varm)
INTERCEPT3("ncmpi_get_varm_all", libncmpi_get_varm_all)
INTERCEPT3("ncmpi_get_varm_schar", libncmpi_get_varm_schar)
INTERCEPT3("ncmpi_get_varm_schar_all", libncmpi_get_varm_schar_all)
INTERCEPT3("ncmpi_get_varm_text", libncmpi_get_varm_text)
INTERCEPT3("ncmpi_get_varm_text_all", libncmpi_get_varm_text_all)
INTERCEPT3("ncmpi_get_varm_short", libncmpi_get_varm_short)
INTERCEPT3("ncmpi_get_varm_short_all", libncmpi_get_varm_short_all)
INTERCEPT3("ncmpi_get_varm_int", libncmpi_get_varm_int)
INTERCEPT3("ncmpi_get_varm_int_all", libncmpi_get_varm_int_all)
INTERCEPT3("ncmpi_get_varm_float", libncmpi_get_varm_float)
INTERCEPT3("ncmpi_get_varm_float_all", libncmpi_get_varm_float_all)
INTERCEPT3("ncmpi_get_varm_double", libncmpi_get_varm_double)
INTERCEPT3("ncmpi_get_varm_double_all", libncmpi_get_varm_double_all)
INTERCEPT3("ncmpi_get_varm_longlong", libncmpi_get_varm_longlong)
INTERCEPT3("ncmpi_get_varm_longlong_all", libncmpi_get_varm_longlong_all)
INTERCEPT3("ncmpi_put_varm_uchar", libncmpi_put_varm_uchar)
INTERCEPT3("ncmpi_put_varm_uchar_all", libncmpi_put_varm_uchar_all)
INTERCEPT3("ncmpi_put_varm_ushort", libncmpi_put_varm_ushort)
INTERCEPT3("ncmpi_put_varm_ushort_all", libncmpi_put_varm_ushort_all)
INTERCEPT3("ncmpi_put_varm_uint", libncmpi_put_varm_uint)
INTERCEPT3("ncmpi_put_varm_uint_all", libncmpi_put_varm_uint_all)
INTERCEPT3("ncmpi_put_varm_long", libncmpi_put_varm_long)
INTERCEPT3("ncmpi_put_varm_long_all", libncmpi_put_varm_long_all)
INTERCEPT3("ncmpi_put_varm_ulonglong", libncmpi_put_varm_ulonglong)
INTERCEPT3("ncmpi_put_varm_ulonglong_all", libncmpi_put_varm_ulonglong_all)
INTERCEPT3("ncmpi_get_varm_uchar", libncmpi_get_varm_uchar)
INTERCEPT3("ncmpi_get_varm_uchar_all", libncmpi_get_varm_uchar_all)
INTERCEPT3("ncmpi_get_varm_ushort", libncmpi_get_varm_ushort)
INTERCEPT3("ncmpi_get_varm_ushort_all", libncmpi_get_varm_ushort_all)
INTERCEPT3("ncmpi_get_varm_uint", libncmpi_get_varm_uint)
INTERCEPT3("ncmpi_get_varm_uint_all", libncmpi_get_varm_uint_all)
INTERCEPT3("ncmpi_get_varm_long", libncmpi_get_varm_long)
INTERCEPT3("ncmpi_get_varm_long_all", libncmpi_get_varm_long_all)
INTERCEPT3("ncmpi_get_varm_ulonglong", libncmpi_get_varm_ulonglong)
INTERCEPT3("ncmpi_get_varm_ulonglong_all", libncmpi_get_varm_ulonglong_all)
INTERCEPT3("ncmpi_put_varn", libncmpi_put_varn)
INTERCEPT3("ncmpi_put_varn_all", libncmpi_put_varn_all)
INTERCEPT3("ncmpi_get_varn", libncmpi_get_varn)
INTERCEPT3("ncmpi_get_varn_all", libncmpi_get_varn_all)
INTERCEPT3("ncmpi_put_varn_text", libncmpi_put_varn_text)
INTERCEPT3("ncmpi_put_varn_text_all", libncmpi_put_varn_text_all)
INTERCEPT3("ncmpi_put_varn_schar", libncmpi_put_varn_schar)
INTERCEPT3("ncmpi_put_varn_schar_all", libncmpi_put_varn_schar_all)
INTERCEPT3("ncmpi_put_varn_short", libncmpi_put_varn_short)
INTERCEPT3("ncmpi_put_varn_short_all", libncmpi_put_varn_short_all)
INTERCEPT3("ncmpi_put_varn_int", libncmpi_put_varn_int)
INTERCEPT3("ncmpi_put_varn_int_all", libncmpi_put_varn_int_all)
INTERCEPT3("ncmpi_put_varn_float", libncmpi_put_varn_float)
INTERCEPT3("ncmpi_put_varn_float_all", libncmpi_put_varn_float_all)
INTERCEPT3("ncmpi_put_varn_double", libncmpi_put_varn_double)
INTERCEPT3("ncmpi_put_varn_double_all", libncmpi_put_varn_double_all)
INTERCEPT3("ncmpi_put_varn_longlong", libncmpi_put_varn_longlong)
INTERCEPT3("ncmpi_put_varn_longlong_all", libncmpi_put_varn_longlong_all)
INTERCEPT3("ncmpi_put_varn_uchar", libncmpi_put_varn_uchar)
INTERCEPT3("ncmpi_put_varn_uchar_all", libncmpi_put_varn_uchar_all)
INTERCEPT3("ncmpi_put_varn_ushort", libncmpi_put_varn_ushort)
INTERCEPT3("ncmpi_put_varn_ushort_all", libncmpi_put_varn_ushort_all)
INTERCEPT3("ncmpi_put_varn_uint", libncmpi_put_varn_uint)
INTERCEPT3("ncmpi_put_varn_uint_all", libncmpi_put_varn_uint_all)
INTERCEPT3("ncmpi_put_varn_long", libncmpi_put_varn_long)
INTERCEPT3("ncmpi_put_varn_long_all", libncmpi_put_varn_long_all)
INTERCEPT3("ncmpi_put_varn_ulonglong", libncmpi_put_varn_ulonglong)
INTERCEPT3("ncmpi_put_varn_ulonglong_all", libncmpi_put_varn_ulonglong_all)
INTERCEPT3("ncmpi_get_varn_text", libncmpi_get_varn_text)
INTERCEPT3("ncmpi_get_varn_text_all", libncmpi_get_varn_text_all)
INTERCEPT3("ncmpi_get_varn_schar", libncmpi_get_varn_schar)
INTERCEPT3("ncmpi_get_varn_schar_all", libncmpi_get_varn_schar_all)
INTERCEPT3("ncmpi_get_varn_short", libncmpi_get_varn_short)
INTERCEPT3("ncmpi_get_varn_short_all", libncmpi_get_varn_short_all)
INTERCEPT3("ncmpi_get_varn_int", libncmpi_get_varn_int)
INTERCEPT3("ncmpi_get_varn_int_all", libncmpi_get_varn_int_all)
INTERCEPT3("ncmpi_get_varn_float", libncmpi_get_varn_float)
INTERCEPT3("ncmpi_get_varn_float_all", libncmpi_get_varn_float_all)
INTERCEPT3("ncmpi_get_varn_double", libncmpi_get_varn_double)
INTERCEPT3("ncmpi_get_varn_double_all", libncmpi_get_varn_double_all)
INTERCEPT3("ncmpi_get_varn_longlong", libncmpi_get_varn_longlong)
INTERCEPT3("ncmpi_get_varn_longlong_all", libncmpi_get_varn_longlong_all)
INTERCEPT3("ncmpi_get_varn_uchar", libncmpi_get_varn_uchar)
INTERCEPT3("ncmpi_get_varn_uchar_all", libncmpi_get_varn_uchar_all)
INTERCEPT3("ncmpi_get_varn_ushort", libncmpi_get_varn_ushort)
INTERCEPT3("ncmpi_get_varn_ushort_all", libncmpi_get_varn_ushort_all)
INTERCEPT3("ncmpi_get_varn_uint", libncmpi_get_varn_uint)
INTERCEPT3("ncmpi_get_varn_uint_all", libncmpi_get_varn_uint_all)
INTERCEPT3("ncmpi_get_varn_long", libncmpi_get_varn_long)
INTERCEPT3("ncmpi_get_varn_long_all", libncmpi_get_varn_long_all)
INTERCEPT3("ncmpi_get_varn_ulonglong", libncmpi_get_varn_ulonglong)
INTERCEPT3("ncmpi_get_varn_ulonglong_all", libncmpi_get_varn_ulonglong_all)
INTERCEPT3("ncmpi_get_vard", libncmpi_get_vard)
INTERCEPT3("ncmpi_get_vard_all", libncmpi_get_vard_all)
INTERCEPT3("ncmpi_put_vard", libncmpi_put_vard)
INTERCEPT3("ncmpi_put_vard_all", libncmpi_put_vard_all)
INTERCEPT3("ncmpi_wait", libncmpi_wait)
INTERCEPT3("ncmpi_wait_all", libncmpi_wait_all)
INTERCEPT3("ncmpi_cancel", libncmpi_cancel)
INTERCEPT3("ncmpi_buffer_attach", libncmpi_buffer_attach)
INTERCEPT3("ncmpi_buffer_detach", libncmpi_buffer_detach)
INTERCEPT3("ncmpi_inq_buffer_usage", libncmpi_inq_buffer_usage)
INTERCEPT3("ncmpi_inq_buffer_size", libncmpi_inq_buffer_size)
INTERCEPT3("ncmpi_inq_nreqs", libncmpi_inq_nreqs)
INTERCEPT3("ncmpi_iput_var1", libncmpi_iput_var1)
INTERCEPT3("ncmpi_iput_var1_text", libncmpi_iput_var1_text)
INTERCEPT3("ncmpi_iput_var1_schar", libncmpi_iput_var1_schar)
INTERCEPT3("ncmpi_iput_var1_short", libncmpi_iput_var1_short)
INTERCEPT3("ncmpi_iput_var1_int", libncmpi_iput_var1_int)
INTERCEPT3("ncmpi_iput_var1_float", libncmpi_iput_var1_float)
INTERCEPT3("ncmpi_iput_var1_double", libncmpi_iput_var1_double)
INTERCEPT3("ncmpi_iput_var1_longlong", libncmpi_iput_var1_longlong)
INTERCEPT3("ncmpi_iget_var1", libncmpi_iget_var1)
INTERCEPT3("ncmpi_iget_var1_schar", libncmpi_iget_var1_schar)
INTERCEPT3("ncmpi_iget_var1_text", libncmpi_iget_var1_text)
INTERCEPT3("ncmpi_iget_var1_short", libncmpi_iget_var1_short)
INTERCEPT3("ncmpi_iget_var1_int", libncmpi_iget_var1_int)
INTERCEPT3("ncmpi_iget_var1_float", libncmpi_iget_var1_float)
INTERCEPT3("ncmpi_iget_var1_double", libncmpi_iget_var1_double)
INTERCEPT3("ncmpi_iget_var1_longlong", libncmpi_iget_var1_longlong)
INTERCEPT3("ncmpi_bput_var1", libncmpi_bput_var1)
INTERCEPT3("ncmpi_bput_var1_text", libncmpi_bput_var1_text)
INTERCEPT3("ncmpi_bput_var1_schar", libncmpi_bput_var1_schar)
INTERCEPT3("ncmpi_bput_var1_short", libncmpi_bput_var1_short)
INTERCEPT3("ncmpi_bput_var1_int", libncmpi_bput_var1_int)
INTERCEPT3("ncmpi_bput_var1_float", libncmpi_bput_var1_float)
INTERCEPT3("ncmpi_bput_var1_double", libncmpi_bput_var1_double)
INTERCEPT3("ncmpi_bput_var1_longlong", libncmpi_bput_var1_longlong)
INTERCEPT3("ncmpi_iput_var1_uchar", libncmpi_iput_var1_uchar)
INTERCEPT3("ncmpi_iput_var1_ushort", libncmpi_iput_var1_ushort)
INTERCEPT3("ncmpi_iput_var1_uint", libncmpi_iput_var1_uint)
INTERCEPT3("ncmpi_iput_var1_long", libncmpi_iput_var1_long)
INTERCEPT3("ncmpi_iput_var1_ulonglong", libncmpi_iput_var1_ulonglong)
INTERCEPT3("ncmpi_iget_var1_uchar", libncmpi_iget_var1_uchar)
INTERCEPT3("ncmpi_iget_var1_ushort", libncmpi_iget_var1_ushort)
INTERCEPT3("ncmpi_iget_var1_uint", libncmpi_iget_var1_uint)
INTERCEPT3("ncmpi_iget_var1_long", libncmpi_iget_var1_long)
INTERCEPT3("ncmpi_iget_var1_ulonglong", libncmpi_iget_var1_ulonglong)
INTERCEPT3("ncmpi_bput_var1_uchar", libncmpi_bput_var1_uchar)
INTERCEPT3("ncmpi_bput_var1_ushort", libncmpi_bput_var1_ushort)
INTERCEPT3("ncmpi_bput_var1_uint", libncmpi_bput_var1_uint)
INTERCEPT3("ncmpi_bput_var1_long", libncmpi_bput_var1_long)
INTERCEPT3("ncmpi_bput_var1_ulonglong", libncmpi_bput_var1_ulonglong)
INTERCEPT3("ncmpi_iput_var", libncmpi_iput_var)
INTERCEPT3("ncmpi_iput_var_schar", libncmpi_iput_var_schar)
INTERCEPT3("ncmpi_iput_var_text", libncmpi_iput_var_text)
INTERCEPT3("ncmpi_iput_var_short", libncmpi_iput_var_short)
INTERCEPT3("ncmpi_iput_var_int", libncmpi_iput_var_int)
INTERCEPT3("ncmpi_iput_var_float", libncmpi_iput_var_float)
INTERCEPT3("ncmpi_iput_var_double", libncmpi_iput_var_double)
INTERCEPT3("ncmpi_iput_var_longlong", libncmpi_iput_var_longlong)
INTERCEPT3("ncmpi_iget_var", libncmpi_iget_var)
INTERCEPT3("ncmpi_iget_var_schar", libncmpi_iget_var_schar)
INTERCEPT3("ncmpi_iget_var_text", libncmpi_iget_var_text)
INTERCEPT3("ncmpi_iget_var_short", libncmpi_iget_var_short)
INTERCEPT3("ncmpi_iget_var_int", libncmpi_iget_var_int)
INTERCEPT3("ncmpi_iget_var_float", libncmpi_iget_var_float)
INTERCEPT3("ncmpi_iget_var_double", libncmpi_iget_var_double)
INTERCEPT3("ncmpi_iget_var_longlong", libncmpi_iget_var_longlong)
INTERCEPT3("ncmpi_bput_var", libncmpi_bput_var)
INTERCEPT3("ncmpi_bput_var_schar", libncmpi_bput_var_schar)
INTERCEPT3("ncmpi_bput_var_text", libncmpi_bput_var_text)
INTERCEPT3("ncmpi_bput_var_short", libncmpi_bput_var_short)
INTERCEPT3("ncmpi_bput_var_int", libncmpi_bput_var_int)
INTERCEPT3("ncmpi_bput_var_float", libncmpi_bput_var_float)
INTERCEPT3("ncmpi_bput_var_double", libncmpi_bput_var_double)
INTERCEPT3("ncmpi_bput_var_longlong", libncmpi_bput_var_longlong)
INTERCEPT3("ncmpi_iput_var_uchar", libncmpi_iput_var_uchar)
INTERCEPT3("ncmpi_iput_var_ushort", libncmpi_iput_var_ushort)
INTERCEPT3("ncmpi_iput_var_uint", libncmpi_iput_var_uint)
INTERCEPT3("ncmpi_iput_var_long", libncmpi_iput_var_long)
INTERCEPT3("ncmpi_iput_var_ulonglong", libncmpi_iput_var_ulonglong)
INTERCEPT3("ncmpi_iget_var_uchar", libncmpi_iget_var_uchar)
INTERCEPT3("ncmpi_iget_var_ushort", libncmpi_iget_var_ushort)
INTERCEPT3("ncmpi_iget_var_uint", libncmpi_iget_var_uint)
INTERCEPT3("ncmpi_iget_var_long", libncmpi_iget_var_long)
INTERCEPT3("ncmpi_iget_var_ulonglong", libncmpi_iget_var_ulonglong)
INTERCEPT3("ncmpi_bput_var_uchar", libncmpi_bput_var_uchar)
INTERCEPT3("ncmpi_bput_var_ushort", libncmpi_bput_var_ushort)
INTERCEPT3("ncmpi_bput_var_uint", libncmpi_bput_var_uint)
INTERCEPT3("ncmpi_bput_var_long", libncmpi_bput_var_long)
INTERCEPT3("ncmpi_bput_var_ulonglong", libncmpi_bput_var_ulonglong)
INTERCEPT3("ncmpi_iput_vara", libncmpi_iput_vara)
INTERCEPT3("ncmpi_iput_vara_schar", libncmpi_iput_vara_schar)
INTERCEPT3("ncmpi_iput_vara_text", libncmpi_iput_vara_text)
INTERCEPT3("ncmpi_iput_vara_short", libncmpi_iput_vara_short)
INTERCEPT3("ncmpi_iput_vara_int", libncmpi_iput_vara_int)
INTERCEPT3("ncmpi_iput_vara_float", libncmpi_iput_vara_float)
INTERCEPT3("ncmpi_iput_vara_double", libncmpi_iput_vara_double)
INTERCEPT3("ncmpi_iput_vara_longlong", libncmpi_iput_vara_longlong)
INTERCEPT3("ncmpi_iget_vara", libncmpi_iget_vara)
INTERCEPT3("ncmpi_iget_vara_schar", libncmpi_iget_vara_schar)
INTERCEPT3("ncmpi_iget_vara_text", libncmpi_iget_vara_text)
INTERCEPT3("ncmpi_iget_vara_short", libncmpi_iget_vara_short)
INTERCEPT3("ncmpi_iget_vara_int", libncmpi_iget_vara_int)
INTERCEPT3("ncmpi_iget_vara_float", libncmpi_iget_vara_float)
INTERCEPT3("ncmpi_iget_vara_double", libncmpi_iget_vara_double)
INTERCEPT3("ncmpi_iget_vara_longlong", libncmpi_iget_vara_longlong)
INTERCEPT3("ncmpi_bput_vara", libncmpi_bput_vara)
INTERCEPT3("ncmpi_bput_vara_schar", libncmpi_bput_vara_schar)
INTERCEPT3("ncmpi_bput_vara_text", libncmpi_bput_vara_text)
INTERCEPT3("ncmpi_bput_vara_short", libncmpi_bput_vara_short)
INTERCEPT3("ncmpi_bput_vara_int", libncmpi_bput_vara_int)
INTERCEPT3("ncmpi_bput_vara_float", libncmpi_bput_vara_float)
INTERCEPT3("ncmpi_bput_vara_double", libncmpi_bput_vara_double)
INTERCEPT3("ncmpi_bput_vara_longlong", libncmpi_bput_vara_longlong)
INTERCEPT3("ncmpi_iput_vara_uchar", libncmpi_iput_vara_uchar)
INTERCEPT3("ncmpi_iput_vara_ushort", libncmpi_iput_vara_ushort)
INTERCEPT3("ncmpi_iput_vara_uint", libncmpi_iput_vara_uint)
INTERCEPT3("ncmpi_iput_vara_long", libncmpi_iput_vara_long)
INTERCEPT3("ncmpi_iput_vara_ulonglong", libncmpi_iput_vara_ulonglong)
INTERCEPT3("ncmpi_iget_vara_uchar", libncmpi_iget_vara_uchar)
INTERCEPT3("ncmpi_iget_vara_ushort", libncmpi_iget_vara_ushort)
INTERCEPT3("ncmpi_iget_vara_uint", libncmpi_iget_vara_uint)
INTERCEPT3("ncmpi_iget_vara_long", libncmpi_iget_vara_long)
INTERCEPT3("ncmpi_iget_vara_ulonglong", libncmpi_iget_vara_ulonglong)
INTERCEPT3("ncmpi_bput_vara_uchar", libncmpi_bput_vara_uchar)
INTERCEPT3("ncmpi_bput_vara_ushort", libncmpi_bput_vara_ushort)
INTERCEPT3("ncmpi_bput_vara_uint", libncmpi_bput_vara_uint)
INTERCEPT3("ncmpi_bput_vara_long", libncmpi_bput_vara_long)
INTERCEPT3("ncmpi_bput_vara_ulonglong", libncmpi_bput_vara_ulonglong)
INTERCEPT3("ncmpi_iput_vars", libncmpi_iput_vars)
INTERCEPT3("ncmpi_iput_vars_schar", libncmpi_iput_vars_schar)
INTERCEPT3("ncmpi_iput_vars_text", libncmpi_iput_vars_text)
INTERCEPT3("ncmpi_iput_vars_short", libncmpi_iput_vars_short)
INTERCEPT3("ncmpi_iput_vars_int", libncmpi_iput_vars_int)
INTERCEPT3("ncmpi_iput_vars_float", libncmpi_iput_vars_float)
INTERCEPT3("ncmpi_iput_vars_double", libncmpi_iput_vars_double)
INTERCEPT3("ncmpi_iput_vars_longlong", libncmpi_iput_vars_longlong)
INTERCEPT3("ncmpi_iget_vars", libncmpi_iget_vars)
INTERCEPT3("ncmpi_iget_vars_schar", libncmpi_iget_vars_schar)
INTERCEPT3("ncmpi_iget_vars_text", libncmpi_iget_vars_text)
INTERCEPT3("ncmpi_iget_vars_short", libncmpi_iget_vars_short)
INTERCEPT3("ncmpi_iget_vars_int", libncmpi_iget_vars_int)
INTERCEPT3("ncmpi_iget_vars_float", libncmpi_iget_vars_float)
INTERCEPT3("ncmpi_iget_vars_double", libncmpi_iget_vars_double)
INTERCEPT3("ncmpi_iget_vars_longlong", libncmpi_iget_vars_longlong)
INTERCEPT3("ncmpi_bput_vars", libncmpi_bput_vars)
INTERCEPT3("ncmpi_bput_vars_schar", libncmpi_bput_vars_schar)
INTERCEPT3("ncmpi_bput_vars_text", libncmpi_bput_vars_text)
INTERCEPT3("ncmpi_bput_vars_short", libncmpi_bput_vars_short)
INTERCEPT3("ncmpi_bput_vars_int", libncmpi_bput_vars_int)
INTERCEPT3("ncmpi_bput_vars_float", libncmpi_bput_vars_float)
INTERCEPT3("ncmpi_bput_vars_double", libncmpi_bput_vars_double)
INTERCEPT3("ncmpi_bput_vars_longlong", libncmpi_bput_vars_longlong)
INTERCEPT3("ncmpi_iput_vars_uchar", libncmpi_iput_vars_uchar)
INTERCEPT3("ncmpi_iput_vars_ushort", libncmpi_iput_vars_ushort)
INTERCEPT3("ncmpi_iput_vars_uint", libncmpi_iput_vars_uint)
INTERCEPT3("ncmpi_iput_vars_long", libncmpi_iput_vars_long)
INTERCEPT3("ncmpi_iput_vars_ulonglong", libncmpi_iput_vars_ulonglong)
INTERCEPT3("ncmpi_iget_vars_uchar", libncmpi_iget_vars_uchar)
INTERCEPT3("ncmpi_iget_vars_ushort", libncmpi_iget_vars_ushort)
INTERCEPT3("ncmpi_iget_vars_uint", libncmpi_iget_vars_uint)
INTERCEPT3("ncmpi_iget_vars_long", libncmpi_iget_vars_long)
INTERCEPT3("ncmpi_iget_vars_ulonglong", libncmpi_iget_vars_ulonglong)
INTERCEPT3("ncmpi_bput_vars_uchar", libncmpi_bput_vars_uchar)
INTERCEPT3("ncmpi_bput_vars_ushort", libncmpi_bput_vars_ushort)
INTERCEPT3("ncmpi_bput_vars_uint", libncmpi_bput_vars_uint)
INTERCEPT3("ncmpi_bput_vars_long", libncmpi_bput_vars_long)
INTERCEPT3("ncmpi_bput_vars_ulonglong", libncmpi_bput_vars_ulonglong)
INTERCEPT3("ncmpi_iput_varm", libncmpi_iput_varm)
INTERCEPT3("ncmpi_iput_varm_schar", libncmpi_iput_varm_schar)
INTERCEPT3("ncmpi_iput_varm_text", libncmpi_iput_varm_text)
INTERCEPT3("ncmpi_iput_varm_short", libncmpi_iput_varm_short)
INTERCEPT3("ncmpi_iput_varm_int", libncmpi_iput_varm_int)
INTERCEPT3("ncmpi_iput_varm_float", libncmpi_iput_varm_float)
INTERCEPT3("ncmpi_iput_varm_double", libncmpi_iput_varm_double)
INTERCEPT3("ncmpi_iput_varm_longlong", libncmpi_iput_varm_longlong)
INTERCEPT3("ncmpi_iget_varm", libncmpi_iget_varm)
INTERCEPT3("ncmpi_iget_varm_schar", libncmpi_iget_varm_schar)
INTERCEPT3("ncmpi_iget_varm_text", libncmpi_iget_varm_text)
INTERCEPT3("ncmpi_iget_varm_short", libncmpi_iget_varm_short)
INTERCEPT3("ncmpi_iget_varm_int", libncmpi_iget_varm_int)
INTERCEPT3("ncmpi_iget_varm_float", libncmpi_iget_varm_float)
INTERCEPT3("ncmpi_iget_varm_double", libncmpi_iget_varm_double)
INTERCEPT3("ncmpi_iget_varm_longlong", libncmpi_iget_varm_longlong)
INTERCEPT3("ncmpi_bput_varm", libncmpi_bput_varm)
INTERCEPT3("ncmpi_bput_varm_schar", libncmpi_bput_varm_schar)
INTERCEPT3("ncmpi_bput_varm_text", libncmpi_bput_varm_text)
INTERCEPT3("ncmpi_bput_varm_short", libncmpi_bput_varm_short)
INTERCEPT3("ncmpi_bput_varm_int", libncmpi_bput_varm_int)
INTERCEPT3("ncmpi_bput_varm_float", libncmpi_bput_varm_float)
INTERCEPT3("ncmpi_bput_varm_double", libncmpi_bput_varm_double)
INTERCEPT3("ncmpi_bput_varm_longlong", libncmpi_bput_varm_longlong)
INTERCEPT3("ncmpi_iput_varm_uchar", libncmpi_iput_varm_uchar)
INTERCEPT3("ncmpi_iput_varm_ushort", libncmpi_iput_varm_ushort)
INTERCEPT3("ncmpi_iput_varm_uint", libncmpi_iput_varm_uint)
INTERCEPT3("ncmpi_iput_varm_long", libncmpi_iput_varm_long)
INTERCEPT3("ncmpi_iput_varm_ulonglong", libncmpi_iput_varm_ulonglong)
INTERCEPT3("ncmpi_iget_varm_uchar", libncmpi_iget_varm_uchar)
INTERCEPT3("ncmpi_iget_varm_ushort", libncmpi_iget_varm_ushort)
INTERCEPT3("ncmpi_iget_varm_uint", libncmpi_iget_varm_uint)
INTERCEPT3("ncmpi_iget_varm_long", libncmpi_iget_varm_long)
INTERCEPT3("ncmpi_iget_varm_ulonglong", libncmpi_iget_varm_ulonglong)
INTERCEPT3("ncmpi_bput_varm_uchar", libncmpi_bput_varm_uchar)
INTERCEPT3("ncmpi_bput_varm_ushort", libncmpi_bput_varm_ushort)
INTERCEPT3("ncmpi_bput_varm_uint", libncmpi_bput_varm_uint)
INTERCEPT3("ncmpi_bput_varm_long", libncmpi_bput_varm_long)
INTERCEPT3("ncmpi_bput_varm_ulonglong", libncmpi_bput_varm_ulonglong)
INTERCEPT3("ncmpi_iput_varn", libncmpi_iput_varn)
INTERCEPT3("ncmpi_iget_varn", libncmpi_iget_varn)
INTERCEPT3("ncmpi_iput_varn_text", libncmpi_iput_varn_text)
INTERCEPT3("ncmpi_iput_varn_schar", libncmpi_iput_varn_schar)
INTERCEPT3("ncmpi_iput_varn_short", libncmpi_iput_varn_short)
INTERCEPT3("ncmpi_iput_varn_int", libncmpi_iput_varn_int)
INTERCEPT3("ncmpi_iput_varn_float", libncmpi_iput_varn_float)
INTERCEPT3("ncmpi_iput_varn_double", libncmpi_iput_varn_double)
INTERCEPT3("ncmpi_iput_varn_longlong", libncmpi_iput_varn_longlong)
INTERCEPT3("ncmpi_iput_varn_uchar", libncmpi_iput_varn_uchar)
INTERCEPT3("ncmpi_iput_varn_ushort", libncmpi_iput_varn_ushort)
INTERCEPT3("ncmpi_iput_varn_uint", libncmpi_iput_varn_uint)
INTERCEPT3("ncmpi_iput_varn_long", libncmpi_iput_varn_long)
INTERCEPT3("ncmpi_iput_varn_ulonglong", libncmpi_iput_varn_ulonglong)
INTERCEPT3("ncmpi_iget_varn_text", libncmpi_iget_varn_text)
INTERCEPT3("ncmpi_iget_varn_schar", libncmpi_iget_varn_schar)
INTERCEPT3("ncmpi_iget_varn_short", libncmpi_iget_varn_short)
INTERCEPT3("ncmpi_iget_varn_int", libncmpi_iget_varn_int)
INTERCEPT3("ncmpi_iget_varn_float", libncmpi_iget_varn_float)
INTERCEPT3("ncmpi_iget_varn_double", libncmpi_iget_varn_double)
INTERCEPT3("ncmpi_iget_varn_longlong", libncmpi_iget_varn_longlong)
INTERCEPT3("ncmpi_iget_varn_uchar", libncmpi_iget_varn_uchar)
INTERCEPT3("ncmpi_iget_varn_ushort", libncmpi_iget_varn_ushort)
INTERCEPT3("ncmpi_iget_varn_uint", libncmpi_iget_varn_uint)
INTERCEPT3("ncmpi_iget_varn_long", libncmpi_iget_varn_long)
INTERCEPT3("ncmpi_iget_varn_ulonglong", libncmpi_iget_varn_ulonglong)
INTERCEPT3("ncmpi_bput_varn", libncmpi_bput_varn)
INTERCEPT3("ncmpi_bput_varn_text", libncmpi_bput_varn_text)
INTERCEPT3("ncmpi_bput_varn_schar", libncmpi_bput_varn_schar)
INTERCEPT3("ncmpi_bput_varn_short", libncmpi_bput_varn_short)
INTERCEPT3("ncmpi_bput_varn_int", libncmpi_bput_varn_int)
INTERCEPT3("ncmpi_bput_varn_float", libncmpi_bput_varn_float)
INTERCEPT3("ncmpi_bput_varn_double", libncmpi_bput_varn_double)
INTERCEPT3("ncmpi_bput_varn_longlong", libncmpi_bput_varn_longlong)
INTERCEPT3("ncmpi_bput_varn_uchar", libncmpi_bput_varn_uchar)
INTERCEPT3("ncmpi_bput_varn_ushort", libncmpi_bput_varn_ushort)
INTERCEPT3("ncmpi_bput_varn_uint", libncmpi_bput_varn_uint)
INTERCEPT3("ncmpi_bput_varn_long", libncmpi_bput_varn_long)
INTERCEPT3("ncmpi_bput_varn_ulonglong", libncmpi_bput_varn_ulonglong)
INTERCEPT3("ncmpi_mput_var", libncmpi_mput_var)
INTERCEPT3("ncmpi_mput_var_all", libncmpi_mput_var_all)
INTERCEPT3("ncmpi_mput_var_text", libncmpi_mput_var_text)
INTERCEPT3("ncmpi_mput_var_text_all", libncmpi_mput_var_text_all)
INTERCEPT3("ncmpi_mput_var_schar", libncmpi_mput_var_schar)
INTERCEPT3("ncmpi_mput_var_schar_all", libncmpi_mput_var_schar_all)
INTERCEPT3("ncmpi_mput_var_uchar", libncmpi_mput_var_uchar)
INTERCEPT3("ncmpi_mput_var_uchar_all", libncmpi_mput_var_uchar_all)
INTERCEPT3("ncmpi_mput_var_short", libncmpi_mput_var_short)
INTERCEPT3("ncmpi_mput_var_short_all", libncmpi_mput_var_short_all)
INTERCEPT3("ncmpi_mput_var_ushort", libncmpi_mput_var_ushort)
INTERCEPT3("ncmpi_mput_var_ushort_all", libncmpi_mput_var_ushort_all)
INTERCEPT3("ncmpi_mput_var_int", libncmpi_mput_var_int)
INTERCEPT3("ncmpi_mput_var_int_all", libncmpi_mput_var_int_all)
INTERCEPT3("ncmpi_mput_var_uint", libncmpi_mput_var_uint)
INTERCEPT3("ncmpi_mput_var_uint_all", libncmpi_mput_var_uint_all)
INTERCEPT3("ncmpi_mput_var_long", libncmpi_mput_var_long)
INTERCEPT3("ncmpi_mput_var_long_all", libncmpi_mput_var_long_all)
INTERCEPT3("ncmpi_mput_var_float", libncmpi_mput_var_float)
INTERCEPT3("ncmpi_mput_var_float_all", libncmpi_mput_var_float_all)
INTERCEPT3("ncmpi_mput_var_double", libncmpi_mput_var_double)
INTERCEPT3("ncmpi_mput_var_double_all", libncmpi_mput_var_double_all)
INTERCEPT3("ncmpi_mput_var_longlong", libncmpi_mput_var_longlong)
INTERCEPT3("ncmpi_mput_var_longlong_all", libncmpi_mput_var_longlong_all)
INTERCEPT3("ncmpi_mput_var_ulonglong", libncmpi_mput_var_ulonglong)
INTERCEPT3("ncmpi_mput_var_ulonglong_all", libncmpi_mput_var_ulonglong_all)
INTERCEPT3("ncmpi_mput_var1", libncmpi_mput_var1)
INTERCEPT3("ncmpi_mput_var1_all", libncmpi_mput_var1_all)
INTERCEPT3("ncmpi_mput_var1_text", libncmpi_mput_var1_text)
INTERCEPT3("ncmpi_mput_var1_text_all", libncmpi_mput_var1_text_all)
INTERCEPT3("ncmpi_mput_var1_schar", libncmpi_mput_var1_schar)
INTERCEPT3("ncmpi_mput_var1_schar_all", libncmpi_mput_var1_schar_all)
INTERCEPT3("ncmpi_mput_var1_uchar", libncmpi_mput_var1_uchar)
INTERCEPT3("ncmpi_mput_var1_uchar_all", libncmpi_mput_var1_uchar_all)
INTERCEPT3("ncmpi_mput_var1_short", libncmpi_mput_var1_short)
INTERCEPT3("ncmpi_mput_var1_short_all", libncmpi_mput_var1_short_all)
INTERCEPT3("ncmpi_mput_var1_ushort", libncmpi_mput_var1_ushort)
INTERCEPT3("ncmpi_mput_var1_ushort_all", libncmpi_mput_var1_ushort_all)
INTERCEPT3("ncmpi_mput_var1_int", libncmpi_mput_var1_int)
INTERCEPT3("ncmpi_mput_var1_int_all", libncmpi_mput_var1_int_all)
INTERCEPT3("ncmpi_mput_var1_uint", libncmpi_mput_var1_uint)
INTERCEPT3("ncmpi_mput_var1_uint_all", libncmpi_mput_var1_uint_all)
INTERCEPT3("ncmpi_mput_var1_long", libncmpi_mput_var1_long)
INTERCEPT3("ncmpi_mput_var1_long_all", libncmpi_mput_var1_long_all)
INTERCEPT3("ncmpi_mput_var1_float", libncmpi_mput_var1_float)
INTERCEPT3("ncmpi_mput_var1_float_all", libncmpi_mput_var1_float_all)
INTERCEPT3("ncmpi_mput_var1_double", libncmpi_mput_var1_double)
INTERCEPT3("ncmpi_mput_var1_double_all", libncmpi_mput_var1_double_all)
INTERCEPT3("ncmpi_mput_var1_longlong", libncmpi_mput_var1_longlong)
INTERCEPT3("ncmpi_mput_var1_longlong_all", libncmpi_mput_var1_longlong_all)
INTERCEPT3("ncmpi_mput_var1_ulonglong", libncmpi_mput_var1_ulonglong)
INTERCEPT3("ncmpi_mput_var1_ulonglong_all", libncmpi_mput_var1_ulonglong_all)
INTERCEPT3("ncmpi_mput_vara", libncmpi_mput_vara)
INTERCEPT3("ncmpi_mput_vara_all", libncmpi_mput_vara_all)
INTERCEPT3("ncmpi_mput_vara_text", libncmpi_mput_vara_text)
INTERCEPT3("ncmpi_mput_vara_text_all", libncmpi_mput_vara_text_all)
INTERCEPT3("ncmpi_mput_vara_schar", libncmpi_mput_vara_schar)
INTERCEPT3("ncmpi_mput_vara_schar_all", libncmpi_mput_vara_schar_all)
INTERCEPT3("ncmpi_mput_vara_uchar", libncmpi_mput_vara_uchar)
INTERCEPT3("ncmpi_mput_vara_uchar_all", libncmpi_mput_vara_uchar_all)
INTERCEPT3("ncmpi_mput_vara_short", libncmpi_mput_vara_short)
INTERCEPT3("ncmpi_mput_vara_short_all", libncmpi_mput_vara_short_all)
INTERCEPT3("ncmpi_mput_vara_ushort", libncmpi_mput_vara_ushort)
INTERCEPT3("ncmpi_mput_vara_ushort_all", libncmpi_mput_vara_ushort_all)
INTERCEPT3("ncmpi_mput_vara_int", libncmpi_mput_vara_int)
INTERCEPT3("ncmpi_mput_vara_int_all", libncmpi_mput_vara_int_all)
INTERCEPT3("ncmpi_mput_vara_uint", libncmpi_mput_vara_uint)
INTERCEPT3("ncmpi_mput_vara_uint_all", libncmpi_mput_vara_uint_all)
INTERCEPT3("ncmpi_mput_vara_long", libncmpi_mput_vara_long)
INTERCEPT3("ncmpi_mput_vara_long_all", libncmpi_mput_vara_long_all)
INTERCEPT3("ncmpi_mput_vara_float", libncmpi_mput_vara_float)
INTERCEPT3("ncmpi_mput_vara_float_all", libncmpi_mput_vara_float_all)
INTERCEPT3("ncmpi_mput_vara_double", libncmpi_mput_vara_double)
INTERCEPT3("ncmpi_mput_vara_double_all", libncmpi_mput_vara_double_all)
INTERCEPT3("ncmpi_mput_vara_longlong", libncmpi_mput_vara_longlong)
INTERCEPT3("ncmpi_mput_vara_longlong_all", libncmpi_mput_vara_longlong_all)
INTERCEPT3("ncmpi_mput_vara_ulonglong", libncmpi_mput_vara_ulonglong)
INTERCEPT3("ncmpi_mput_vara_ulonglong_all", libncmpi_mput_vara_ulonglong_all)
INTERCEPT3("ncmpi_mput_vars", libncmpi_mput_vars)
INTERCEPT3("ncmpi_mput_vars_all", libncmpi_mput_vars_all)
INTERCEPT3("ncmpi_mput_vars_text", libncmpi_mput_vars_text)
INTERCEPT3("ncmpi_mput_vars_text_all", libncmpi_mput_vars_text_all)
INTERCEPT3("ncmpi_mput_vars_schar", libncmpi_mput_vars_schar)
INTERCEPT3("ncmpi_mput_vars_schar_all", libncmpi_mput_vars_schar_all)
INTERCEPT3("ncmpi_mput_vars_uchar", libncmpi_mput_vars_uchar)
INTERCEPT3("ncmpi_mput_vars_uchar_all", libncmpi_mput_vars_uchar_all)
INTERCEPT3("ncmpi_mput_vars_short", libncmpi_mput_vars_short)
INTERCEPT3("ncmpi_mput_vars_short_all", libncmpi_mput_vars_short_all)
INTERCEPT3("ncmpi_mput_vars_ushort", libncmpi_mput_vars_ushort)
INTERCEPT3("ncmpi_mput_vars_ushort_all", libncmpi_mput_vars_ushort_all)
INTERCEPT3("ncmpi_mput_vars_int", libncmpi_mput_vars_int)
INTERCEPT3("ncmpi_mput_vars_int_all", libncmpi_mput_vars_int_all)
INTERCEPT3("ncmpi_mput_vars_uint", libncmpi_mput_vars_uint)
INTERCEPT3("ncmpi_mput_vars_uint_all", libncmpi_mput_vars_uint_all)
INTERCEPT3("ncmpi_mput_vars_long", libncmpi_mput_vars_long)
INTERCEPT3("ncmpi_mput_vars_long_all", libncmpi_mput_vars_long_all)
INTERCEPT3("ncmpi_mput_vars_float", libncmpi_mput_vars_float)
INTERCEPT3("ncmpi_mput_vars_float_all", libncmpi_mput_vars_float_all)
INTERCEPT3("ncmpi_mput_vars_double", libncmpi_mput_vars_double)
INTERCEPT3("ncmpi_mput_vars_double_all", libncmpi_mput_vars_double_all)
INTERCEPT3("ncmpi_mput_vars_longlong", libncmpi_mput_vars_longlong)
INTERCEPT3("ncmpi_mput_vars_longlong_all", libncmpi_mput_vars_longlong_all)
INTERCEPT3("ncmpi_mput_vars_ulonglong", libncmpi_mput_vars_ulonglong)
INTERCEPT3("ncmpi_mput_vars_ulonglong_all", libncmpi_mput_vars_ulonglong_all)
INTERCEPT3("ncmpi_mput_varm", libncmpi_mput_varm)
INTERCEPT3("ncmpi_mput_varm_all", libncmpi_mput_varm_all)
INTERCEPT3("ncmpi_mput_varm_text", libncmpi_mput_varm_text)
INTERCEPT3("ncmpi_mput_varm_text_all", libncmpi_mput_varm_text_all)
INTERCEPT3("ncmpi_mput_varm_schar", libncmpi_mput_varm_schar)
INTERCEPT3("ncmpi_mput_varm_schar_all", libncmpi_mput_varm_schar_all)
INTERCEPT3("ncmpi_mput_varm_uchar", libncmpi_mput_varm_uchar)
INTERCEPT3("ncmpi_mput_varm_uchar_all", libncmpi_mput_varm_uchar_all)
INTERCEPT3("ncmpi_mput_varm_short", libncmpi_mput_varm_short)
INTERCEPT3("ncmpi_mput_varm_short_all", libncmpi_mput_varm_short_all)
INTERCEPT3("ncmpi_mput_varm_ushort", libncmpi_mput_varm_ushort)
INTERCEPT3("ncmpi_mput_varm_ushort_all", libncmpi_mput_varm_ushort_all)
INTERCEPT3("ncmpi_mput_varm_int", libncmpi_mput_varm_int)
INTERCEPT3("ncmpi_mput_varm_int_all", libncmpi_mput_varm_int_all)
INTERCEPT3("ncmpi_mput_varm_uint", libncmpi_mput_varm_uint)
INTERCEPT3("ncmpi_mput_varm_uint_all", libncmpi_mput_varm_uint_all)
INTERCEPT3("ncmpi_mput_varm_long", libncmpi_mput_varm_long)
INTERCEPT3("ncmpi_mput_varm_long_all", libncmpi_mput_varm_long_all)
INTERCEPT3("ncmpi_mput_varm_float", libncmpi_mput_varm_float)
INTERCEPT3("ncmpi_mput_varm_float_all", libncmpi_mput_varm_float_all)
INTERCEPT3("ncmpi_mput_varm_double", libncmpi_mput_varm_double)
INTERCEPT3("ncmpi_mput_varm_double_all", libncmpi_mput_varm_double_all)
INTERCEPT3("ncmpi_mput_varm_longlong", libncmpi_mput_varm_longlong)
INTERCEPT3("ncmpi_mput_varm_longlong_all", libncmpi_mput_varm_longlong_all)
INTERCEPT3("ncmpi_mput_varm_ulonglong", libncmpi_mput_varm_ulonglong)
INTERCEPT3("ncmpi_mput_varm_ulonglong_all", libncmpi_mput_varm_ulonglong_all)
INTERCEPT3("ncmpi_mget_var", libncmpi_mget_var)
INTERCEPT3("ncmpi_mget_var_all", libncmpi_mget_var_all)
INTERCEPT3("ncmpi_mget_var_text", libncmpi_mget_var_text)
INTERCEPT3("ncmpi_mget_var_text_all", libncmpi_mget_var_text_all)
INTERCEPT3("ncmpi_mget_var_schar", libncmpi_mget_var_schar)
INTERCEPT3("ncmpi_mget_var_schar_all", libncmpi_mget_var_schar_all)
INTERCEPT3("ncmpi_mget_var_uchar", libncmpi_mget_var_uchar)
INTERCEPT3("ncmpi_mget_var_uchar_all", libncmpi_mget_var_uchar_all)
INTERCEPT3("ncmpi_mget_var_short", libncmpi_mget_var_short)
INTERCEPT3("ncmpi_mget_var_short_all", libncmpi_mget_var_short_all)
INTERCEPT3("ncmpi_mget_var_ushort", libncmpi_mget_var_ushort)
INTERCEPT3("ncmpi_mget_var_ushort_all", libncmpi_mget_var_ushort_all)
INTERCEPT3("ncmpi_mget_var_int", libncmpi_mget_var_int)
INTERCEPT3("ncmpi_mget_var_int_all", libncmpi_mget_var_int_all)
INTERCEPT3("ncmpi_mget_var_uint", libncmpi_mget_var_uint)
INTERCEPT3("ncmpi_mget_var_uint_all", libncmpi_mget_var_uint_all)
INTERCEPT3("ncmpi_mget_var_long", libncmpi_mget_var_long)
INTERCEPT3("ncmpi_mget_var_long_all", libncmpi_mget_var_long_all)
INTERCEPT3("ncmpi_mget_var_float", libncmpi_mget_var_float)
INTERCEPT3("ncmpi_mget_var_float_all", libncmpi_mget_var_float_all)
INTERCEPT3("ncmpi_mget_var_double", libncmpi_mget_var_double)
INTERCEPT3("ncmpi_mget_var_double_all", libncmpi_mget_var_double_all)
INTERCEPT3("ncmpi_mget_var_longlong", libncmpi_mget_var_longlong)
INTERCEPT3("ncmpi_mget_var_longlong_all", libncmpi_mget_var_longlong_all)
INTERCEPT3("ncmpi_mget_var_ulonglong", libncmpi_mget_var_ulonglong)
INTERCEPT3("ncmpi_mget_var_ulonglong_all", libncmpi_mget_var_ulonglong_all)
INTERCEPT3("ncmpi_mget_var1", libncmpi_mget_var1)
INTERCEPT3("ncmpi_mget_var1_all", libncmpi_mget_var1_all)
INTERCEPT3("ncmpi_mget_var1_text", libncmpi_mget_var1_text)
INTERCEPT3("ncmpi_mget_var1_text_all", libncmpi_mget_var1_text_all)
INTERCEPT3("ncmpi_mget_var1_schar", libncmpi_mget_var1_schar)
INTERCEPT3("ncmpi_mget_var1_schar_all", libncmpi_mget_var1_schar_all)
INTERCEPT3("ncmpi_mget_var1_uchar", libncmpi_mget_var1_uchar)
INTERCEPT3("ncmpi_mget_var1_uchar_all", libncmpi_mget_var1_uchar_all)
INTERCEPT3("ncmpi_mget_var1_short", libncmpi_mget_var1_short)
INTERCEPT3("ncmpi_mget_var1_short_all", libncmpi_mget_var1_short_all)
INTERCEPT3("ncmpi_mget_var1_ushort", libncmpi_mget_var1_ushort)
INTERCEPT3("ncmpi_mget_var1_ushort_all", libncmpi_mget_var1_ushort_all)
INTERCEPT3("ncmpi_mget_var1_int", libncmpi_mget_var1_int)
INTERCEPT3("ncmpi_mget_var1_int_all", libncmpi_mget_var1_int_all)
INTERCEPT3("ncmpi_mget_var1_uint", libncmpi_mget_var1_uint)
INTERCEPT3("ncmpi_mget_var1_uint_all", libncmpi_mget_var1_uint_all)
INTERCEPT3("ncmpi_mget_var1_long", libncmpi_mget_var1_long)
INTERCEPT3("ncmpi_mget_var1_long_all", libncmpi_mget_var1_long_all)
INTERCEPT3("ncmpi_mget_var1_float", libncmpi_mget_var1_float)
INTERCEPT3("ncmpi_mget_var1_float_all", libncmpi_mget_var1_float_all)
INTERCEPT3("ncmpi_mget_var1_double", libncmpi_mget_var1_double)
INTERCEPT3("ncmpi_mget_var1_double_all", libncmpi_mget_var1_double_all)
INTERCEPT3("ncmpi_mget_var1_longlong", libncmpi_mget_var1_longlong)
INTERCEPT3("ncmpi_mget_var1_longlong_all", libncmpi_mget_var1_longlong_all)
INTERCEPT3("ncmpi_mget_var1_ulonglong", libncmpi_mget_var1_ulonglong)
INTERCEPT3("ncmpi_mget_var1_ulonglong_all", libncmpi_mget_var1_ulonglong_all)
INTERCEPT3("ncmpi_mget_vara", libncmpi_mget_vara)
INTERCEPT3("ncmpi_mget_vara_all", libncmpi_mget_vara_all)
INTERCEPT3("ncmpi_mget_vara_text", libncmpi_mget_vara_text)
INTERCEPT3("ncmpi_mget_vara_text_all", libncmpi_mget_vara_text_all)
INTERCEPT3("ncmpi_mget_vara_schar", libncmpi_mget_vara_schar)
INTERCEPT3("ncmpi_mget_vara_schar_all", libncmpi_mget_vara_schar_all)
INTERCEPT3("ncmpi_mget_vara_uchar", libncmpi_mget_vara_uchar)
INTERCEPT3("ncmpi_mget_vara_uchar_all", libncmpi_mget_vara_uchar_all)
INTERCEPT3("ncmpi_mget_vara_short", libncmpi_mget_vara_short)
INTERCEPT3("ncmpi_mget_vara_short_all", libncmpi_mget_vara_short_all)
INTERCEPT3("ncmpi_mget_vara_ushort", libncmpi_mget_vara_ushort)
INTERCEPT3("ncmpi_mget_vara_ushort_all", libncmpi_mget_vara_ushort_all)
INTERCEPT3("ncmpi_mget_vara_int", libncmpi_mget_vara_int)
INTERCEPT3("ncmpi_mget_vara_int_all", libncmpi_mget_vara_int_all)
INTERCEPT3("ncmpi_mget_vara_uint", libncmpi_mget_vara_uint)
INTERCEPT3("ncmpi_mget_vara_uint_all", libncmpi_mget_vara_uint_all)
INTERCEPT3("ncmpi_mget_vara_long", libncmpi_mget_vara_long)
INTERCEPT3("ncmpi_mget_vara_long_all", libncmpi_mget_vara_long_all)
INTERCEPT3("ncmpi_mget_vara_float", libncmpi_mget_vara_float)
INTERCEPT3("ncmpi_mget_vara_float_all", libncmpi_mget_vara_float_all)
INTERCEPT3("ncmpi_mget_vara_double", libncmpi_mget_vara_double)
INTERCEPT3("ncmpi_mget_vara_double_all", libncmpi_mget_vara_double_all)
INTERCEPT3("ncmpi_mget_vara_longlong", libncmpi_mget_vara_longlong)
INTERCEPT3("ncmpi_mget_vara_longlong_all", libncmpi_mget_vara_longlong_all)
INTERCEPT3("ncmpi_mget_vara_ulonglong", libncmpi_mget_vara_ulonglong)
INTERCEPT3("ncmpi_mget_vara_ulonglong_all", libncmpi_mget_vara_ulonglong_all)
INTERCEPT3("ncmpi_mget_vars", libncmpi_mget_vars)
INTERCEPT3("ncmpi_mget_vars_all", libncmpi_mget_vars_all)
INTERCEPT3("ncmpi_mget_vars_text", libncmpi_mget_vars_text)
INTERCEPT3("ncmpi_mget_vars_text_all", libncmpi_mget_vars_text_all)
INTERCEPT3("ncmpi_mget_vars_schar", libncmpi_mget_vars_schar)
INTERCEPT3("ncmpi_mget_vars_schar_all", libncmpi_mget_vars_schar_all)
INTERCEPT3("ncmpi_mget_vars_uchar", libncmpi_mget_vars_uchar)
INTERCEPT3("ncmpi_mget_vars_uchar_all", libncmpi_mget_vars_uchar_all)
INTERCEPT3("ncmpi_mget_vars_short", libncmpi_mget_vars_short)
INTERCEPT3("ncmpi_mget_vars_short_all", libncmpi_mget_vars_short_all)
INTERCEPT3("ncmpi_mget_vars_ushort", libncmpi_mget_vars_ushort)
INTERCEPT3("ncmpi_mget_vars_ushort_all", libncmpi_mget_vars_ushort_all)
INTERCEPT3("ncmpi_mget_vars_int", libncmpi_mget_vars_int)
INTERCEPT3("ncmpi_mget_vars_int_all", libncmpi_mget_vars_int_all)
INTERCEPT3("ncmpi_mget_vars_uint", libncmpi_mget_vars_uint)
INTERCEPT3("ncmpi_mget_vars_uint_all", libncmpi_mget_vars_uint_all)
INTERCEPT3("ncmpi_mget_vars_long", libncmpi_mget_vars_long)
INTERCEPT3("ncmpi_mget_vars_long_all", libncmpi_mget_vars_long_all)
INTERCEPT3("ncmpi_mget_vars_float", libncmpi_mget_vars_float)
INTERCEPT3("ncmpi_mget_vars_float_all", libncmpi_mget_vars_float_all)
INTERCEPT3("ncmpi_mget_vars_double", libncmpi_mget_vars_double)
INTERCEPT3("ncmpi_mget_vars_double_all", libncmpi_mget_vars_double_all)
INTERCEPT3("ncmpi_mget_vars_longlong", libncmpi_mget_vars_longlong)
INTERCEPT3("ncmpi_mget_vars_longlong_all", libncmpi_mget_vars_longlong_all)
INTERCEPT3("ncmpi_mget_vars_ulonglong", libncmpi_mget_vars_ulonglong)
INTERCEPT3("ncmpi_mget_vars_ulonglong_all", libncmpi_mget_vars_ulonglong_all)
INTERCEPT3("ncmpi_mget_varm", libncmpi_mget_varm)
INTERCEPT3("ncmpi_mget_varm_all", libncmpi_mget_varm_all)
INTERCEPT3("ncmpi_mget_varm_text", libncmpi_mget_varm_text)
INTERCEPT3("ncmpi_mget_varm_text_all", libncmpi_mget_varm_text_all)
INTERCEPT3("ncmpi_mget_varm_schar", libncmpi_mget_varm_schar)
INTERCEPT3("ncmpi_mget_varm_schar_all", libncmpi_mget_varm_schar_all)
INTERCEPT3("ncmpi_mget_varm_uchar", libncmpi_mget_varm_uchar)
INTERCEPT3("ncmpi_mget_varm_uchar_all", libncmpi_mget_varm_uchar_all)
INTERCEPT3("ncmpi_mget_varm_short", libncmpi_mget_varm_short)
INTERCEPT3("ncmpi_mget_varm_short_all", libncmpi_mget_varm_short_all)
INTERCEPT3("ncmpi_mget_varm_ushort", libncmpi_mget_varm_ushort)
INTERCEPT3("ncmpi_mget_varm_ushort_all", libncmpi_mget_varm_ushort_all)
INTERCEPT3("ncmpi_mget_varm_int", libncmpi_mget_varm_int)
INTERCEPT3("ncmpi_mget_varm_int_all", libncmpi_mget_varm_int_all)
INTERCEPT3("ncmpi_mget_varm_uint", libncmpi_mget_varm_uint)
INTERCEPT3("ncmpi_mget_varm_uint_all", libncmpi_mget_varm_uint_all)
INTERCEPT3("ncmpi_mget_varm_long", libncmpi_mget_varm_long)
INTERCEPT3("ncmpi_mget_varm_long_all", libncmpi_mget_varm_long_all)
INTERCEPT3("ncmpi_mget_varm_float", libncmpi_mget_varm_float)
INTERCEPT3("ncmpi_mget_varm_float_all", libncmpi_mget_varm_float_all)
INTERCEPT3("ncmpi_mget_varm_double", libncmpi_mget_varm_double)
INTERCEPT3("ncmpi_mget_varm_double_all", libncmpi_mget_varm_double_all)
INTERCEPT3("ncmpi_mget_varm_longlong", libncmpi_mget_varm_longlong)
INTERCEPT3("ncmpi_mget_varm_longlong_all", libncmpi_mget_varm_longlong_all)
INTERCEPT3("ncmpi_mget_varm_ulonglong", libncmpi_mget_varm_ulonglong)
INTERCEPT3("ncmpi_mget_varm_ulonglong_all", libncmpi_mget_varm_ulonglong_all)
PPTRACE_END_INTERCEPT_FUNCTIONS(pnetcdf)

static void init_pnetcdf() {
  INSTRUMENT_FUNCTIONS(pnetcdf);

  if (eztrace_autostart_enabled())
    eztrace_start();

  _pnetcdf_initialized = 1;
}

static void finalize_pnetcdf() {
  _pnetcdf_initialized = 0;

  eztrace_stop();
}

static void _pnetcdf_init(void) __attribute__((constructor));
static void _pnetcdf_init(void) {
  eztrace_log(dbg_lvl_debug, "eztrace_pnetcdf constructor starts\n");
  EZT_REGISTER_MODULE(pnetcdf, "Module for PNetCDF functions",
		      init_pnetcdf, finalize_pnetcdf);
  eztrace_log(dbg_lvl_debug, "eztrace_pnetcdf constructor ends\n");
}
