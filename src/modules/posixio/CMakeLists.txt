add_library(eztrace-posixio SHARED
  posixio.c
  posix_io_otf2.c
)

target_link_libraries(eztrace-posixio
  PRIVATE
    atomic
    dl
    eztrace-core
    eztrace-instrumentation
    eztrace-lib
)

target_include_directories(eztrace-posixio
  PRIVATE
  ${CMAKE_SOURCE_DIR}/src/core/include/eztrace-core/
)

#--------------------------------------------

install(
  TARGETS eztrace-posixio
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
