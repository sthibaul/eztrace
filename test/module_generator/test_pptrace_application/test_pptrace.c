/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#include <stdio.h>
#include "test_pptrace.h"

double test_pptrace_foo(uint32_t *array, uint32_t array_size) {
  printf("Entered test_pptrace_foo!\n");
  uint32_t i, sum;
  sum = 0;

  for (i = 0; i < array_size; i++) {
    array[i] = array[i] * (i + 1);
    sum += array[i];
  }

  printf("Leaving test_pptrace_foo!\n");
  return sum;
}

