enable_testing()

if (EZTRACE_ENABLE_COMPILER_INSTRUMENTATION)

  set(CMAKE_C_FLAGS "-finstrument-functions -O0")
  set(LINK_OPTIONS  "-rdynamic")

  add_executable(foo foo.c)
  add_test(build_foo "${CMAKE_COMMAND}" --build "${CMAKE_BINARY_DIR}" --target foo)

  add_test (compiler_instrumentation_tests bash "${CMAKE_CURRENT_SOURCE_DIR}/run.sh" DEPENDS "build_foo_cat")
  set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/compiler_instrumentation")


  # Get the list of tests, and set environment variables
  get_property(test_list DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY TESTS)
  set_property(TEST ${test_list}
    PROPERTY ENVIRONMENT
    "EZTRACE_LIBRARY_PATH=${EZTRACE_LIBRARY_PATH}"
    ${TEST_ENVIRONMENT}
  )
endif()
