enable_testing()

add_executable(htable
  htable.c)

# depending on the framework, you need to link to it
target_link_libraries(htable
  eztrace-core)

target_compile_options(htable
  PRIVATE
    -Wall -Wextra -Wpedantic
    -Werror
)

target_include_directories(htable
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}/src/core/include/eztrace-core/
)

add_test(NAME htable COMMAND htable)
set(EZTRACE_LIBRARY_PATH "${CMAKE_BINARY_DIR}/src/eztrace-lib")

if (EZTRACE_ENABLE_MPI)
add_test (mpi_tests bash ${CMAKE_CURRENT_SOURCE_DIR}/../mpi/run.sh)
set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/mpi")
endif()
if (EZTRACE_ENABLE_MEMORY)
add_test (memory_tests bash ${CMAKE_CURRENT_SOURCE_DIR}/../memory/run.sh)
set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/memory")
endif()
if (EZTRACE_ENABLE_OPENMP)
add_test (openmp_tests bash ${CMAKE_CURRENT_SOURCE_DIR}/../openmp/run.sh)
set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/omp")
endif()

if (EZTRACE_ENABLE_OMPT)
add_test (ompt_tests bash ${CMAKE_CURRENT_SOURCE_DIR}/../ompt/run.sh)
set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/ompt")
endif()

if (EZTRACE_ENABLE_STARPU)
add_test (starpu_tests bash ${CMAKE_CURRENT_SOURCE_DIR}/../starpu/run.sh)
set(EZTRACE_LIBRARY_PATH "${EZTRACE_LIBRARY_PATH}:${CMAKE_BINARY_DIR}/src/modules/starpu")
endif()

# Get the list of tests, and set environment variables
get_property(test_list DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY TESTS)
set_property(TEST ${test_list}
  PROPERTY ENVIRONMENT
  "EZTRACE_LIBRARY_PATH=${EZTRACE_LIBRARY_PATH}"
  ${TEST_ENVIRONMENT}
)

