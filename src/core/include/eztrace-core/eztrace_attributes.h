/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#ifndef EZTRACE_ATTRIBUTES_H
#define EZTRACE_ATTRIBUTES_H

#ifdef __GNUC__
#define MAYBE_UNUSED __attribute__((unused))
#define FALLTHROUGH __attribute__((fallthrough))
#define COLD __attribute__((cold))
#define REENTRANT __attribute__((const))
#define NODISCARD __attribute__((warn_unused_result))
#else
#define MAYBE_UNUSED
#define FALLTHROUGH
#define COLD
#define REENTRANT
#define NODISCARD
#endif

#endif /* EZTRACE_ATTRIBUTES_H */
