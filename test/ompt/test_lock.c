#include <stdio.h>
#include <omp.h>

int main()
{
  omp_lock_t lock1, lock2;
  omp_init_lock(&lock1);
  omp_init_lock(&lock2);
  int total = 0;

  #pragma omp parallel num_threads(50)
  {
    omp_set_lock(&lock1);
    omp_set_lock(&lock2);
    total += 1;
    omp_unset_lock(&lock1);
    omp_unset_lock(&lock2);
  }
  
  omp_destroy_lock(&lock1);
  omp_destroy_lock(&lock2);
  printf("Total threads: %d\n", total);
  return 0;
}
