BEGIN_MODULE
NAME main
DESC "Module for the main program"

# In this section, insert the #include/#define directives that are needed for
# compiling
BEGIN_INCLUDE

END_INCLUDE


# In this section, insert the CFLAGS that are needed for compiling
BEGIN_CFLAGS

END_CFLAGS

# In this section, insert the LDFLAGS that are needed for compiling
BEGIN_LDFLAGS

END_LDFLAGS

int bin_instrumented_function(double *t, int size, int n)
int lib_instrumented_function(double *t, int size, int n)

END_MODULE
